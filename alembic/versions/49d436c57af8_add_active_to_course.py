"""add active to course

Revision ID: 49d436c57af8
Revises: e88eb5be679b
Create Date: 2018-09-05 00:57:40.458908

"""

# revision identifiers, used by Alembic.
revision = '49d436c57af8'
down_revision = 'e88eb5be679b'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('courses', sa.Column('active', sa.Boolean))


def downgrade():
	op.drop_column('courses', 'active')