"""add assgnmntgrd table

Revision ID: e55725cee1ee
Revises: 17f9efc32294
Create Date: 2018-03-05 20:43:22.400212

"""

# revision identifiers, used by Alembic.
revision = 'e55725cee1ee'
down_revision = '17f9efc32294'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
	op.create_table(
		'assgnmnt_grade',
		sa.Column('homework_id', sa.Integer, sa.ForeignKey('homework.id'), primary_key=True),
		sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id'), primary_key=True),
		sa.Column('percent_score', sa.Float),
		sa.Column('max_point', sa.Float)
	)


def downgrade():
	op.drop_table('assgnmnt_grade')