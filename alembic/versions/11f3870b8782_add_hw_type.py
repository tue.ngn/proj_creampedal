"""add hw type

Revision ID: 11f3870b8782
Revises:
Create Date: 2018-02-27 19:18:16.915880

"""

# revision identifiers, used by Alembic.
revision = '11f3870b8782'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('homework', sa.Column('assgnmt_typ', sa.Integer))


def downgrade():
    op.drop_column('homework', 'assgnmt_typ')
