"""add hw q omit giveback

Revision ID: 17f9efc32294
Revises: 11f3870b8782
Create Date: 2018-03-05 20:41:02.206460

"""

# revision identifiers, used by Alembic.
revision = '17f9efc32294'
down_revision = '11f3870b8782'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('homework_question', sa.Column('give_back', sa.Boolean))
    op.add_column('homework_question', sa.Column('omit', sa.Boolean))

def downgrade():
    op.drop_column('homework_question', 'give_back')
    op.drop_column('homework_question', 'omit')
