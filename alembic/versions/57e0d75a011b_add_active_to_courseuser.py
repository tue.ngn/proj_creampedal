"""add_active_to_courseuser

Revision ID: 57e0d75a011b
Revises: e55725cee1ee
Create Date: 2018-05-14 20:47:57.382997

"""

# revision identifiers, used by Alembic.
revision = '57e0d75a011b'
down_revision = 'e55725cee1ee'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('course_user', sa.Column('active', sa.Boolean))


def downgrade():
	op.drop_column('course_user', 'active')
