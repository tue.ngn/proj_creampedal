"""add order to homeworkquestion

Revision ID: e88eb5be679b
Revises: 57e0d75a011b
Create Date: 2018-05-19 04:38:02.851915

"""

# revision identifiers, used by Alembic.
revision = 'e88eb5be679b'
down_revision = '57e0d75a011b'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('homework_question', sa.Column('order', sa.Integer))


def downgrade():
	op.drop_column('homework_question', 'order')
