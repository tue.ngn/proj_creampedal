import numpy as np
import pandas as pd

import cv2
from imutils.perspective import order_points
from PIL import Image
import base64

import os
import io
import csv

from urllib.parse import unquote

import werkzeug as wz

import sys

import functools

hwprcnt = [None,3,2,4,5,6,7,8]
hwmxpnt = [1,1,1,1,1,1,1,1]
tstprcnt = [None, 0.8,1,0.7,0,0.6,1]
tstmxpnt = [1.2,1.2,1.2,1.2,1.2,1.2,1.2]
low = None
low_array = []

grade_range = (0.845, 0.745, 0.645, 0.545)

def replaceNone(num):
	return [(0 if a is None else a) for a in num]

class Grades():
	def __init__(self, percent,max_point):
		self.prcnt = np.asarray(percent, dtype=np.float)
		self.mxpnt = np.asarray(max_point, dtype=np.float)
	def points(self):
		self.pnt = [(a*b if a != None else None) for a,b in zip(self.prcnt,self.mxpnt)]
		return self.pnt
	def sum(self, dropnum=0):
		pnts = np.multiply(
			np.nan_to_num(self.prcnt),
			np.nan_to_num(self.mxpnt)
			)
		pnts = np.multiply(
			pnts,
			self.droplist(dropnum)
			)
		return sum(pnts)
	def droplist(self, dropnum):
		if self.prcnt.size > 1:
			lowpos = np.argpartition(np.nan_to_num(self.prcnt),dropnum)
			minlist = np.ones(self.prcnt.size)
			for x in range(dropnum):
				minlist[lowpos[x]] = 0
			return minlist.tolist()
		else:
			return [1]
	def maxpnt(self, dropnum = 0):
		droplist = np.multiply(
			np.nan_to_num(self.mxpnt),
			self.droplist(dropnum=dropnum)
			)
		return np.sum(droplist).tolist()
	def avg(self):
		return np.average(np.nan_to_num(self.prcnt))

class letterGrade():
	def __init__(self,a_range, b_range,c_range,d_range):
		self.a_range = a_range
		self.b_range = b_range
		self.c_range = c_range
		self.d_range = d_range
	def toLet(self, percent):
		if percent >= self.a_range:
			return 'A'
		elif percent >= self.b_range:
			return 'B'
		elif percent >= self.c_range:
			return 'C'
		elif percent >= self.d_range:
			return 'D'
		else:
			return 'F'

class TableMake():
	def __init__(self):
		pass

class RowMake():
	def __init__(self, cell = None):
		self.row_id = None
		self.row_class = []
		#self.row_text = None
		self.row_cell = []
		if cell:
			self.add_cell(cell)
	def add_class(self, *args):
		for x in args:
			self.row_class.append(x)
	def add_text(self, text):
		self.row_text = text
	def add_id(self, cell_id):
		self.row_id = cell_id
	def add_cell(self, cell):
		if isinstance(cell, list):
			for x in cell:
				self.row_cell.append(x)
		else:
			self.row_cell.append(cell)
	def __str__(self):

		class_text = (
			"class=\"{}\"".format(' '.join(
				str(x) for x in self.row_class))
			if self.row_class else ''
			)
		id_text = "id=\"{}\"".format(self.row_id) if self.row_id else ''
		row_cell = (
			''.join(str(x) for x in self.row_cell)
			if self.row_cell else ''
			)
		cell_html = (
			"<tr {} {}>{}</tr>".format(
				id_text,
				class_text,
				row_cell
				)
			)
		return cell_html



class CellMake():
	def __init__(self, text = None):
		self.cell_id = None
		self.cell_class = []
		#self.cell_text = None
		self.add_text(text)
	def add_class(self, *args):
		for x in args:
			self.cell_class.append(x)
	def add_text(self, text):
		self.cell_text = text
	def add_id(self, cell_id):
		self.cell_id = cell_id
	def __str__(self):
		class_text = (
			"class=\"{}\"".format(' '.join(
				str(x) for x in self.cell_class))
			if self.cell_class else ''
			)
		id_text = "id=\"{}\"".format(self.cell_id) if self.cell_id else ''
		cell_text = self.cell_text if self.cell_text else ''

		cell_html = (
			"<td {} {}>{}</td>".format(
				id_text,
				class_text,
				cell_text
				)
			)
		return cell_html

#courtesy of imutils, removed order_points. used a shift in detect circle
def four_point_transform(image, pts):
	# obtain a consistent order of the points and unpack them
	# individually
	#rect = order_points(pts)

	(tl, tr, br, bl) = pts

	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))

	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))

	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype="float32")

	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(pts, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

	# return the warped image
	return warped

#better to detect if image is already greyscale instead of except
def detectCircle(image):
	image = cv2.resize(image, (0,0), fx=0.5, fy=0.5  )
	try:
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	except:
		gray = image

	bilateral_filtered_image = cv2.bilateralFilter(gray, 5, 175, 175)
	#edge_detected_image = cv2.Canny(bilateral_filtered_image, 75, 200)
	thresh = cv2.threshold(bilateral_filtered_image, 0, 255,
		cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

	edge_detected_image = cv2.Canny(thresh, 75, 200)

	edgergb = Image.fromarray((edge_detected_image).astype(np.uint8))
	edgergb.save("edged.png")
	#cv2.imshow('Edge', edge_detected_image)
	#cv2.waitKey(0)

	(im2, contours, hierarchy)= cv2.findContours(edge_detected_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

	zipped = zip(contours,hierarchy[0])
	cnts = []
	heir = []

	'''
	for c in contours:
		# approximate the contour
		approx = cv2.approxPolyDP(c,0.01*cv2.arcLength(c,True),True)
		area = cv2.contourArea(c)
		if ((len(approx) > 8) & (area > 30) ):
			cnts.append(c)
	'''
	mark = 0
	child = []
	for num,c in enumerate(contours):
		# approximate the contour
		approx = cv2.approxPolyDP(c,0.01*cv2.arcLength(c,True),True)
		area = cv2.contourArea(c)
		if ((len(approx) > 8) & (area > 30) & (num not in child) ):
			cnts.append(c)

			k = num
			y = 0
			while (hierarchy[0][k][2] != -1):
				#print(hierarchy[0][k][2])
				k = hierarchy[0][k][2]
				y = y+1


			#print('contours heir#:{}'.format(y))
			if(y>=7):
				k=num
				while (hierarchy[0][k][2] != -1):
					k = hierarchy[0][k][2]
					child.append(k)
				D = num

			elif(y>=5):
				#cv2.drawContours(image, contours[x], -1, (0, 255, 0), 2)
				#print('contours heir#:{}'.format(y))
				k=num
				while (hierarchy[0][k][2] != -1):
					k = hierarchy[0][k][2]
					child.append(k)
				if (mark == 0):
					A = num
				elif  (mark == 1):
					B = num     # i.e., A is already found, assign current contour to B
				elif  (mark == 2):
					C = num     # i.e., A and B are already found, assign current contour to C
				mark = mark + 1
			#print(child)

	threecnts = []
	threecnts.append(contours[A])
	threecnts.append(contours[B])
	threecnts.append(contours[C])
	threecnts.append(contours[D])


	MA = cv2.moments(contours[A])
	cx = int(MA['m10']/MA['m00'])
	cy = int(MA['m01']/MA['m00'])
	MA_center = (cx,cy)
	#print('A center:{} {}'.format(cx, cy))
	MB = cv2.moments(contours[B])
	cx = int(MB['m10']/MB['m00'])
	cy = int(MB['m01']/MB['m00'])
	MB_center = (cx,cy)
	#print('B center:{} {}'.format(cx, cy))
	MC = cv2.moments(contours[C])
	cx = int(MC['m10']/MC['m00'])
	cy = int(MC['m01']/MC['m00'])
	MC_center = (cx,cy)
	#print('C center:{} {}'.format(cx, cy))
	MD = cv2.moments(contours[D])
	cx = int(MD['m10']/MD['m00'])
	cy = int(MD['m01']/MD['m00'])
	MD_center = (cx,cy)


	M = [MA_center,MB_center,MC_center,MD_center]
	#print(M)

	order = order_points( np.array(M))
	#print(order)
	MD_pos = 0
	for num,pnt in enumerate(order):
		if np.array_equal(pnt,MD_center):
			MD_pos = num
			#print('MD_pos:{}'.format(num))
			break


	shifted = np.roll(order, (2-MD_pos)*2)
	#print(shifted)

	#print('D center:{} {}'.format(cx, cy))

	#cv2.drawContours(image, (contours[A],contours[B],contours[C],contours[D]), -1, (0, 255, 0), 2)
	# cv2.drawContours(image, ([contours[A][4],contours[A][5],contours[A][6]]), -1, (0, 255, 0), 3)
	# cv2.drawContours(image, ([contours[A][7],contours[A][8],contours[A][8]]), -1, (0, 255, 0), 3)
	#cv2.imshow("image", image)
	#cv2.waitKey(0)
	print(shifted)
	return shifted
	'''
	return (
		MD_center,
		MC_center,
		MA_center,
		MB_center
		)
	'''


def cropMarker(file_image):
	image = file_image
	#image = cv2.imread(file_image)
	try:
		(MD_center,MC_center,MA_center,MB_center) = detectCircle(image)
	except:
		raise
		return 'Unable to find all markers'

	try:
		warpedarray = np.array([
					MD_center,
					MC_center,
					MA_center,
					MB_center
				])
	except:
		print('Unable to find markers'.format())
		return 'Unable to find markers'.format()

	image = cv2.resize(image, (0,0), fx=1, fy=1  )
	warped = four_point_transform(image, warpedarray)

	############return warped

	#if already grayscale, then except
	try:
		gray = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
	except:
		gray = warped

	graycrop = Image.fromarray((gray).astype(np.uint8))
	graycrop.save("graycrop.png")

	thresh = cv2.threshold(gray, 0, 255,
		cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

	threshrgb = Image.fromarray((thresh).astype(np.uint8))
	threshrgb.save("thresh.png")

	##### convert and save as svg
	height = thresh.shape[0]
	width = thresh.shape[1]
	string = cv2.imencode('.png', thresh)[1].tostring()
	base = base64.b64encode(string)
	f = open('path.svg', 'w+')
	f.write(
		'<svg xmlns="http://www.w3.org/2000/svg" \
		xmlns:xlink="http://www.w3.org/1999/xlink" \
		width="{}" height="{}" viewBox="0 0 {} {}">\n'.format(
			width,height,width,height
			)
		)
	f.write('\t<image width="{}" height="{}" xlink:href="data:image/png;base64,'.format(width, height))
	f.write(base.decode('utf-8'))

	f.write('"/>\n')
	f.write('</svg>')
	f.close()

class ProcessImage():
	def __init__(self, image):
		if isinstance(image, str):
			self.img = cv2.imread(image)
			#print('is a str')
		elif isinstance(image, wz.datastructures.FileStorage):
			npimg = np.fromstring(image.read(), np.uint8)
			self.img = cv2.imdecode(npimg, cv2.IMREAD_UNCHANGED)
			print('is a filestorage')
		else:
			print('file unreadable')
	def get_img(self):
		return self.img
	def make_gray(self):
		#is there a way to detect gray image? this is a work around
		try:
			self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
		except:
			pass
	@staticmethod
	def gray(image):
		return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	@staticmethod
	def bilateralFilter(image):
		return cv2.bilateralFilter(image, 5, 175, 175)
	def threshold(self,image):
		img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		return cv2.threshold(img, 0, 255,
			cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
	@staticmethod
	def canny(image):
		return cv2.Canny(image, 75, 200)
	@staticmethod
	def resize(image, fx = 1, fy = 1):
		return cv2.resize(image, (0,0), fx=fx, fy=fy)
	def save_png(self):
		img_save = Image.fromarray((self.img).astype(np.uint8))
		img_save.save("graycrop2.png")
	def get_img(self):
		return self.img
	def crop(self, four_points):
		self.img = four_point_transform(self.img, four_points)
	def save_svg(self):
		##### convert and save as svg
		img = self.threshold(self.img)
		height = img.shape[0]
		width = img.shape[1]
		string = cv2.imencode('.png', img)[1].tostring()
		base = base64.b64encode(string)
		f = open('path.svg', 'w+')
		f.write(
			'<svg xmlns="http://www.w3.org/2000/svg" \
			xmlns:xlink="http://www.w3.org/1999/xlink" \
			width="{}" height="{}" viewBox="0 0 {} {}">\n'.format(
				width,height,width,height
				)
			)
		f.write('\t<image width="{}" height="{}" xlink:href="data:image/png;base64,'.format(width, height))
		f.write(base.decode('utf-8'))

		f.write('"/>\n')
		f.write('</svg>')
		f.close()


class BubbleSheet(ProcessImage):
	def __init__(self, image):
		ProcessImage.__init__(self, image)

class DetectCircle_():
	@classmethod
	def img_prep(cls, image, resize = 1):
		img = image
		img = ProcessImage.gray(img)
		img = ProcessImage.bilateralFilter(img)
		img = ProcessImage.canny(img)
		img = ProcessImage.resize(img,fx=resize,fy=resize)
		return img
	@classmethod
	def find(cls, image):
		(im2, contours, hierarchy)= cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
		three = []
		key = None
		#zipped = zip(contours,hierarchy[0])
		#cnts = []
		#heir = []
		mark = 0
		child = []
		for num,c in enumerate(contours):
			# approximate the contour
			approx = cv2.approxPolyDP(c,0.01*cv2.arcLength(c,True),True)
			area = cv2.contourArea(c)
			if ((len(approx) > 8) & (num not in child) ): #& (area > 30) no need to exclude small area?
				#cnts.append(c)
				k = num
				y = 0
				while (hierarchy[0][k][2] != -1):
					#print(hierarchy[0][k][2])
					k = hierarchy[0][k][2]
					y = y+1
				#print('contours heir#:{}'.format(y))
				if(y>=7):
					k=num
					while (hierarchy[0][k][2] != -1):
						k = hierarchy[0][k][2]
						child.append(k)
					key = cls.calc_center(contours[num])
				elif(y>=5):
					#cv2.drawContours(image, contours[x], -1, (0, 255, 0), 2)
					#print('contours heir#:{}'.format(y))
					k=num
					while (hierarchy[0][k][2] != -1):
						k = hierarchy[0][k][2]
						child.append(k)
					if (mark == 0):
						three.append(cls.calc_center(contours[num]))
					elif  (mark == 1):
						three.append(cls.calc_center(contours[num]))
					elif  (mark == 2):
						three.append(cls.calc_center(contours[num]))
					mark = mark + 1
		return (three,key)
	@staticmethod
	def calc_center(contour):
		MA = cv2.moments(contour)
		cx = int(MA['m10']/MA['m00'])
		cy = int(MA['m01']/MA['m00'])
		return (cx,cy)

	#order funct put center in order with key as 3rd point
	def order(three,key):
		centers = three
		centers.append(key)
		centers = order_points(np.array(centers))
		for num,pnt in enumerate(centers):
			if np.array_equal(pnt,key):
				key_pos = num
				break
		centers = np.roll(centers, (2-key_pos)*2)
		return centers

	@classmethod
	def get_points(cls, image, resize = 1):
		img = cls.img_prep(image,resize)
		(three,key) = cls.find(img)
		centers = cls.order(three,key)
		centers = np.divide(centers,resize)
		return centers
	def process(self,image_dir):
		self.img_prep(image_dir)
		self.find()
		self.order()
		return self.four_cent

#plug in request.files?
#plug in allowed file extension
def file_check(file, ext):
	if 'file' not in file:
		pass #break some how? #return redirect(request.url)
	if file == '':
		pass #break some how?
	if not allow_files(ext, file):  #make new fuction
		pass #break some how?

csv_string = """text,with,Polish,non-Latin,lettes
1,2,3,4,5,6
a,b,c,d,e,f
gęś,zółty,wąż,idzie,wąską,dróżką,
"""
stream = io.StringIO(csv_string)
reader = csv.DictReader(stream, delimiter=',')


def debug(func):
	"""Print the function signature and return value"""
	@functools.wraps(func)
	def wrapper_debug(*args, **kwargs):
		args_repr = [repr(a) for a in args]                      # 1
		kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
		signature = ", ".join(args_repr + kwargs_repr)           # 3
		print(f"Calling {func.__name__}({signature})")
		value = func(*args, **kwargs)
		print(f"{func.__name__!r} returned {value!r}")           # 4
		return value
	return wrapper_debug

def parse_csv(file_csv):
	stream = io.StringIO(request.files["file"].stream.read().decode("UTF8"))
	csv_input = csv.DictReader(stream)
	return csv_input

def add_user(school_id, last, first):
	print(school_id, last, first)

def extract_csv(funct, csv, *args):
	for row in csv:
		#print('this is row: {}'.format(row))
		things = extract_row(row, *args)
		#print(things)
		funct(*things)

def extract_row(row, *args):
	list_ = []
	#print(args)
	for arg in args:
		#print('this is arg: {}'.format(row[str(arg)]))
		list_.append(row[str(arg)])
	#print(list_)
	return tuple(list_)

def print_(*args):
	print(args)

dict_1 = {
	"P1": 'p1',
	"P2": 'p2',
	"P3": 'p3'
}

dict_2 = {
	"P1": 'p1',
	"P2": 'x2',
	"P3": 'x3'
}

dict_3 = {
	"P1": functools.partial(print_, 'stuff1'),
	"P2": functools.partial(print_, 'stuff2'),
	"P3": functools.partial(print_, 'stuff3')
}

@debug
def map_dict(dict_a,dict_b):
	for key in dict_a:
		dict_b[key](dict_a[key])




if __name__ == '__main__':
	set_1 = set(dict_1.items())
	set_2 = set(dict_2.items())
	new_dict = dict(set_2 - set_1)
	#print(new_dict)
	map_dict(new_dict, dict_3)

	df = pd.DataFrame([[1, 2], [3, 4]], columns=list('AB'))
	df2 = pd.DataFrame([[5, 6], [7, 8]], columns=list('AB'))
	df = df.append(df2)

	#print(df)
	#extract_csv(add_user, reader, 'text', 'with', 'Polish')

	'''
	test = ProcessImage('board2.jpg')
	points = DetectCircle_.get_points(test.get_img(), resize = 0.6)
	test.crop(points)
	test.save_svg()
	'''

	#testdetectcircle.save_png()


	'''
	test = Grades(hwprcnt,hwmxpnt)
	print('points:{}'.format(test.points()))
	print('sum:{}'.format(test.sum(dropnum = 1)))
	print('droplist:{}'.format(test.droplist(1)))
	print('maxpnt:{}'.format(test.maxpnt(dropnum = 1)))
	print('avg:{}'.format(test.avg()))

	(a,b,c,d) = grade_range
	test2 = letterGrade(a,b,c,d)
	print('Letter Grade:{}'.format(test2.toLet(0.9)))
	'''
	'''
	test = []
	if test:
		print('test is there')
	else:
		print('test not there')
	rowtest = RowMake()
	rowtest.add_id('rowid')
	rowtest.add_class('rowclass')
	for x in hwprcnt:
		celltest = CellMake()
		celltest.add_class('data', 'homework{}'.format(x))
		celltest.add_text(x)
		celltest.add_id('cellid123{}'.format(x))
		rowtest.add_cell(celltest)
	print(rowtest)

	rowtest2 = RowMake()
	print(rowtest2)
	'''
	#print(cropMarker(cv2.imread('board2.jpg')))

	#testbub = BubbleSheet('board2.jpg')
	#print(testbub.get_img())

	'''
	np.random.seed(24)
	df = pd.DataFrame({'A': np.linspace(1, 10, 10)})
	df = pd.concat([df, pd.DataFrame(np.random.randn(10, 4), columns=list('BCDE'))],
				   axis=1)
	df.iloc[0, 2] = np.nan


	def color_negative_red(val):
		"""
		Takes a scalar and returns a string with
		the css property `'color: red'` for negative
		strings, black otherwise.
		"""
		color = 'red' if val < 0 else 'black'
		return 'color: %s' % color

	style = df.style.applymap(color_negative_red).render()
	print(style)
	text = df.to_html()
	print(text)
	'''
