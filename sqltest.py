
'''
from flask import Flask, request, flash, url_for, redirect, render_template,\
		 Response, jsonify, current_app, send_file
import flask
'''
from flask import request, flash, url_for, redirect, render_template,\
		 Response, jsonify, current_app, send_file
from init_flask import app


from dbconn import session, Base, engine #, session2, engine2
import pandas as pd
#import plotly as py
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean,\
	SmallInteger, Text, Table, PrimaryKeyConstraint, UniqueConstraint,\
	 ForeignKeyConstraint, MetaData, DateTime, and_, or_, Float
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from wtforms import Form, TextField, validators, ValidationError, \
	SubmitField, RadioField, FieldList, FormField, BooleanField, StringField,\
	TextAreaField, PasswordField, SelectField, DateTimeField

from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user

from numpy import genfromtxt
import numpy as np
from scipy import stats

import csv

#from flask_alchemydumps import AlchemyDumps, AlchemyDumpsCommand , create
import flask_alchemydumps as aldump   #made changes and turned it into a local file
from flask_alchemydumps import AlchemyDumps
from flask_script import Manager

from sqlalchemy.ext.serializer import dumps, loads

from sqlalchemy.inspection import inspect

import datetime 
import time
import pytz

from functools import wraps

import copy

#from flask_wkhtmltopdf import Wkhtmltopdf

import os
import io

import json

from ar_markers import HammingMarker, detect_markers
from scipy.ndimage import zoom
from PIL import Image
import cv2
import math
import pyqrcode
from pyzbar.pyzbar import decode
from imutils.perspective import four_point_transform

static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)))

import statistics

from functions import Grades, letterGrade, CellMake, RowMake


def con_check():
	#print('attempting to connect...')
	while True:
		try:
			engine.connect()
			print('connected')
			break
		except:
			print('unable to connect')
		finally:
			#pass
			session.close()

#to insure connection.
def connection_check(func):
	#print('connection_check')
	def Wrapper(*args):
		con_check()
		return func(*args)
	return Wrapper

#wkhtmltopdf = Wkhtmltopdf(app)

class Alembic(Base):

	__tablename__  	= 'alembic_version'
	version_num = Column(String, primary_key=True, unique=True)


class Users(UserMixin, Base):

	__tablename__  	= 'users'
	id = Column(Integer, primary_key=True, unique=True)
	password 	= Column(String(80))
	school_id 	= Column(Integer)
	first_name 	= Column(String(20))
	last_name 	= Column(String(20))
	user_type 	= Column(Integer)
	#course 		= relationship('Courses', secondary=course_user, back_populates="user",lazy="dynamic")
	course_user = relationship('CourseUser', back_populates="user", lazy="dynamic")
	selection 	= relationship('StudentSelection', lazy="dynamic") 
	'''
	def __init__(self, school_id, last_name, first_name, user_type, password=None):
	#def __init__(self, school_id):
		self.school_id = school_id
		self.first_name = first_name
		self.last_name = last_name
		self.user_type = user_type
		self.password = password
	'''

	@classmethod
	@connection_check
	def get(cls, id):
		try:
			return session.query(cls).filter_by(school_id=id).first()
		except:
			return 'error in users.get'
		finally:
			session.close()

#@connection_check
class Courses(Base):
	__tablename__  	= 'courses'    
	id             	= Column(Integer, primary_key=True)
	name 			= Column(String(20))
	description		= Column(Text)
	#variable name
	#user 			= relationship('Users', secondary=course_user, back_populates="course", lazy="dynamic")
	course_user		= relationship('CourseUser', back_populates="course", lazy="dynamic")
	homework 		= relationship('Homework')

	def __init__(self, name, description):
		self.name 			= name
		self.description 	= description

	@classmethod
	@connection_check
	def get(cls, id):
		try:
			return session.query(cls).filter_by(id=id).first()
		except:
			return 'error in Courses.get'
		finally:
			session.close()

	@connection_check
	def get_users(self, includeInactive = 0):
		try:
			crsu = session.query(CourseUser).filter(and_(
				CourseUser.course_id == self.id,
				or_(CourseUser.active == 1, includeInactive == 1 )
				))
			usrs = session.query(Users).filter(
				Users.id.in_(usr.user_id for usr in crsu)
				)
			return usrs
		except:
			raise
			return 'error in Courses.get_users'
		finally:
			session.close()

class Homework(Base):
	__tablename__  	= 'homework'
	id             	= Column(Integer, primary_key=True, nullable = False)
	course_id		= Column(Integer, ForeignKey('courses.id'))
	description		= Column(Text) 
	due_date		= Column(DateTime)
	assgnmt_typ		= Column(Integer)
		#secondary is variable name  	
	#question 		= relationship('Question', secondary = homework_question, lazy="dynamic")
	homework_question	= relationship('HomeworkQuestion', back_populates="homework", lazy="dynamic")
	selection 		= relationship('StudentSelection')
	course 			= relationship('Courses')

	def __init__(self, description, date, assgntyp):
		self.description 	= description
		self.due_date 		= date
		self.assgnmt_typ	= assgntyp

	@classmethod
	def get(cls, id):
		try:
			return session.query(cls).filter_by(id=id).first()
		except:
			return 'error in Homework.get'
		finally:
			session.close()

	def get_questions(self):
		try:
			hwqs = session.query(HomeworkQuestion).filter_by(
				homework_id = self.id
				)
			qs = session.query(Question).filter(
				Question.id.in_(hwq.question_id for hwq in hwqs)
				)
			return qs
		except:
			raise
			return 'error in Homework.get_questions'
		finally:
			session.close()

class Question(Base):
	__tablename__  	= 'question'    
	id             	= Column(Integer, primary_key=True) 
	question_type	= Column(Integer) 
	description		= Column(Text) 
	multiplec 		= relationship('MultipleC',
		cascade="all,delete",
		back_populates='question',
		uselist=False
	)  
	#still not sure how uselist works. uselist mean it can be a list? dont add using append
	selection 	= relationship('StudentSelection',
		lazy="dynamic",
		cascade="all,delete",
	) #lazy is not needed if i create selection all at one with init 
	#dont need
	#homework 		= relationship('homework_question', back_populates="question", lazy="dynamic")
	homework_question	= relationship('HomeworkQuestion',
		back_populates="question", 
		cascade="all,delete" 
	)
	
	def __init__(self, type, text):
		self.question_type 	= type
		self.description 	= text

class MultipleC(Base):
	__tablename__  	= 'multiplec'    
	id             	= Column(Integer, primary_key=True) 
	question_id		= Column(Integer, ForeignKey('question.id')) 
	question		= relationship('Question', back_populates='multiplec')
	text			= Column(Text)
	choice 			= relationship('Choice', cascade="all,delete",) 

	def __init__(self, text):
		self.text 	= text

class Choice(Base):
	__tablename__  	= "choice"    #matches the name of the actual database table
	id             	= Column(Integer, primary_key=True)
	multiplec_id	= Column(Integer, ForeignKey('multiplec.id'))
	text			= Column(Text) 
	order 			= Column(SmallInteger)                                   
	true_or_false	= Column(Boolean, default=False)  

	#selection 	= relationship('StudentSelection')	 
 
	def __init__(self, text, order, bool):
		self.text = text
		self.order = order
		self.true_or_false = bool

'''(tables with foreignkeys as primary keys
 must be placed last for alchemydump to order and merge correctly)'''

#make assoce table an object to get session.merge working correctly
class CourseUser(Base):
	__tablename__  	= 'course_user'    
	course_id       = Column(Integer, ForeignKey('courses.id'), primary_key=True) 
	user_id			= Column(Integer, ForeignKey('users.id'), primary_key=True)
	user			= relationship('Users')
	course			= relationship('Courses')
	active			= Column(Boolean, default=True)

	def __init__(self, course, user):
		self.course_id = course.id
		self.user_id = user.id


class HomeworkQuestion(Base):
	__tablename__  	= 'homework_question'    
	homework_id     = Column(Integer, ForeignKey('homework.id'), primary_key=True) 
	question_id		= Column(Integer, ForeignKey('question.id'), primary_key=True)
	homework		= relationship('Homework')
	question		= relationship('Question')
	give_back 		= Column(Boolean)
	omit 			= Column(Boolean)

	def __init__(self, homework, question):
		self.homework_id = homework.id
		self.question_id = question.id


#need to put max_point onto Homework
class AssgnmntGrade(Base):
	__tablename__  	= "assgnmnt_grade"
	homework_id		= Column(Integer, ForeignKey('homework.id'), primary_key=True)
	user_id         = Column(Integer, ForeignKey('users.id'), primary_key=True)
	percent_score	= Column(Float)
	max_point		= Column(Float)

	homework		= relationship('Homework')
	users			= relationship('Users')

#delete right_or_wrong
class StudentSelection(Base):
	__tablename__  	= "selection"    #matches the name of the actual database table
	#id				= Column(Integer, primary_key=True, autoincrement=True)
	user_id         = Column(Integer, ForeignKey('users.id'), primary_key=True)
	homework_id		= Column(Integer, ForeignKey('homework.id'), primary_key=True)#, primary_key=True)
	question_id		= Column(Integer, ForeignKey('question.id'), primary_key=True)#, primary_key=True)
		  #id				= Column(Integer, primary_key=True)
	selection 		= Column(Integer)  
	#slctn			= Column(Integer,ForeignKey('choice.id') )                                 
	right_or_wrong	= Column(Boolean, default=False)
	
	users			= relationship('Users', uselist=False) #, back_populates='selection')  
	homework		= relationship('Homework', uselist=False)  #uselist=False, backref='memberships', lazy='dynamic')
	question		= relationship('Question', uselist=False)

	def __init__(self, user, homework, question, select):
		self.user_id = user.id
		self.homework_id = homework.id
		self.question_id = question.id
		self.selection = select.id
		self.right_or_wrong = select.true_or_false

	@classmethod
	def get_hw_ss(cls,usr_id,hwqs, ifNone, let_choice = 1):
		ss_array = []
		try:
			for hwq in hwqs:
				ss = session.query(cls).filter(and_(
				cls.user_id== usr_id, 
				cls.question_id == hwq.id
				)).first()
				ss_array.append(
					(choice_order(ss.selection) if let_choice else ss.selection) 
					if ss else ifNone
					)
			return ss_array
		except:
			#raise
			return 'error in StudentSelection.get_hw_ss'
		finally:
			session.close()		

'''
class Attendance(Base):
	__tablename__  	= "attendance"
	date_day 		= Column(Date, primary_key=True)
	course_id       = Column(Integer, ForeignKey('courses.id'), primary_key=True) 
	user_id			= Column(Integer, ForeignKey('users.id'), primary_key=True)
	#add relatoin to course and user.
'''

class LoginForm(Form):
	school_id = TextField("Student ID",[validators.Required("Please enter your Student ID.")])
	submit = SubmitField("Send")

class AdminLoginForm(Form):
	school_id = TextField("School ID",[validators.Required("Please enter your School ID.")])
	password = PasswordField("Password",[validators.Required("Please enter your Password.")])
	submit = SubmitField("Send")       

class QuestionInput(Form):
	#test    = TextField("test text")
	text 	= TextAreaField("What is the question?", [validators.Required("Please enter question.")])
	choice1 = TextField("Choice 1", [validators.Required("Please enter choice.")])
	choice2 = TextField("Choice 2", [validators.Required("Please enter choice.")])
	choice3 = TextField("Choice 3", [validators.Required("Please enter choice.")])
	choice4 = TextField("Choice 4", [validators.Required("Please enter choice.")])
	submit 	= SubmitField("Send")

class MultipleChoice(Form):
	pass

class BlankForm(Form):
	pass

class OmitForm(Form):
	pass

class GiveBackForm(Form):
	pass

#removed?
class crtassgnmnt(Form):
	submit 	= SubmitField("create")
	tsthw	= RadioField('testorhomework', choices = [('Homework', 1), ('Test', 2)])

#removed
class DateTime(Form):
	dttm = DateTimeField(
		"duedate", format="%Y-%m-%dT%H:%M:%S",
		#default=datetime.today, ## Now it will call it everytime.
		#validators=[validators.DataRequired()]
	)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

@login_manager.unauthorized_handler
def unauthorized_handler():
	return 'Unauthorized'

#role: -1 = anyone; 0 = Admin; 2 = student
def login_required(type=-1):
	def wrapper(fn):
		@wraps(fn)
		def decorated_view(*args, **kwargs):
			while True:
				try:
					engine.connect()
					break
				except:
					print('unable to connect')
				finally:
					session.close()
			if not current_user.is_authenticated:
				return redirect(url_for('login'))

			utype = current_user.user_type
			if ( (utype != type) and (type != -1)):
				print('error in login_required')
				return login_manager.unauthorized()      
			return fn(*args, **kwargs)
		return decorated_view
	return wrapper

#checks to see if student is in course or the course page
def course_check():
	def wrapper(fn):
		@wraps(fn)
		def decorated_view(idnum):
			#print('course check')

			try:
				user = session.query(Users).filter_by(id=current_user.id).first()
				if(int(idnum) not in [r.course_id for r in user.course_user]):
					print('error in course_check')
					return login_manager.unauthorized()			
			except:
				return 'course_check error'
			finally:
				session.close()

			return fn(idnum)
		return decorated_view
	return wrapper

#checks to see if student is in courseID, -1 = no one can enter
def hw_course_check():
	def wrapper(fn):
		@wraps(fn)
		def decorated_view(idnum):
			try:
				homework = session.query(Homework).filter_by(id=idnum).first()
				user = session.query(Users).filter_by(id=current_user.id).first()
				if not homework:
					return login_manager.unauthorized()
				if (homework.course_id not in [r.course_id for r in user.course_user]):
					return login_manager.unauthorized()
			except:
				return 'error in hw_course_check'
			finally:
				session.close()
			return fn(idnum)
		return decorated_view
	return wrapper




#not sure why i need this anymore
'''
@app.before_request
def before_request():
	flask.session.permanent = False     #I dont think this is working. Still logged in after closing browser
	app.permanent_session_lifetime = datetime.timedelta(minutes=20)
	flask.session.modified = True
	flask.g.user = current_user
'''

#used for user_id stored in session. Close browser = lose login
@login_manager.user_loader
def user_loader(id):
	try:
		user = session.query(Users).filter_by(id=id).first()
		return user
	except:
		return None
	finally:
		session.close()

@app.route('/login', methods=['GET', 'POST'])
def login():
	while True:
		#print('connecting...')
		try:
			engine.connect()
			break
		except:
			print('unable to connect')
		finally:
			session.close()

	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = LoginForm(request.form)
	if request.method == 'GET':
		return render_template('login.html', form = form)
	if form.validate():
		school_id = request.form['school_id']
		try:
			if session.query(Users).filter_by(school_id=school_id).first():
				user = session.query(Users).filter_by(school_id=school_id).first()
				if user.user_type != 2:
					return 'invalid user type'
				else:
					print(user.first_name+user.last_name)
					login_user(user,
						remember = True,
						force = True						
					) #, duration = datetime.timedelta(minutes=20)  doesnt work on pythonanywhere
					next = request.args.get('next')
				'''
				if not is_safe_url(next):
					return flask.abort(400)
				'''
				return redirect(next or url_for('index'))
			else:
				return 'Student ID not found.'
		except:
			raise
			return 'error in login'
		finally:
			session.close()

	return render_template('login.html', form = form)


#@connection_check
@app.route("/adminlogin", methods=['GET', 'POST'])
def adminlogin():
	if current_user.is_authenticated:
		return redirect(url_for('admin'))
	form = AdminLoginForm(request.form)
	if request.method == 'GET':
		return render_template('adminlogin.html', form = form)
	if form.validate():
		school_id = request.form['school_id']
		password = request.form['password']
		try:
			if session.query(Users).filter_by(school_id=school_id).first():
				user = session.query(Users).filter_by(school_id=school_id).first()
				if password == user.password:
					login_user(user, remember = False, force = True ) #, duration = datetime.timedelta(minutes=20)  doesnt work on pythonanywhere
					next = request.args.get('next')
					return redirect(next or url_for('admin'))				
					'''
					if not is_safe_url(next):
						return flask.abort(400)
					'''
				else:
					return 'invalid password'
			else:
				return 'ID not found.'
		except:
			return 'error in login'
		finally:
			session.close()
	return render_template('adminlogin.html', form = form)	



@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('login'))

@app.route('/')
@login_required(type= -1)
def index():

	#user_courses = [r.course_id for r in current_user.course_user]
	try:
		user = session.query(Users).filter_by(id=current_user.id).first()
		user_courses = [r.course_id for r in user.course_user]	
		if len(user_courses) == 1:
			return redirect(url_for('course', idnum = user_courses[0]))
		else:
			return redirect(url_for('courses'))
	except:
		return 'error in index'
	finally:
		session.close()

@app.route('/admin', methods=['GET', 'POST'])
@login_required(type= 0)
def admin():
	return render_template('admin.html', user = current_user)
	'''
	route_list = []
	for rule in app.url_map.iter_rules():
		if (rule.endpoint != 'bootstrap.static' 
			and rule.endpoint != 'static' 
			and rule.endpoint != 'number'):
			#print(str(rule.endpoint))
			route_list.append(str(rule.endpoint))
	return render_template('index.html', user = current_user, route = route_list)
	'''
@app.route('/admincourse')
@login_required(type= 0)
def admincourse():
	allcrs = session.query(Courses).all()
	return render_template('admincourse.html', allcrs = allcrs)

@app.route('/hwrevise/<idnum>', methods=['GET', 'POST'])
@login_required(type= 0)
def hwrevise(idnum):
	zipped = []
	info = []
	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		answer_array = []

		for hwquest in homework.homework_question:
			choice_array = []
			true_choice = None
			choiceid = None
			
			for choice in hwquest.question.multiplec.choice:
				choice_array.append((str(choice.id),choice.text))
				if choice.true_or_false:
					true_choice = choice.order
					choiceid = choice.id
			if true_choice is None:
				#print(hwquest.question.multiplec.id)
				answer_array.append('NA')
			else:
				answer_array.append(num_to_let(true_choice))


			setattr(MultipleChoice, 'q'+str(hwquest.question.id), 
				RadioField(hwquest.question.multiplec.text, 
					choices = choice_array, 
					default = choiceid))
			setattr(OmitForm, 'omt'+str(hwquest.question.id), 
				BooleanField('omt'+str(hwquest.question.id),
					default = hwquest.omit)
			)
			setattr(GiveBackForm, 'gvbck'+str(hwquest.question.id), 
				BooleanField('gvbck'+str(hwquest.question.id),
					default = hwquest.give_back)
			)

		omtform = OmitForm(request.form)
		gvbckform = GiveBackForm(request.form)
		ddate = homework.due_date
		#print(ddate.isoformat())
	except:
		return 'error in creating homework revision'
	finally:
		session.close()
	

	form = MultipleChoice(request.form)
	zipped = list(zip(form, answer_array, omtform, gvbckform))

	#clears form. need better method
	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		for hwquest in homework.homework_question:
			delattr(MultipleChoice, 'q'+str(hwquest.question.id))
			delattr(OmitForm, 'omt'+str(hwquest.question.id))
			delattr(GiveBackForm, 'gvbck'+str(hwquest.question.id))
	except:
		return 'error in clearing forms'
	finally:
		session.close()

	if request.method == 'POST':
		try:
			homework = session.query(Homework).filter_by(id=idnum).first()
			for hwquest in homework.homework_question:
				try:
					choiceid = None
					question = session.query(Question).filter_by(
						id=hwquest.question_id).first()
					choice_select = session.query(Choice).filter_by(
						id=request.form['q'+str(hwquest.question_id)]).first()
					for choice in hwquest.question.multiplec.choice:
						if choice.true_or_false:
							choiceid = choice.id
					if choiceid != choice_select.id:
						for choice in hwquest.question.multiplec.choice:
							choice.true_or_false = False
						choice_select.true_or_false = True
					info.append('answer key updated')
				except:
					pass

				try:
					request.form['omt'+str(hwquest.question_id)]
					hwquest.omit = True
				except:
					hwquest.omit = False
				try:
					request.form['gvbck'+str(hwquest.question_id)]
					hwquest.give_back = True
				except:
					hwquest.give_back = False

				dttm = request.form['datetime']
				homework.due_date = dttm

				session.commit()

			return render_template('hwrevise.html', zipped = zipped, 
				answer = answer_array, info = info, ddate= (ddate.isoformat() if ddate else None))
		except:
			session.rollback()
			raise
			return Response('error in homework input')
		finally:
			session.close()

	else:
		#print(zipped[0][0].default)
		return render_template(
			'hwrevise.html',
			zipped = zipped,
			ddate= (ddate.isoformat() if ddate else None)
		)	

@app.route('/alchdump', methods=['GET', 'POST'])
@login_required(type= 0)
def alchdump():
	ad = AlchemyDumps(Base, session, app.root_path)

	if request.method == 'GET':
		return render_template('alchdump.html', text = ad.text)
	if request.method == 'POST':
		filenum = request.form['filenum']
		if request.form['btn'] == 'History':
			ad.history()
			return render_template('alchdump.html', text = ad.text)
		if request.form['btn'] == 'Backup':
			ad.create()
			return render_template('alchdump.html', text = ad.text)
		if request.form['btn'] == 'Restore':
			ad.restore(filenum)
			return render_template('alchdump.html', text = ad.text)
	else:
		return 'error in alchdump'


####need to make
@app.route('/courses')
@login_required(type= 2)
def courses():
	#user_courses = [r.course_id for r in current_user.course_user]
	return 'This page is not avaliable at the moment' #render_template('courses.html', user = current_user)


@app.route('/questioninput', methods=['GET','POST'])
@login_required(type= 0)
def questioninput():
	form = QuestionInput(request.form)
	setattr(form.text, 'render_kw', {'onkeyup': 'Preview.Update()'})
	#setattr(form.text, 'default', "text text 2")
	#form.text.data = "test text"
	#form.choice1.data = "test text"

	if request.method == 'POST':
		if form.validate():
			print('input received')
			newquestion = Question(1, "needs description")			
			newmc = MultipleC(request.form['text'])
			newchoice1 = Choice(request.form['choice1'], 0, True)
			newchoice2 = Choice(request.form['choice2'],1, False)
			newchoice3 = Choice(request.form['choice3'],2, False)
			newchoice4 = Choice(request.form['choice4'],3, False)

			newmc.choice.append(newchoice1)
			newmc.choice.append(newchoice2)
			newmc.choice.append(newchoice3)
			newmc.choice.append(newchoice4)
			newquestion.multiplec = newmc

			session.add(newquestion)
			session.commit()
			session.close()
			return render_template('questioninput.html', form = form, stuff="hello")

		else:
			print('input failed')
			return render_template('questioninput.html', form = form)

	else:
		print('first')		
		return render_template('questioninput.html', form = form, stuff="hello")
 
@app.route('/create_assgnmnt', methods=['GET', 'POST'])
@login_required(type= 0)
def create_assgnmnt():
	form = crtassgnmnt(request.form)
	allcrs = session.query(Courses).all()
	text = []
	now = datetime.datetime.now().replace(microsecond=0).isoformat()

	if request.method == 'GET':
		return render_template('crtassgnmnt.html',
			allcrs = allcrs,
			text = text,
			form = form,
			now = now
		)
	elif request.method == 'POST':

		courseid = request.form['courseid']
		assgntyp = request.form['assgntyp']
		numq = request.form['numq']
		dttm = request.form['datetime']
		dscrptn = request.form['dscrptn']
		qdscrpt = request.form['qdscrpt']

		try:
			if (int(assgntyp) == 1) or (int(assgntyp) == 2):
				#print(assgntyp)
				hw = Homework(dscrptn, dttm, assgntyp)
				session.add(hw)

				crse = session.query(Courses).filter_by(id = courseid).first()
				crse.homework.append(hw)

				for x in range(int(numq)):
					q = Question(1, qdscrpt)
					mc = MultipleC('none')
					session.add(q)
					session.add(mc)
					session.commit()
					q.multiplec=mc
					for y in range(4):
						c = Choice('none',y,False)
						q.multiplec.choice.append(c)

					hq = HomeworkQuestion(hw,q)
					session.add(hq)
			elif (int(assgntyp) == 3):
				hw = Homework(dscrptn, dttm, assgntyp)
				session.add(hw)

				crse = session.query(Courses).filter_by(id = courseid).first()
				crse.homework.append(hw)
			else:
				return "error creating assignment"
			session.commit()

		except:
			session.rollback()
			raise
			return 'error on create_assgnmnt'
		finally:			
			session.close()
		return 'assignment created'
	else:
		return 'error in create_assgnmnt'

@app.route('/showquestions')
@login_required(type= 0)
def showquestions():
	qs = session.query(Question).all()
	for q in qs:
		choice_array = []
		for choice in q.multiplec.choice:
			choice_array.append((str(choice.id),choice.text))
		setattr(
			MultipleChoice,
			'q'+str(q.id), 
			RadioField(
				q.multiplec.text, 
				choices = choice_array
			)
		)

	form = MultipleChoice(request.form)

	#clears form. need better method
	try:
		for q in qs:
			delattr(MultipleChoice, 'q'+str(q.id))
	except:
		return 'error in clearing MultipleChoice form'
	finally:
		session.close()

	return wkhtmltopdf.render_template_to_pdf('showquestions.html', download=True, save=False,form= form)
	#return render_template('showquestions.html', form= form)# questions = questions,

@app.route('/reviseq/<qnum>', methods=['GET','POST'])
@login_required(type= 0)
def reviseq(qnum):
	q = session.query(Question).filter_by(id=qnum).first()

	setattr(
		BlankForm,
		'qtext', 
		TextAreaField(
			'Question', 
			default = q.multiplec.text
		)
	)
	form = BlankForm(request.form)

	for choice in q.multiplec.choice:
		setattr(
			MultipleChoice,
			'c'+str(choice.id),
			TextAreaField(
				'Choice', 
				default = choice.text
			)			
		)
	mc = MultipleChoice(request.form)

	#clears form. need better method
	try:
		for choice in q.multiplec.choice:
			delattr(MultipleChoice, 'c'+str(choice.id))
	except:
		return 'error in clearing MultipleChoice form'

	if request.method == 'POST':
		#if request.form['btn'] == 'Preview':
	
		choice_array = []
		for choice in q.multiplec.choice:
			choice_array.append((str(choice.id),request.form['c'+str(choice.id)]))

		setattr(
			MultipleChoice,
			'q'+str(q.id),
			RadioField(
				request.form['qtext'], 
				choices = choice_array
			)			
		)
		prevmc = MultipleChoice(request.form)

		#clears form. need better method
		try:
			delattr(MultipleChoice, 'q'+str(q.id))
		except:
			return 'error in clearing MultipleChoice form'

		
		if request.form['btn'] == 'Save':
			q.multiplec.text = request.form['qtext']
			for choice in q.multiplec.choice:
				choice.text = request.form['c'+str(choice.id)]
			try:
				session.commit()
			except:
				session.rollback()
			finally:
				session.close()
			return render_template('reviseq.html', form= form, mc = mc, prevmc = prevmc)
		return render_template('reviseq.html', form= form, mc = mc, prevmc = prevmc)
	return render_template('reviseq.html', form= form, mc = mc)


@app.route('/homework/<idnum>', methods=['GET','POST'])
@login_required(type= 2)
@hw_course_check()
def homework(idnum):
	disable = False
	now = datetime.datetime.now().astimezone(pytz.utc)	
	grace = datetime.timedelta(hours = 8)
	zipped = []
	message = []

	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		due = homework.due_date.astimezone(pytz.utc)
		duegrace = due+grace
		info = {'point':0, 'total':0}
		if now > duegrace:
			disable = True
		answer_array = []
		omit_array = []
		crrct_array = []
		gvbck_array	= []

		for hwquest in homework.homework_question:
			choice_array = []

			true_choice = None

			if not hwquest.omit:
				info['total'] += 1

			userselect = session.query(StudentSelection).filter(and_(
				StudentSelection.user_id == current_user.id,
				StudentSelection.homework_id == homework.id,
				StudentSelection.question_id == hwquest.question_id
				)).first()

			for choice in hwquest.question.multiplec.choice:
				choice_array.append((str(choice.id),choice.text))
				if choice.true_or_false:
					true_choice = choice.order
			if true_choice is None:
				answer_array.append('NA')
			else:
				answer_array.append(num_to_let(true_choice))

			if userselect:
				slctn = userselect.selection
				chc = session.query(Choice).filter_by(id = userselect.selection).first()
				crrct_array.append(chc.true_or_false)
				if chc.true_or_false and not hwquest.omit:
					info['point'] += 1
				#process give back
				if hwquest.give_back == 1:
					info['point'] += 1
			else:
				crrct_array.append(False)
				slctn = None

			omit_array.append(hwquest.omit)
			gvbck_array.append(hwquest.give_back)

			setattr(MultipleChoice, 'q'+str(hwquest.question.id), 
				RadioField(hwquest.question.multiplec.text, 
					choices = choice_array, 
					default = slctn))
	except:

		return 'error in creating homework'
	finally:
		session.close()

	#print(gvbck_array)
	form = MultipleChoice(request.form)
	zipped = list(zip(form, crrct_array, answer_array, omit_array, gvbck_array))


	#clears form. need better method
	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		for hwquest in homework.homework_question:
			delattr(MultipleChoice, 'q'+str(hwquest.question.id))
	except:
		return 'error in clearing MultipleChoice form'
	finally:
		session.close()

	
	if request.method == 'POST':
		try:
			homework = session.query(Homework).filter_by(id=idnum).first()
			for hwquest in homework.homework_question:
				try:
					questionselectone = session.query(Question).filter_by(
						id=hwquest.question_id).first()
					choice_select = session.query(Choice).filter_by(
						id=request.form['q'+str(hwquest.question_id)]).first()
					studentselectone = StudentSelection(current_user, homework, 
						hwquest.question, choice_select) #request.form[str(question.id)] returns the selected choice_id of each question_id		
					session.merge(studentselectone)
					session.commit()
					
				except:
					pass
			message.append('Last saved {}'.format(
				datetime.datetime.now().astimezone(pytz.timezone("America/Chicago")).strftime('%b-%d %I:%M %p')
				))
			return render_template('homework.html', zipped = zipped, 
				disable = disable, answer = answer_array, info = info, 
				message = message
			)
		except:
			session.rollback()
			raise
			return Response('error in homework input')
		finally:
			session.close()
	else:
		#print(zipped[0][0].default)
		return render_template('homework.html', zipped = zipped, 
			disable = disable, info = info)

#testing if i can use plotly. own app run needed. delete
@app.route('/stuff')
def beta():
	print('stuff')
	df = pd.read_csv('http://www.stat.ubc.ca/~jenny/notOcto/STAT545A/examples/gapminder/data/gapminderDataFiveYear.txt', sep='\t')
	df2007 = df[df.year==2007]
	df1952 = df[df.year==1952]
	df.head(2)

	fig = {
		'data': [
			{
				'x': df[df['year']==year]['gdpPercap'],
				'y': df[df['year']==year]['lifeExp'],
				'name': year, 'mode': 'markers',
			} for year in [1952, 1982, 2007]
		],
		'layout': {
			'xaxis': {'title': 'GDP per Capita', 'type': 'log'},
			'yaxis': {'title': "Life Expectancy"}
		}
	}

	graphJSON = json.dumps(fig, cls=py.utils.PlotlyJSONEncoder)

	return render_template('beta.html',
		url = py.plotly.plot(fig, filename='pandas/grouped-scatter')
	) 


@app.route("/tables/<hwnum>")
@login_required(type= 0)
def show_tables(hwnum):
	try:
		hwid = hwnum
		hw = Homework.get(hwid)
		sshw = StudentSelection.get_hw_ss(hwid)
		qhw = pd.read_sql(sshw.statement,session.bind)
		if qhw.empty:
			return 'no data for homework'
		crs = Courses.get(hw.course_id)
		usrs = crs.get_users()

	except:
		raise
	finally:
		session.close()

	usrdata = pd.read_sql(usrs.statement, session.bind, index_col = 'id')
	usrdata.index.names = ['user_id']
	usrname = usrdata.loc[:, ['last_name', 'first_name']]

	qhwtbl = qhw.loc[:, ['user_id', 'question_id', 'selection']]
	qhwtbl['choiceorder'] = qhwtbl['selection'].apply(choice_order)
	qhwtblslctn = qhwtbl.pivot(index='user_id', columns='question_id', values='choiceorder')

	fnltbl = usrname.join(qhwtblslctn)
	fnltbl = fnltbl.set_index(['last_name', 'first_name'])


	qhwtbl['rghtwrng'] = qhwtbl['selection'].apply(choicerw)
	qhwtblrw = qhwtbl.pivot(index='user_id', columns='question_id', values='rghtwrng')

	fnltblrw = usrname.join(qhwtblrw)
	fnltblrw = fnltblrw.set_index(['last_name', 'first_name'])

	redmap = fnltblrw.applymap(
		lambda y: 'background-color: red' if not y else ''
	)

	#prcntcrrct = pd.DataFrame(fnltblrw.apply(np.sum,axis = 0).div(fnltblrw.apply(np.size,axis = 0)))
	prcntcrrct2 = fnltblrw.apply(np.sum,axis = 0)/fnltblrw.apply(np.size,axis = 0)

	#print()
	#print(test2)

	#redmap['prcntcrrct'] = barmap
	#print(barmap)
	#print(redmap)

	#qhwtblslctn = qhwtblslctn.style.apply(lambda x: redmap,axis=None)

	fnltbl.loc['prcntcrrct'] = prcntcrrct2

	test = pd.DataFrame(fnltbl.loc['prcntcrrct'])
	#print(test)

	barmap = test.applymap(
		lambda y: 
		'width:10em; height:80%; background:linear-gradient(90deg,#d65f5f {}%, transparent 0%);'.format(y*100)
		if y else ''
	)

	redmap.loc['prcntcrrct'] = barmap['prcntcrrct']

	#print(redmap)
	#print(fnltbl)

	end = fnltbl.style.apply(lambda x: redmap,axis=None)

	return render_template('view.html',
		tables = [end.set_table_attributes(
			"class='dataframe table table-hover table-bordered'").render()
		]
		
	)

	'''
	return render_template('view.html',	
		tables=[qhwtblslctn.style.render(
		classes='table table-striped table-sm').replace('<th>', '<th scope="col">')],
		titles = ['na', 'test table']
	)
	'''


@app.route("/alphatable/<hwnum>")
def alphatable(hwnum):
	html_row = []
	try:
		hwid = 2
		hw = Homework.get(hwid)
		hwqs = hw.get_questions()
		crs = Courses.get(hw.course_id)
		usrs = crs.get_users()

	except:
		raise
	finally:
		session.close()
	for usr in usrs:
		newrow = RowMake()
		fname = CellMake()
		fname.add_text(usr.first_name)
		newrow.add_cell(fname)
		lname = CellMake()
		lname.add_text(usr.last_name)
		newrow.add_cell(lname)

		hwss = StudentSelection.get_hw_ss(
			usr_id = usr.id,
			hwqs = hwqs,
			ifNone = '-'
			)
		for ss in hwss:
			sscell = CellMake()
			sscell.add_text(ss)
			newrow.add_cell(sscell)
		html_row.append(newrow)
	return render_template(
		'alpha.html',
		html_row = html_row
		)


@app.route("/coursegrades/<crsnum>")
@login_required(type= 0)
def coursegrades(crsnum):
	user_list = []
	try:
		crs_id = crsnum
		if int(crs_id) == 2:
			hwdrop = 1
			lbdrop = 1
		elif int(crs_id) == 3:
			hwdrop = 0
			lbdrop = 1
		else:
			hwdrop = 0
			lbdrop = 0

		(a,b,c,d) = (0.845,0.745,0.645,0.545)
		letgrd = letterGrade(a,b,c,d)

		crsu = session.query(CourseUser).filter_by(course_id = crs_id)
		for usr in crsu:
			usrid = usr.user.id
			crs = session.query(Courses).filter_by(id=crs_id).first()
			crshw = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==1,
				#now > Homework.due_date
				)
			)
			crstst = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==2))
			crslb = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==3))		
			crsfrmllb = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==4))
			crsfinal = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==5))

			hwgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crshw),
					AssgnmntGrade.user_id == usrid
				))
			)
			tstgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crstst),
					AssgnmntGrade.user_id == usrid
				))
			)
			lbgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crslb),
					AssgnmntGrade.user_id == usrid
				))
			)
			frmllbgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crsfrmllb),
					AssgnmntGrade.user_id == usrid
				))
			)
			finalgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crsfinal),
					AssgnmntGrade.user_id == usrid
				))
			)	

			hwprcnt = [grd.percent_score for grd in hwgrd]
			hwmxpnts = [grd.max_point for grd in hwgrd]
			tstprcnt = [grd.percent_score for grd in tstgrd]
			tstmxpnts = [grd.max_point for grd in tstgrd]
			lbprcnt = [grd.percent_score for grd in lbgrd]
			lbmxpnts = [grd.max_point for grd in lbgrd]		
			frmllbprcnt = [grd.percent_score for grd in frmllbgrd]
			frmllbmxpnts = [grd.max_point for grd in frmllbgrd]						
			finalprcnt = [grd.percent_score for grd in finalgrd]
			finalmxpnts = [grd.max_point for grd in finalgrd]		

			hw = Grades(hwprcnt,hwmxpnts)
			tst = Grades(tstprcnt,tstmxpnts)
			lb = Grades(lbprcnt,lbmxpnts)
			frmllb = Grades(frmllbprcnt,frmllbmxpnts)
			final = Grades(finalprcnt,finalmxpnts)

			hwzip = zip(
				hwprcnt,
				hw.points(),
				hwmxpnts,
				hw.droplist(hwdrop)
				)
			tstzip = zip(
				tstprcnt,
				tst.points(),
				tstmxpnts,
				)
			lbzip = zip(
				lbprcnt,
				lb.points(),
				lbmxpnts,
				lb.droplist(lbdrop)
				)
			frmllbzip = zip(
				frmllbprcnt,
				frmllb.points(),
				frmllbmxpnts,
				)

			#####
			fnlpntmx = 17.5
			fnlpnt = tst.avg() * fnlpntmx
			fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]

			finalzip = zip(
				[a*100 for a in finalprcnt],
				final.points(),
				finalmxpnts,
				)

			sumpt = hw.sum(dropnum=hwdrop)+tst.sum()+lb.sum(dropnum=lbdrop)+frmllb.sum()+fnlpnt
			summax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt()+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+fnlpntmx
			wfinalsumpt = hw.sum(dropnum=hwdrop)+tst.sum(dropnum=1)+lb.sum(dropnum=lbdrop)+frmllb.sum()+final.sum()
			wfinalsummax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt(dropnum=1)+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+final.maxpnt()			

			fnlprcnt = sumpt/summax
			wfinalfnlprcnt = wfinalsumpt/wfinalsummax
			#print(wfinalfnlprcnt)
			grd = letgrd.toLet(max(fnlprcnt,wfinalfnlprcnt)) 

			ttlzip = [
				sumpt,
				summax,
				fnlprcnt*100,
				wfinalsumpt,
				wfinalsummax,
				wfinalfnlprcnt*100,
				grd
			]

			usr_info = [
				usr.user.school_id,
				usr.user.first_name, 
				usr.user.last_name
			]			
			usr_all = [
				usr_info,
				hwzip,
				tstzip,
				lbzip,
				frmllbzip,
				ttlzip,
				fnlpntzip,
				finalzip
			]
			user_list.append(usr_all)
			header = []
			title_start = ['ID', 'First', 'Last']
			title_end = ['FnlPnt','total', 'MaxPt', 'percent','wfinaltotal', 'wfinalMaxPt', 'wfinalpercent', 'Letter']
			header += title_start

			for num, x in enumerate(filter(None,crstst), start=1):
				header.append("EXM{}".format(num))
			for num, x in enumerate(filter(None,crsfinal), start=1):
				header.append("FINAL{}".format(num))							
			for num, x in enumerate(filter(None,crshw), start=1):
				header.append("HW{}".format(num))
			for num, x in enumerate(filter(None,crslb), start=1):
				header.append("LAB{}".format(num))
			for num, x in enumerate(filter(None,crsfrmllb), start=1):
				header.append("FORMAL{}".format(num))
			header += title_end	
			#print(ttlzip[0],ttlzip[1],ttlzip[2])		
	except:
		#return 'error'
		raise
	finally:
		'''
		for info,hw,tst,lb,frmllb,ttl in user_list:
			print(info[0],info[1], info[2])
			for hwprcnt,hwpnts,hwmxpnts in hw:
				print(hwprcnt,hwpnts,hwmxpnts)
			for tstprcnt,tstpnts,tstmxpnts in tst:
				print(tstprcnt,tstpnts,tstmxpnts)
			for lbprcnt,lbpnts,lbmxpnts in lb:
				print(lbprcnt,lbpnts,lbmxpnts)
			for frmllbprcnt,frmllbpnts,frmllbmxpnts in frmllb:
				print(frmllbprcnt,frmllbpnts,frmllbmxpnts)
			print(ttl[0],ttl[1], ttl[2])
		'''
		session.close()


	return render_template(
		'coursegrades.html',
		user_list = user_list,
		header = header		
	)

@app.route("/gradehw/<hwnum>")
@login_required(type= 0)
def gradehw(hwnum):
	try:
		hw = session.query(Homework).filter_by(id = hwnum).first()
		crsu = session.query(CourseUser).filter_by(course_id = hw.course_id)
		for usr in crsu:
			usrhw = session.query(StudentSelection).filter(and_(
				StudentSelection.user_id == usr.user_id,
				StudentSelection.homework_id == hw.id,
			))

			hwqs = session.query(HomeworkQuestion).filter(and_(
				HomeworkQuestion.homework_id == hw.id,
				HomeworkQuestion.omit == 0,
			))

			point = 0
			total = 0

			for hwq in hwqs:
				total += 1					
				hws = session.query(StudentSelection).filter(and_(
					StudentSelection.user_id == usr.user_id,
					StudentSelection.homework_id == hw.id,
					StudentSelection.question_id == hwq.question_id
				)).first()
				try:
					chc = session.query(Choice).filter_by(id = hws.selection).first()
					if chc.true_or_false:
						point +=1
					#process giveback
					if hwq.give_back == 1:
						point +=1

				except:
					#raise
					pass
			try:
				newgrd = AssgnmntGrade(homework_id = hw.id, user_id = usr.user_id)
				newgrd.percent_score = point/total
			except:
				newgrd.percent_score = None

			session.merge(newgrd)
			session.commit()
	except:
		session.rollback()
		raise
	finally:
		session.close()
	return redirect(url_for('admincourse'))

@app.route('/api', methods = ['GET'])
@login_required(type= 0)
def this_func():
	"""This is a function. It does nothing."""
	return jsonify({ 'result': '' })

@app.route('/api/help', methods = ['GET'])
@login_required(type= 0)
def help():
	"""Print available functions."""
	route_list = []
	for rule in app.url_map.iter_rules():
		print(str(rule.endpoint))
		route_list.append(str(rule))
		#if rule.endpoint != 'static':
			#print(app.view_functions[rule.endpoint].__doc__)
			#func_list[rule.rule] = app.view_functions[rule.endpoint].__doc__
	return Response(response= route_list)


@app.route('/test/<number>', methods=['GET','POST'])
@login_required(type= 2)
@hw_course_check()
def number(number):
	return number


@app.route('/course/<idnum>', methods=['GET','POST'])
@login_required(type= 2)
@course_check()
def course(idnum):

	now = datetime.datetime.now()	
	grace = datetime.timedelta(hours = 8)
	nowgrace = now-grace
	#last modified
	if os.path.isfile(os.path.join('static','schedule', '{}.pdf'.format(idnum))):
		lstmdf = os.path.getmtime(os.path.join('static','schedule', '{}.pdf'.format(idnum)))
		lstmdf = datetime.datetime.fromtimestamp(lstmdf)
		lstmdf = lstmdf.strftime('%m/%d')
	else:
		lstmdf = 'NA'

	try:
		this_course = session.query(Courses).filter_by(id=idnum).first()
		hw = session.query(Homework).filter(and_(
			Homework.course_id == this_course.id,
			Homework.assgnmt_typ == 1
		))
		exms = session.query(Homework).filter(and_(
			Homework.course_id == this_course.id,
			Homework.assgnmt_typ == 2
		))
		return render_template('course.html',
			user=current_user,
			this_course=this_course,
			hw=hw, 
			now =nowgrace,
			exms = exms,
			lstmdf = lstmdf
		)
	except:
		raise
		return 'error on course page'
	finally:
		session.close()

@app.route("/hwextract/<hwnum>", methods=["GET", "POST"])
@login_required(type= 0)
def hwextract(hwnum):
	if request.method == "POST":
		#hwcsv = genfromtxt(request.files["hwcsv"], delimiter=',', skip_header=1, dtype=None)
		#data = hwcsv.tolist()
		#hwcsv = csv.DictReader(request.files["hwcsv"], delimiter=',')
		stream = io.StringIO(request.files["hwcsv"].stream.read().decode("UTF8"))
		csv_input = csv.DictReader(stream)

		try:
			hwqs = session.query(HomeworkQuestion).filter_by(homework_id=hwnum)
			for row in csv_input:
				user = session.query(Users).filter_by(school_id=row['id']).first()
				for hwq in hwqs:
					try:
						slctn = row[str(hwq.question_id)]
					except:
						return ('question id ' + str(hwq.question_id) +' not found in csv file')

					choice = session.query(Choice).filter(and_(
						Choice.multiplec_id == hwq.question.multiplec.id,
						Choice.order == let_to_num(slctn)
					)).first()

					if choice:
						newslctn = StudentSelection(user, hwq.homework, hwq.question, choice)
						session.merge(newslctn)
						#print('choice found')
					else:
						pass
						#return (str(hwq.question.multiplec.id)+ 'choice not found' + 'student id' + str(user.school_id))
					
			session.commit()
			#print(" extract_hw success")
		except:
			session.rollback() #Rollback the changes on error
			return "extract_hw error"
			#raise
		finally:
			session.close() #Close the connection 

		return render_template("hwextract.html", hwnum =hwnum)  
	else:
		return render_template("hwextract.html", hwnum =hwnum)

@app.route("/lbgrade/<crsnum>", methods=["GET", "POST"])
@login_required(type= 0)
def lbgrade(crsnum):
	if request.method == "POST":
		stream = io.StringIO(request.files["grdcsv"].stream.read().decode("UTF8"))
		csv_input = csv.DictReader(stream)
		try:
			crs = session.query(Courses).filter_by(id=crsnum).first()
			crsusr = session.query(CourseUser).filter_by(course_id = crsnum)
			crslbs = session.query(Homework).filter(and_(
				Homework.course==crs,
				or_(Homework.assgnmt_typ==3,Homework.assgnmt_typ==4)
				))
			usrs = (session.query(Users).
				filter(Users.id.
					in_(usr.user_id for usr in crsusr)
				)
			)
			#print ([usr.user_id for usr in crsusr])
			for row in csv_input:
				usr = session.query(Users).filter_by(school_id=row['id']).first()

				if usr in usrs:
					for lb in crslbs:
						try:
							grd = row[str(lb.id)]
							newgrd = AssgnmntGrade(homework_id = lb.id, user_id = usr.id)
							newgrd.percent_score = grd
							session.merge(newgrd)
							session.commit()
							#print('grade added')
						except:

							session.rollback()
				else:
					print('user not found')
		except:
			session.rollback() #Rollback the changes on error
			raise
			return "extract_hw error"
		finally:
			session.close()
		return render_template("lbgrade.html", crsnum = crsnum)  
	else:
		print('else')
		return render_template("lbgrade.html", crsnum = crsnum)

@app.route('/grades/<idnum>', methods=['GET','POST'])
@login_required(type= 2)
@course_check()
def stdnt_grd(idnum):
	now = datetime.datetime.now()
	try:
		if int(idnum) == 2:
			hwdrop = 1
			lbdrop = 1
		elif int(idnum) == 3:
			hwdrop = 0
			lbdrop = 1
		else:
			hwdrop = 0
			lbdrop = 0
		(a,b,c,d) = (0.845,0.745,0.645,0.545)
		letgrd = letterGrade(a,b,c,d)

		#crsnum = crsnum
		usrid = current_user.id
		crs = session.query(Courses).filter_by(id=idnum).first()
		crshw = session.query(Homework).filter(and_(
			Homework.course==crs,                
			Homework.assgnmt_typ==1,
			now > Homework.due_date)
		)
		crstst = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==2))
		crslb = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==3))		
		crsfrmllb = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==4))
		crsfinal = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==5))		

		hwgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crshw),
				AssgnmntGrade.user_id == usrid
			))
		)
		tstgrd = (session.query(AssgnmntGrade).
			filter   (and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crstst),
				AssgnmntGrade.user_id == usrid
			))
		)
		lbgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crslb),
				AssgnmntGrade.user_id == usrid
			))
		)
		frmllbgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crsfrmllb),
				AssgnmntGrade.user_id == usrid
			))
		)
		finalgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crsfinal),
				AssgnmntGrade.user_id == usrid
			))
		)		

		hwprcnt = [grd.percent_score for grd in hwgrd]
		hwmxpnts = [grd.max_point for grd in hwgrd]
		tstprcnt = [grd.percent_score for grd in tstgrd]
		tstmxpnts = [grd.max_point for grd in tstgrd]
		lbprcnt = [grd.percent_score for grd in lbgrd]
		lbmxpnts = [grd.max_point for grd in lbgrd]		
		frmllbprcnt = [grd.percent_score for grd in frmllbgrd]
		frmllbmxpnts = [grd.max_point for grd in frmllbgrd]						
		finalprcnt = [grd.percent_score for grd in finalgrd]
		finalmxpnts = [grd.max_point for grd in finalgrd]		


		hw = Grades(hwprcnt,hwmxpnts)
		tst = Grades(tstprcnt,tstmxpnts)
		lb = Grades(lbprcnt,lbmxpnts)
		frmllb = Grades(frmllbprcnt,frmllbmxpnts)
		final = Grades(finalprcnt,finalmxpnts)



		#####
		fnlpntmx = 17.5
		fnlpnt = tst.avg() * fnlpntmx

		sumpt = hw.sum(dropnum=hwdrop)+tst.sum()+lb.sum(dropnum=lbdrop)+frmllb.sum()+fnlpnt
		summax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt()+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+fnlpntmx
		#####
		wfinalsumpt = hw.sum(dropnum=hwdrop)+tst.sum(dropnum=1)+lb.sum(dropnum=lbdrop)+frmllb.sum()+final.sum()
		wfinalsummax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt(dropnum=1)+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+final.maxpnt()
		#print(final.maxpnt())
		

		hwzip = zip(
			[a*100 for a in hwprcnt],
			hw.points(),
			hwmxpnts,
			hw.droplist(hwdrop)
			)
		tstzip = zip(
			[a*100 for a in tstprcnt],
			tst.points(),
			tstmxpnts,
			)
		lbzip = zip(
			[a*100 for a in lbprcnt],
			lb.points(),
			lbmxpnts,
			lb.droplist(lbdrop)
			)
		frmllbzip = zip(
			[a*100 for a in frmllbprcnt],
			frmllb.points(),
			frmllbmxpnts,
			)
		fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]
		finalzip = zip(
			[a*100 for a in finalprcnt],
			final.points(),
			finalmxpnts,
			)

		fnlprcnt = sumpt/summax
		grd = letgrd.toLet(fnlprcnt) 

		ttlzip = [
				sumpt,
				summax,
				fnlprcnt*100,
				grd
			]
		option1zip = [hwzip,tstzip,lbzip,frmllbzip,ttlzip,fnlpntzip,finalzip]


		hwzip = zip(
			[a*100 for a in hwprcnt],
			hw.points(),
			hwmxpnts,
			hw.droplist(hwdrop)
			)
		tstzip = zip(
			[a*100 for a in tstprcnt],
			tst.points(),
			tstmxpnts,
			tst.droplist(1)
			)

		lbzip = zip(
			[a*100 for a in lbprcnt],
			lb.points(),
			lbmxpnts,
			lb.droplist(lbdrop)
			)
		frmllbzip = zip(
			[a*100 for a in frmllbprcnt],
			frmllb.points(),
			frmllbmxpnts,
			)
		fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]
		finalzip = zip(
			[a*100 for a in finalprcnt],
			final.points(),
			finalmxpnts,
			)
		fnlprcnt = wfinalsumpt/wfinalsummax
		grd = letgrd.toLet(fnlprcnt) 
		ttlzip = [
				wfinalsumpt,
				wfinalsummax,
				fnlprcnt*100,
				grd
			]

		option2zip = [hwzip,tstzip,lbzip,frmllbzip,ttlzip,fnlpntzip,finalzip]


	except:
		raise
		return 'error in stdnt_grd'
	finally:
		session.close()

	return render_template('stdnt_grd_wfinal.html',
		option1zip=option1zip,
		option2zip=option2zip

	)



def load_data(file_name):
	data = genfromtxt(file_name, delimiter=',', skip_header=1, dtype=None) #, converters={0: lambda s: int(s)}
	return data.tolist()


#old?
def extract_hw(file_path, hw_id):
	file_path = file_path
	try:
		data = load_data(file_path)
		query = session.query(HomeworkQuestion).filter_by(homework_id=hw_id)
		for i in data:

			user = session.query(Users).filter_by(school_id=i[0]).first()
			#print(user.school_id)
			select = list(i[1:])
			zipped = list(zip(query,select))
			for hwq,slctn in zipped:
				choice = session.query(Choice).filter(and_(
					Choice.multiplec_id == hwq.question.multiplec.id,
					Choice.order == let_to_num(slctn)
				)).first()
				if choice:
					newslctn = StudentSelection(user, hwq.homework, hwq.question, choice)
					session.merge(newslctn)
				
		session.commit()
		print(file_path + " extract_hw success")
	except:
		print(file_path + " extract_hw error")
		session.rollback() #Rollback the changes on error
		raise
	finally:
		session.close() #Close the connection 

def let_to_num(let):
	if let == 'A':
		return 0
	elif let == 'B':
		return 1
	elif let == 'C':
		return 2
	elif let == 'D':
		return 3
	else:
		pass

def num_to_let(num):
	if num == 0:
		return 'A'
	elif num == 1:
		return 'B'
	elif num == 2:
		return 'C'
	elif num == 3:
		return 'D'
	else:
		return 'NA'

def choice_order(cid):
	try:
		choice = session.query(Choice).filter_by(id=cid).first()
		if choice:
			return num_to_let(choice.order)
		else:
			return 'NA'
	except:
		return 'error in choice_order'
	finally:
		session.close()

#not needed?
def calc_points(prcnt, mxpnt):
	return(prcnt*mxpnt)

def choicerw(cid):
	try:
		chc = session.query(Choice).filter_by(id=cid).first()
		if chc:
			return chc.true_or_false
		else:
			return False
	except:
		return False
		raise
		return 'error in choicerw'
	finally:
		session.close()

def clr_neg_red(cid):

	if cid != cid:
		color = 'red'
		return 'color: %s' % color
	try:
		chc = session.query(Choice).filter_by(id=cid).first()
		if chc:
			if not chc.true_or_false:
				color = 'red'
				return 'color: %s' % color
			else:
				color = 'black'
				return 'color: %s' % color
		else:
			return 'error in clr_neg_red. choice not found.'
	except:
		raise
		return 'error in clr_neg_red'
	finally:
		session.close()

def rmvnone(val):
	if val is None:
		return 1
	else:
		return val

def rowIndex(row):
	print(row)
	return row

#not need anymore
def highlight(value):
	hw = session.query(StudentSelection).filter_by(homework_id = 8)
	qhw = pd.read_sql(hw.statement,session.bind)

	qhwtbl = qhw.loc[:, ['user_id', 'question_id', 'selection']]
	qhwtbl['rghtwrng'] = qhwtbl['selection'].apply(choicerw)
	qhwtblrw = qhwtbl.pivot(index='user_id', columns='question_id', values='rghtwrng')
	print (qhwtblrw)
	return qhwtblrw.applymap(
		lambda x: 'background-color: red' if not x else ''
	)

@app.route('/pdf')
def create_pdf():
	form = LoginForm(request.form)
	return wkhtmltopdf.render_template_to_pdf('showquestions.html', download=True, save=False)

#import ast
from urllib.parse import unquote
@app.route('/exmscn/<hwnum>')
@login_required(type= 2)
def exmscn(hwnum):
	#static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
	stdnt_id = current_user.school_id
	#folder = os.path.join('exam_drop', str(hwnum))
	file = os.path.join('exam_drop', str(hwnum), '{}.png'.format(stdnt_id))
	#print(static_file_dir)
	#if os.path.isfile(os.path.join('static',file)):
		#print('file yes')
	if os.path.isfile(os.path.join(static_file_dir,'static',file)):
	#if os.path.isfile(unquote(url_for('static', filename= file))):

		return ("<img src=" + 
			unquote(
				url_for('static', filename= file)
				) + ' '
			"width='100%'' height='auto'" +
			">"
		)
		'''	

		return ("<img src=" +
			os.path.join(static_file_dir,'static',file) + ' '
			"width='100%'' height='auto'" +
			">"
		)
		'''
	else:
		return ('file not found')
			
@app.route('/schdl/<crsnum>')
@login_required(type= 2)
def schdl(crsnum):
	#static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
	if os.path.isfile(os.path.join(static_file_dir,'static','schedule', '{}.pdf'.format(crsnum))):
		statbuf = os.path.getmtime(os.path.join(static_file_dir,'static','schedule', '{}.pdf'.format(crsnum)))
		statbuf = datetime.datetime.fromtimestamp(statbuf)
		statbuf = statbuf.strftime('%m / %d')
		#print("Modification time: {}".format(statbuf))
		file = os.path.join(static_file_dir,'schedule', '{}.pdf'.format(crsnum))
		#print(file)
		'''
		return ("<embed src=" + 
			unquote(
				url_for('static', filename= file)
				) + ' '
			"width='100%'' height='100%'" +
			">"
		)
		'''
		return send_file(os.path.join(static_file_dir,'static','schedule', '{}.pdf'.format(crsnum)
				)
		)
	else:
		return ('file not found')

@app.route('/course/<crsnum>/homework/<hwnum>')
#@login_required(type= 2)
def hw_file(crsnum, hwnum):
	if os.path.isfile(os.path.join(static_file_dir,'static',
		'course', crsnum, 'homework', '{}.pdf'.format(hwnum))
	):
		file = 	os.path.join('course', crsnum, 
			'homework', '{}.pdf'.format(hwnum)
		)
		
		return send_file(os.path.join(static_file_dir,'static',file))
	else:
		return ('file not found')

@app.route('/attndnc/<date>')
@login_required(type= 2)
def attndncs(hwnum):

	now = datetime.datetime.now().replace(microsecond=0).isoformat()



if __name__ == '__main__':
	

	def detectCircle(image):
		image = cv2.resize(image, (0,0), fx=0.5, fy=0.5  ) 
		bilateral_filtered_image = cv2.bilateralFilter(image, 5, 175, 175)
		#cv2.imshow('Bilateral', bilateral_filtered_image)
		#cv2.waitKey(0)

		edge_detected_image = cv2.Canny(bilateral_filtered_image, 75, 200)
		#edgergb = Image.fromarray((edge_detected_image).astype(np.uint8))
		#edgergb.save("edged.png")
		#cv2.imshow('Edge', edge_detected_image)
		#cv2.waitKey(0)

		(im2, contours, hierarchy)= cv2.findContours(edge_detected_image.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

		zipped = zip(contours,hierarchy[0])
		cnts = []
		heir = []

		'''
		for c in contours:
			# approximate the contour
			approx = cv2.approxPolyDP(c,0.01*cv2.arcLength(c,True),True)
			area = cv2.contourArea(c)
			if ((len(approx) > 8) & (area > 30) ):
				cnts.append(c)
		'''
		mark = 0
		child = []
		for num,c in enumerate(contours):
			# approximate the contour
			approx = cv2.approxPolyDP(c,0.01*cv2.arcLength(c,True),True)
			area = cv2.contourArea(c)
			if ((len(approx) > 8) & (area > 30) & (num not in child) ):
				cnts.append(c)

				k = num
				y = 0
				while (hierarchy[0][k][2] != -1):
					#print(hierarchy[0][k][2])
					k = hierarchy[0][k][2]
					y = y+1

				
				#print('contours heir#:{}'.format(y))
				if(y>=7):
					k=num
					while (hierarchy[0][k][2] != -1):
						k = hierarchy[0][k][2]
						child.append(k)
					D = num

				elif(y>=5):
					#cv2.drawContours(image, contours[x], -1, (0, 255, 0), 2)
					#print('contours heir#:{}'.format(y))
					k=num
					while (hierarchy[0][k][2] != -1):
						k = hierarchy[0][k][2]
						child.append(k)
					if (mark == 0):
						A = num
					elif  (mark == 1):
						B = num		# i.e., A is already found, assign current contour to B
					elif  (mark == 2):
						C = num		# i.e., A and B are already found, assign current contour to C
					mark = mark + 1 
				#print(child)
		threecnts = []
		threecnts.append(contours[A])
		threecnts.append(contours[B])
		threecnts.append(contours[C])
		threecnts.append(contours[D])

		MA = cv2.moments(contours[A])
		cx = int(MA['m10']/MA['m00'])
		cy = int(MA['m01']/MA['m00'])
		MA_center = (cx,cy)
		#print('A center:{} {}'.format(cx, cy))
		MB = cv2.moments(contours[B])
		cx = int(MB['m10']/MB['m00'])
		cy = int(MB['m01']/MB['m00'])
		MB_center = (cx,cy)
		#print('B center:{} {}'.format(cx, cy))
		MC = cv2.moments(contours[C])
		cx = int(MC['m10']/MC['m00'])
		cy = int(MC['m01']/MC['m00'])
		MC_center = (cx,cy)	
		#print('C center:{} {}'.format(cx, cy))
		MD = cv2.moments(contours[D])
		cx = int(MD['m10']/MD['m00'])
		cy = int(MD['m01']/MD['m00'])
		MD_center = (cx,cy)
		#print('D center:{} {}'.format(cx, cy))

		#cv2.drawContours(image, (contours[A],contours[B],contours[C],contours[D]), -1, (0, 255, 0), 2)
		# cv2.drawContours(image, ([contours[A][4],contours[A][5],contours[A][6]]), -1, (0, 255, 0), 3)
		# cv2.drawContours(image, ([contours[A][7],contours[A][8],contours[A][8]]), -1, (0, 255, 0), 3)
		#cv2.imshow("image", image)
		#cv2.waitKey(0) 

		return (
			MD_center,
			MC_center,
			MA_center,
			MB_center
			) 


	def scan_exm():
		scnlst = [x for x in os.listdir("exam_scans/")]
		#print(scnlst)
		for scn in scnlst:
			#print(scn)
			image = cv2.imread('exam_scans/{}'.format(scn))
			
			qrs = decode(image)
			for qr in qrs:
				#hw_id = int(qr.data.decode('UTF-8'))
				hwnum = int(qr.data.decode('UTF-8'))
			#print('hwnum: {}'.format(hwnum))
			#markers = detect_markers(image)	
			#print('{} marker length'.format(len(markers)))
			
			'''
			for marker in markers:
				#print('{} marker index'.format(markers.index(marker)))
				#print('{} marker id'.format(marker.id))
				#if markers.index(marker) % 2 == 0:  #goes through every marker x2 but not marker 1
					#print(marker.id)
				if marker.id == 1:
					mone = marker
				elif marker.id == 2:
					mtwo = marker
				elif marker.id == 3:
					mthree = marker
				elif marker.id == 4:
					mfour = marker	
				else:
					break
			'''
			(MD_center,MC_center,MA_center,MB_center) = detectCircle(image) 
			try:
				warpedarray = np.array([
							MD_center,
							MC_center,
							MA_center,
							MB_center
						])
			except:
				print('{} scan error'.format(scn))
				raise
			finally:
				session.close()

			image = cv2.resize(image, (0,0), fx=0.5, fy=0.5  )
			warped = four_point_transform(image, warpedarray)


			############return warped

			#image = cv2.imread("testrgbfill.png")
			gray = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)

			graycrop = Image.fromarray((gray).astype(np.uint8))		
			graycrop.save("graycrop.png")	

			thresh = cv2.threshold(gray, 0, 255,
				cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]


			thresh2 = cv2.threshold(gray, 0, 255,
				cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			threshrgb = Image.fromarray((thresh2).astype(np.uint8))		
			threshrgb.save("exam_crop/{}".format(scn))


			mlength = math.sqrt(
				thresh.shape[0]**2 
				+ thresh.shape[1]**2
			)*1.15
			#print(mlength)
			left = 0
			top = 0
			#plus 1 to adjust for pixel rounding down
			xgap = (mlength*0.013)
			ygap = (mlength*0.013)
			circlesize = (mlength * 0.005)
			let = ['A', 'B', 'C','D']
			y_lvl = (mlength*0.05)
			x_lvl = (mlength*0.10)
			#let_x = left+x_lvl-(xgap*0.22)
			#let_y = top+y_lvl-(.8*ygap)
			#num_x = left+x_lvl-(xgap*1.2)-circlesize
			num_y = top+y_lvl+(circlesize/2*1.2)
			cir_x = left+x_lvl
			cir_y = top+y_lvl


			#id_num = [0,1,2,3,4,5,6,7,8,9]
			id_y_lvl = (mlength*0.30)
			id_x_lvl = (mlength*0.35)
			id_num_x = left+id_x_lvl-(xgap*1.2)-circlesize
			id_num_y = top+id_y_lvl+(circlesize/2*1.2)
			id_cir_x = left+id_x_lvl
			id_cir_y = top+id_y_lvl


			akey_x = left+x_lvl+(xgap+circlesize)*3
			akey_y = top+y_lvl+(circlesize/2*1.2)
			atxt_x = akey_x
			atxt_y = top+y_lvl-(.8*ygap)
			you_x = akey_x+xgap*2
			you_y = atxt_y
			name_x = (mlength*0.30)
			name_y = (mlength*0.50)
			score_x = (mlength*0.30)
			score_y = (mlength*0.52)

			font = cv2.FONT_HERSHEY_SIMPLEX  

			stdnt_id = ""
			for j in range(0,7):
				
				bubbled = None
				for i in range(0,10):
					mask = np.zeros(thresh.shape, dtype="uint8")
					cv2.circle(mask, (int(id_cir_x+xgap*j),int(id_cir_y+ygap*i)),
						int(circlesize), 255, -1)

					mask = cv2.bitwise_and(thresh, thresh, mask=mask)
					total = cv2.countNonZero(mask)

					if bubbled is None or total > bubbled[0]:
						bubbled = (total, i)
				#print(bubbled)
				stdnt_id += str(bubbled[1])
			print('student id {}'.format(stdnt_id))

			#change hwnum for testing
			try:
				#hwqs = session.query(HomeworkQuestion).filter_by(homework_id=66)	
				hwqs = session.query(HomeworkQuestion).filter_by(homework_id=hwnum)
				user = session.query(Users).filter_by(school_id=int(stdnt_id)).first()
				#print("{} {}".format(user.first_name,user.last_name))

				#adding text
				cv2.putText(gray,'Key',(int(atxt_x),
					int(atxt_y)), font, 0.5,(0,0,0),2,cv2.LINE_AA
				)
				cv2.putText(gray,'You',(int(you_x),
					int(you_y)), font, 0.5,(0,0,0),2,cv2.LINE_AA
				)
				cv2.putText(gray,"{} {}".format(user.first_name,user.last_name)
					,(int(name_x),int(name_y)),
					font, 1,(0,0,0),1,cv2.LINE_AA
				)			
			except:
				print('{} {} scan error'.format(scn,stdnt_id))
				#raise
			finally:
				session.close()
			score = hwqs.count()   #score
			try:				
				for i, hwq in enumerate(hwqs):
					bubbled = None
					for j in range(0,4):
						mask = np.zeros(thresh.shape, dtype="uint8")
						cv2.circle(mask, (int(cir_x+xgap*j),int(cir_y+ygap*i)), int(circlesize), 255, -1)

						mask = cv2.bitwise_and(thresh, thresh, mask=mask)
						total = cv2.countNonZero(mask)

						if bubbled is None or total > bubbled[0]:
							bubbled = (total, j)
					#print('{} bubbled'.format(bubbled))

					choice = session.query(Choice).filter(and_(
						Choice.multiplec_id == hwq.question.multiplec.id,
						Choice.order == bubbled[1]
					)).first()

					if choice:
						#print(choice.id)
						newslctn = StudentSelection(user, hwq.homework, hwq.question, choice)
						session.merge(newslctn)



					
					#answer key text
					answr = session.query(Choice).filter(and_(
						Choice.multiplec_id == hwq.question.multiplec.id,
						Choice.true_or_false == 1
					)).first()

					cv2.putText(gray,num_to_let(answr.order) if answr else 'NA',(int(akey_x),
						int(akey_y+ygap*i)), font, 0.5,(0,0,0),2,cv2.LINE_AA
					)

					if choice != answr:
						cv2.putText(gray,num_to_let(choice.order) if answr else 'NA',(int(akey_x+xgap*2),
							int(akey_y+ygap*i)), font, 0.5,(0,0,0),2,cv2.LINE_AA
						)

						score-=1 #subtracts from total score is wrong

				#score text
				cv2.putText(gray,"score: {}/{}".format(score,hwqs.count())
					,(int(score_x),int(score_y)),
					font, 1,(0,0,0),1,cv2.LINE_AA
				)	

				#saving crop + answer key
				thresh3 = cv2.threshold(gray, 0, 255,
					cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
				threshrgb3 = Image.fromarray((thresh3).astype(np.uint8))
				folder = os.path.join('static','exam_drop', str(hwnum))
				if not os.path.exists(os.path.join('static','exam_drop', str(hwnum))):
					os.makedirs(os.path.join('static','exam_drop', str(hwnum)))
					#print(os.path.join('static','exam_drop', str(hwnum)) + " has been created")
				file = os.path.join(folder, "{}.png".format(user.school_id))
				threshrgb3.save(file)

				session.commit()
				print('{} scan successful'.format(scn))
			except:
				session.rollback() #Rollback the changes on error
				#return 'scan error'
				print('{} {} scan error'.format(scn,stdnt_id))
				raise

			finally:
				session.close() #Close the connection 		
	#scan_exm()

	import sys
	#import comtypes.client
	def save_pdf():
		wdFormatPDF = 17
		in_file =  os.path.abspath('test.docx')
		print(in_file)
		out_file =os.path.abspath('test.pdf')
		try:
			word = comtypes.client.CreateObject('Word.Application')
			doc = word.Documents.Open(in_file)
			doc.SaveAs(out_file, FileFormat=wdFormatPDF)
		except:
			raise
		finally:
			doc.Close()
			word.Quit()
	#save_pdf()

	def chngtyp():
		hw = session.query(Homework).filter_by(id= 57).first()
		hw.assgnmt_typ = 5
		hw = session.query(Homework).filter_by(id= 58).first()
		hw.assgnmt_typ = 5
		session.commit()
		session.close()
	#chngtyp()
	def chngmaxscr():
		hws = session.query(Homework).filter_by(assgnmt_typ = 1)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 1
		hws = session.query(Homework).filter_by(assgnmt_typ = 2)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 17.5
		hws = session.query(Homework).filter_by(assgnmt_typ = 3)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 1.2
		hws = session.query(Homework).filter_by(assgnmt_typ = 4)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 4	
		hws = session.query(Homework).filter_by(assgnmt_typ = 5)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 35						
		session.commit()
		session.close()
	#chngmaxscr()

	def test():
		print(os.path.join(url_for('static')))
	#test()

	def writefile():
		file = open('testfile.txt','a+') 
		file.write('Hello World\n') 
		file.write('This is our new text file\n') 
		file.write('and this is another line.\n') 
		file.write('Why? Because we can.\n')
		file.close() 
	#writefile()

	def GradeTable():
		user_list = []
		try:
			crs_id = 2
			crsu = session.query(CourseUser).filter_by(course_id = crs_id)
			for usr in crsu:
				usrid = usr.user.id
				crs = session.query(Courses).filter_by(id=crs_id).first()
				crshw = session.query(Homework).filter(and_(
					Homework.course==crs,                
					Homework.assgnmt_typ==1,
					#now > Homework.due_date
					)
				)
				crstst = session.query(Homework).filter(and_(
					Homework.course==crs,
					Homework.assgnmt_typ==2))
				crslb = session.query(Homework).filter(and_(
					Homework.course==crs,
					Homework.assgnmt_typ==3))		
				crsfrmllb = session.query(Homework).filter(and_(
					Homework.course==crs,
					Homework.assgnmt_typ==4))

				hwgrd = (session.query(AssgnmntGrade).
					filter(and_(
						AssgnmntGrade.homework_id.in_(hw.id for hw in crshw),
						AssgnmntGrade.user_id == usrid
					))
				)

				tstgrd = (session.query(AssgnmntGrade).
					filter   (and_(
						AssgnmntGrade.homework_id.in_(hw.id for hw in crstst),
						AssgnmntGrade.user_id == usrid
					))
				)
				lbgrd = (session.query(AssgnmntGrade).
					filter(and_(
						AssgnmntGrade.homework_id.in_(hw.id for hw in crslb),
						AssgnmntGrade.user_id == usrid
					))
				)
				frmllbgrd = (session.query(AssgnmntGrade).
					filter(and_(
						AssgnmntGrade.homework_id.in_(hw.id for hw in crsfrmllb),
						AssgnmntGrade.user_id == usrid
					))
				)


				hwprcnt = [grd.percent_score for grd in hwgrd]
				hwmxpnts = [grd.max_point for grd in hwgrd]
				tstprcnt = [grd.percent_score for grd in tstgrd]
				tstmxpnts = [grd.max_point for grd in tstgrd]
				lbprcnt = [grd.percent_score for grd in lbgrd]
				lbmxpnts = [grd.max_point for grd in lbgrd]		
				frmllbprcnt = [grd.percent_score for grd in frmllbgrd]
				frmllbmxpnts = [grd.max_point for grd in frmllbgrd]						


				hw = Grades(hwprcnt,hwmxpnts)
				tst = Grades(tstprcnt,tstmxpnts)
				lb = Grades(lbprcnt,lbmxpnts)
				frmllb = Grades(frmllbprcnt,frmllbmxpnts)

				sumpt = hw.sum()+tst.sum()+lb.sum()+frmllb.sum()
				summax = hw.maxpnt()+tst.maxpnt()+lb.maxpnt()+frmllb.maxpnt()
				ttlzip = [
					sumpt,
					summax,
					(sumpt/summax*100)
				]

				hwzip = zip(
					hwprcnt,
					hw.points(),
					hwmxpnts
					)
				tstzip = zip(
						tstprcnt,
						tst.points(),
						tstmxpnts
						)
				lbzip = zip(
						lbprcnt,
						lb.points(),
						lbmxpnts
						)
				frmllbzip = zip(
						frmllbprcnt,
						frmllb.points(),
						frmllbmxpnts
						)
				usr_info = [
					usr.user.school_id,
					usr.user.first_name, 
					usr.user.last_name
				]			
				usr_all = [
					usr_info,
					hwzip,
					tstzip,
					lbzip,
					frmllbzip,
					ttlzip
				]
				user_list.append(usr_all)
		except:
			#return 'error'
			raise
		finally:
			#for info,hw,tst,lb,frmllb,ttl in user_list:
			session.close()
	#GradeTable()
	def make_all_active():
		try:
			usrs = session.query(CourseUser).all()
			for usr in usrs:
				usr.active =1
				session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
	#make_all_active()
	def test():
		try:
			crs = session.query(Courses).filter_by(id = 1).first()
			for u in crs.get_users():
				print(u)
		except:
			raise
		finally:
			session.close()
	#test()

	def get_alembic():
		try:
			almbc = session.query(Alembic).all()
			for ver in almbc:
				print(ver.version_num)
		except:
			raise
		finally:
			pass
	#get_alembic()






	def alphatable():
		try:
			hwid = 2
			hw = Homework.get(hwid)
			hwqs = hw.get_questions()
			crs = Courses.get(hw.course_id)
			usrs = crs.get_users()

		except:
			raise
		finally:
			session.close()
		for usr in usrs:
			newrow = RowMake()
			fname = CellMake()
			fname.add_text(usr.first_name)
			newrow.add_cell(fname)
			lname = CellMake()
			lname.add_text(usr.last_name)
			newrow.add_cell(lname)

			hwss = StudentSelection.get_hw_ss(usr_id = usr.id, hwqs = hwqs, ifNone = None)
			for ss in hwss:
				sscell = CellMake()
				sscell.add_text(ss)
				newrow.add_cell(sscell)
			print(newrow)
	#alphatable()


	app.run(debug = False)