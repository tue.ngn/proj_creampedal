
'''
from flask import Flask, request, flash, url_for, redirect, render_template,\
		 Response, jsonify, current_app, send_file
import flask
'''
from flask import request, flash, url_for, redirect, render_template,\
		 Response, jsonify, current_app, send_file, render_template_string
from init_flask import app


from dbconn import session, Base, engine #, session2, engine2
import pandas as pd
#import plotly as py
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean,\
	SmallInteger, Text, Table, PrimaryKeyConstraint, UniqueConstraint,\
	 ForeignKeyConstraint, MetaData, DateTime, and_, or_, Float
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from wtforms import Form, TextField, validators, ValidationError, \
	SubmitField, RadioField, FieldList, FormField, BooleanField, StringField,\
	TextAreaField, PasswordField, SelectField, DateTimeField

from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user

from numpy import genfromtxt
import numpy as np
from scipy import stats

import csv

#from flask_alchemydumps import AlchemyDumps, AlchemyDumpsCommand , create
import flask_alchemydumps as aldump   #made changes and turned it into a local file
from flask_alchemydumps import AlchemyDumps
from flask_script import Manager

from sqlalchemy.ext.serializer import dumps, loads

from sqlalchemy.inspection import inspect

import datetime
import time
import pytz

from functools import wraps
import functools

import copy

#from flask_wkhtmltopdf import Wkhtmltopdf

import os
import io

import json

from ar_markers import HammingMarker, detect_markers
from scipy.ndimage import zoom
from PIL import Image
import cv2
import math
import pyqrcode
from pyzbar.pyzbar import decode
from imutils.perspective import four_point_transform

static_file_dir = os.path.join(os.path.dirname(os.path.realpath('__file__')))

import statistics

from functions import Grades, letterGrade, CellMake, RowMake, cropMarker, detectCircle
import template

import re
import glob

import uuid

from flask_bcrypt import Bcrypt
bcrypt = Bcrypt(app)


def debug(func):
	"""Print the function signature and return value"""
	@functools.wraps(func)
	def wrapper_debug(*args, **kwargs):
		args_repr = [repr(a) for a in args]                      # 1
		kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
		signature = ", ".join(args_repr + kwargs_repr)           # 3
		print(f"Calling {func.__name__}({signature})")
		value = func(*args, **kwargs)
		print(f"{func.__name__!r} returned {value!r}")           # 4
		return value
	return wrapper_debug

#doesnt work
class MethodDebug(object):
	"""Debug a normal method.

	This decorator is used for debugging a normal method,
	with normal arguments, i.e, not printing out the data
	of the class it's contained in.

	Keyword arguments:
	debug -- Whether or not you want to debug the method.
	"""
	def __init__(self, debug):
		self.debug = debug

	def __call__(self, function):
		def wrapper(*args, **kwargs):
			if self.debug:
				print(args)
				print(kwargs)
			return function(*args, **kwargs)
		return wrapper
#doesnt work
class ClassMethodDebug(object):
	"""Debug a class method.

	This decorator is used for debugging a class method,
	with normal arguments, and self. When using this
	decorator, the method will print out it's arguments
	and the attributes of the class it's contained in.

	Keyword arguments:
	debug -- Whether or not you want to debug the method.
	"""
	def __init__(self, debug):
		self.debug = debug

	def __call__(self, function):
		def wrapper(function_self, *args, **kwargs):
			if self.debug:
				print(function_self.__dict__)
				print(args)
				print(kwargs)
			return function(function_self, *args, **kwargs)
		return wrapper

def con_check():
	#print('attempting to connect...')
	while True:
		try:
			engine.connect()
			#print('connected')
			break
		except:
			print('unable to connect')
		finally:
			#pass
			session.close()

#to insure connection.
def connection_check(func):
	def Wrapper(*args, **kwargs):
		con_check()
		return func(*args, **kwargs)
	return Wrapper

#wkhtmltopdf = Wkhtmltopdf(app)

class Alembic(Base):

	__tablename__   = 'alembic_version'
	version_num = Column(String, primary_key=True, unique=True)


class Users(UserMixin, Base):

	__tablename__   = 'users'
	id = Column(Integer, primary_key=True, unique=True)
	password    = Column(String(255))  #was 80
	school_id   = Column(Integer)
	first_name  = Column(String(20))
	last_name   = Column(String(20))
	user_type   = Column(Integer)
	#course         = relationship('Courses', secondary=course_user, back_populates="user",lazy="dynamic")
	course_user = relationship('CourseUser', back_populates="user", lazy="dynamic")
	selection   = relationship('StudentSelection', lazy="dynamic")
	'''
	def __init__(self, school_id, last_name, first_name, user_type, password=None):
	#def __init__(self, school_id):
		self.school_id = school_id
		self.first_name = first_name
		self.last_name = last_name
		self.user_type = user_type
		self.password = password
	'''

	@classmethod
	@connection_check
	def get(cls, id):
		try:
			return session.query(cls).filter_by(school_id=id).first()
		except:
			return 'error in users.get'
		finally:
			session.close()

	@classmethod
	@connection_check
	def get_from_id(cls, id):
		return session.query(cls).filter_by(id=id).first()

	@connection_check
	def get_courses(self, includeInactive = 0):
		try:
			crsus = session.query(CourseUser).filter(
				CourseUser.user_id == self.id,
				)
			crss = session.query(Courses).filter(and_(
				Courses.id.in_(crsu.course_id for crsu in crsus),
				or_(Courses.active == 1, includeInactive == 1)
				))
			crss_list = [crs for crs in crss]
			return crss_list
		except:
			raise
			return 'error in Users.get_courses'
		finally:
			session.close()

	def get_first_name(self):
		return self.first_name if self.first_name else None
	def get_school_id(self):
		return self.school_id if self.school_id else None
	def get_last_name(self):
		return self.last_name if self.last_name else None

#@connection_check
class Courses(Base):
	__tablename__   = 'courses'
	id              = Column(Integer, primary_key=True)
	name            = Column(String(20))
	description     = Column(Text)
	#variable name
	#user           = relationship('Users', secondary=course_user, back_populates="course", lazy="dynamic")
	course_user     = relationship('CourseUser', back_populates="course", lazy="dynamic")
	homework        = relationship('Homework')
	active          = Column(Boolean, default=False)

	def __init__(self, name, description):
		self.name           = name
		self.description    = description

	@classmethod
	@connection_check
	def get(cls, id):
		try:
			return session.query(cls).filter_by(id=id).first()
		except:
			return 'error in Courses.get'
		finally:
			session.close()

	#needs a "get student" function
	@connection_check
	def get_users(self, includeInactive = 0):
		try:
			crsu = CourseUser.get_from_course(self.id)
			usrs = session.query(Users).filter(and_(
				Users.id.in_(usr.user_id for usr in crsu),
				or_(CourseUser.active == 1, includeInactive == 1)
				))
			usrs_list = [usr for usr in usrs]
			return usrs_list
		except:
			raise
			return 'error in Courses.get_users'
		finally:
			session.close()

	@connection_check
	def get_courseuser(self):
		try:
			crsus = CourseUser.get_from_course(self.id)
			crsu_list = [crsu for crsu in crsus]
			return crsu_list
		except:
			raise
			return 'error in Courses.get_courseuser'
		finally:
			session.close()

	#needs update
	@classmethod
	@connection_check
	def get_homework(cls, course_id, assgnmnt_type):
		try:
			crshw = session.query(Homework).filter(and_(
				Homework.course_id==course_id,
				Homework.assgnmt_typ==assgnmnt_type,
				#now > Homework.due_date
				)
			)
			crshw_list = [hw for hw in crshw]
			return crshw_list
		except:
			raise
			return 'error in Course.get_homework'
		finally:
			session.close()

	@connection_check
	def get_homework_list(self, assgnmnt_type = 2):
		try:
			crshw = session.query(Homework).filter(and_(
				Homework.course_id==self.id,
				Homework.assgnmt_typ==assgnmnt_type,
				#now > Homework.due_date
				)
			)
			crshw_list = [hw for hw in crshw]
			return crshw_list
		except:
			raise
			return 'error in Course.get_homework'
		finally:
			session.close()


	@connection_check
	def add_student(self, school_id):
		try:
			new_crsu = CourseUser(self.id, school_id)
			session.add(new_crsu)
			session.commit()
		except:
			session.rollback()
			raise
			return "error in User.add_student"
		finally:
			session.close()





class Homework(Base):
	__tablename__   = 'homework'
	id              = Column(Integer, primary_key=True, nullable = False)
	course_id       = Column(Integer, ForeignKey('courses.id'))
	description     = Column(Text)
	due_date        = Column(DateTime)
	assgnmt_typ     = Column(Integer)
	#max_points     = Column(Float)
		#secondary is variable name
	#question       = relationship('Question', secondary = homework_question, lazy="dynamic")
	homework_question   = relationship('HomeworkQuestion', back_populates="homework", lazy="dynamic")
	selection       = relationship('StudentSelection')
	course          = relationship('Courses')

	def __init__(self, description, date, assgntyp):
		self.description    = description
		self.due_date       = date
		self.assgnmt_typ    = assgntyp
		#1 =

	@classmethod
	@connection_check
	def get(cls, id):
		try:
			return session.query(cls).filter_by(id=id).first()
		except:
			return 'error in Homework.get'
		finally:
			session.close()

	def get_id(self):
		return self.id
	def get_course_id(self):
		return self.course_id

	@connection_check
	def get_questions(self):
		try:
			hwqs = session.query(HomeworkQuestion).filter_by(
				homework_id = self.id
				)
			qs = session.query(Question).filter(
				Question.id.in_(hwq.question_id for hwq in hwqs)
				)
			qs_list = [q for q in qs]
			return qs_list
		except:
			raise
			return 'error in Homework.get_questions'
		finally:
			session.close()

	def get_due_date(self):
		if self.due_date:
			return self.due_date.isoformat()
		else:
			return None
	def get_due_date_(self):
		if self.due_date:
			return self.due_date
		else:
			return None

	def is_due(self):    #need a get time function and grace as param

		due_date = self.get_due_date_()
		if due_date:
			due_date = due_date.astimezone(pytz.utc)
			grace = datetime.timedelta(hours = 8)
			now = datetime.datetime.now().astimezone(pytz.utc)

			due_grace = due_date + grace

			if now > due_grace:
				return True
			else:
				return False
		else:
			return False

	def change_due_date(self, value):
		try:
			self.due_date = value
			session.merge(self)
			session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
	def make_date_form(self, name = 'blank'):
		name_ = name
		value_ = self.get_due_date()
		hw_duedate = template.forms.datetime(name = name_, value = value_)
		return hw_duedate
	def get_description(self):
		descr = self.description
		return descr
	def change_description(self, value):
		try:
			self.description = value
			session.merge(self)
			session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
	def make_description_text(self, name = 'blank'):
		name_ = name
		value_ = self.get_description()
		q_descr = template.forms.text(name = name_, value = value_)
		return q_descr

class Question(Base):
	__tablename__   = 'question'
	id              = Column(Integer, primary_key=True)
	question_type   = Column(Integer)
	description     = Column(Text)
	multiplec       = relationship('MultipleC',
		cascade="all,delete",
		back_populates='question',
		uselist=False
	)
	#still not sure how uselist works. uselist mean it can be a list? dont add using append
	selection   = relationship('StudentSelection',
		lazy="dynamic",
		cascade="all,delete",
	) #lazy is not needed if i create selection all at one with init
	#dont need
	#homework       = relationship('homework_question', back_populates="question", lazy="dynamic")
	homework_question   = relationship('HomeworkQuestion',
		back_populates="question",
		cascade="all,delete"
	)

	@connection_check
	def __init__(self, type, text):
		self.question_type  = type
		self.description    = text

	@classmethod
	def get(cls, id):
		try:
			return session.query(cls).filter_by(id=id).first()
		except:
			raise
			return 'error in Question.get'


	def get_id(self):
		id_ = self.id
		return id_
	def get_choices(self):
		try:
			choices = MultipleC.get_choices(self.id)
			choices_list = [choice for choice in choices]
			return choices_list
		except:
			raise
		finally:
			session.close()
	def get_answer(self):
		choices = self.get_choices()
		answer_choice = None
		for choice in choices:
			if choice.true_or_false == True:
				answer_choice = choice
				break
		return answer_choice
	def get_answer_id(self):
		def naming():
			naming = 'a_'.format(self.id)
			return naming
		answer_choice = self.get_answer()
		answer_choice_id = answer_choice.id if answer_choice else None
		return answer_choice_id
	def get_description(self):
		descr = self.description
		return descr
	def get_answer(self):
		choices = self.get_choices()
		answer_choice = None
		for choice in choices:
			if choice.true_or_false == True:
				answer_choice = choice
				break
		return answer_choice

	def change_answer(old_choice, value):
		try:
			if not old_choice:
				pass
			else:
				old_choice = Choice.get(int(old_choice))
				old_choice.true_or_false = False
				#print(old_choice.id)
				#print(old_choice.true_or_false)
				session.merge(old_choice)
				session.commit()		#only works is commit after?
			new_choice = Choice.get(int(value))
			#print(new_choice.id)
			new_choice.true_or_false = True
			#print(new_choice.true_or_false)
			session.merge(new_choice)
			session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
	#@ClassMethodDebug
	def change_answer_(self, value):
		try:
			old_choice = self.get_answer()
			if not old_choice:
				pass
			else:
				old_choice.true_or_false = False
				session.merge(old_choice)
				session.commit()		#only works is commit after?
			if value:
				new_choice = Choice.get(int(value))
				new_choice.true_or_false = True
				session.merge(new_choice)
				session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
	def change_description(self, value):
		try:
			self.description = value
			session.merge(self)
			session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()

	def make_radio(self, name = 'blank', checked_id = None):
		name_ = name
		choices = self.get_choices()
		q_radio = template.forms.radio(name = name_, choices = choices, checked_id = checked_id)
		return q_radio
	def make_description_text(self, name = 'blank'):
		name_ = name
		value_ = self.get_description()
		q_descr = template.forms.text(name = name_, value = value_)
		return q_descr

class MultipleC(Base):
	__tablename__   = 'multiplec'
	id              = Column(Integer, primary_key=True)
	question_id     = Column(Integer, ForeignKey('question.id'))
	question        = relationship('Question', back_populates='multiplec')
	text            = Column(Text)
	choice          = relationship('Choice', cascade="all,delete",lazy="dynamic")

	def __init__(self, text):
		self.text   = text

	@classmethod
	def get_choices(cls, question_id):
		try:
			q = session.query(Question).filter_by(id = question_id).first()
			chs = session.query(Choice).filter_by(multiplec_id = q.multiplec.id)
			chs_list = [ch for ch in chs]
			return chs_list
		except:
			raise
		finally:
			session.close()


class Choice(Base):
	__tablename__   = "choice"    #matches the name of the actual database table
	id              = Column(Integer, primary_key=True)
	multiplec_id    = Column(Integer, ForeignKey('multiplec.id'))
	text            = Column(Text)
	order           = Column(SmallInteger)
	true_or_false   = Column(Boolean, default=False)

	#selection  = relationship('StudentSelection')
	@connection_check
	def __init__(self, text, order, bool):
		self.text = text
		self.order = order
		self.true_or_false = bool

	@classmethod
	def get(cls, choice_id):
		try:
			choice = session.query(cls).filter_by(id = choice_id).first()
			return choice
		except:
			raise
		finally:
			session.close()
	def get_id(self):
		return self.id
	def get_true_or_false(self):
		return self.true_or_false if self.true_or_false else False


'''(tables with foreignkeys as primary keys
 must be placed last for alchemydump to order and merge correctly)'''

#make assoce table an object to get session.merge working correctly
class CourseUser(Base):
	__tablename__   = 'course_user'
	course_id       = Column(Integer, ForeignKey('courses.id'), primary_key=True)
	user_id         = Column(Integer, ForeignKey('users.id'), primary_key=True)
	user            = relationship('Users')
	course          = relationship('Courses')
	active          = Column(Boolean, default=True)

	@connection_check
	def __init__(self, course, user):
		self.course_id = course.id
		self.user_id = user.id


	def get_id(self):
		id_ = str(self.course_id) + str(self.user_id).zfill(5)
		return int(id_)
	@classmethod
	@connection_check
	def get_courseuser(cls, crs_id, usr_id):
		try:
			crsu = session.query(cls).filter(
				CourseUser.course_id == crs_id,
				CourseUser.user_id == usr_id
				).first()
			return crsu
		except:
			raise
		finally:
			session.close()

	@classmethod
	@connection_check
	def get_from_course(cls, id):
		try:
			crsus = session.query(cls).filter(CourseUser.course_id == id)
			crsu_list = [crsu for crsu in crsus]
			return crsu_list
		except:
			raise
			return 'error in CourseUser.get_from_users'
		finally:
			session.close()

	@classmethod
	@connection_check
	def get_active_(cls, crs_id, usr_id):
		try:
			crsu = session.query(cls).filter(
				CourseUser.course_id == crs_id,
				CourseUser.user_id == usr_id
				).first()
			return crsu.active
		except:
			raise
			return 'error in CourseUser.get_active'
		finally:
			session.close()

	@connection_check
	def get_active(self):
		return self.active

	@classmethod
	@connection_check
	def toggle_active(cls, crs_id, usr_id, toggle):
		try:
			crsu = session.query(cls).filter(
				CourseUser.course_id == crs_id,
				CourseUser.user_id == usr_id
				).first()
			crsu.active = toggle
			session.commit()
		except:
			session.rollback()
			raise
			return 'error in CourseUser.toggle_active'
		finally:
			session.close()

	@connection_check
	def change_active(self, value):
		try:
			self.active = value
			session.merge(self)
			session.commit()
		except:
			session.rollback()
			#raise
			return 'error in CourseUser.change_active'
		finally:
			session.close()

	def make_active_checkbox(self, name ='blank'):
		name_ = name
		active_ = self.get_active()
		active_checkbox = template.forms.checkbox(name, checked = active_)
		return active_checkbox


class HomeworkQuestion(Base):
	__tablename__   = 'homework_question'
	homework_id     = Column(Integer, ForeignKey('homework.id'), primary_key=True)
	question_id     = Column(Integer, ForeignKey('question.id'), primary_key=True)
	homework        = relationship('Homework')
	question        = relationship('Question')
	give_back       = Column(Boolean)
	omit            = Column(Boolean)
	order			= Column(Integer)

	@connection_check
	def __init__(self, homework, question):
		self.homework_id = homework.id
		self.question_id = question.id

	@classmethod
	@connection_check
	def get(cls,homework_id, question_id):
		try:
			hwq = session.query(cls).filter(and_(
				cls.homework_id == homework_id,
				cls.question_id == question_id
				)).first()
			return hwq
		except:
			raise


	#better to use get, then get.order
	@classmethod
	@connection_check
	def get_order(cls,homework_id, question_id):
		try:
			hwq = session.query(cls).filter(and_(
				cls.homework_id == homework_id,
				cls.question_id == question_id
				)).first()
			return hwq.order
		except:
			raise
		finally:
			session.close()


#need to put max_point onto Homework
class AssgnmntGrade(Base):
	__tablename__   = "assgnmnt_grade"
	homework_id     = Column(Integer, ForeignKey('homework.id'), primary_key=True)
	user_id         = Column(Integer, ForeignKey('users.id'), primary_key=True)
	percent_score   = Column(Float)
	max_point       = Column(Float)

	homework        = relationship('Homework')
	users           = relationship('Users')

	def __init__(self, homework, question):
		self.homework_id = homework.id
		self.question_id = question.id

	@classmethod
	@connection_check
	def get_assgnmnt_grds(cls, user_id, homeworks):
		assgnmnt_grds = (session.query(cls).
				filter(and_(
					cls.homework_id.in_(hw.id for hw in homeworks),
					cls.user_id == user_id
				))
			)
		return assgnmnt_grds

	@classmethod
	@connection_check
	def get_percent(cls, user_id, assgnmnt_grds):
		return [grd.percent_score for grd in assgnmnt_grds]

	@classmethod
	@connection_check
	def get_max_point(cls, user_id, assgnmnt_grds):
		return [grd.percent_score for grd in assgnmnt_grds]

#delete right_or_wrong
class StudentSelection(Base):
	__tablename__   = "selection"    #matches the name of the actual database table
	#id             = Column(Integer, primary_key=True, autoincrement=True)
	user_id         = Column(Integer, ForeignKey('users.id'), primary_key=True)
	homework_id     = Column(Integer, ForeignKey('homework.id'), primary_key=True)#, primary_key=True)
	question_id     = Column(Integer, ForeignKey('question.id'), primary_key=True)#, primary_key=True)
		  #id               = Column(Integer, primary_key=True)
	selection       = Column(Integer)  #foreign key to choice?
	#slctn          = Column(Integer,ForeignKey('choice.id') )
	right_or_wrong  = Column(Boolean, default=False)

	users           = relationship('Users', uselist=False) #, back_populates='selection')
	homework        = relationship('Homework', uselist=False)  #uselist=False, backref='memberships', lazy='dynamic')
	question        = relationship('Question', uselist=False)

	def __init__(self, user, homework, question, select):
		self.user_id = user.id
		self.homework_id = homework.id
		self.question_id = question.id
		self.selection = select.id
		self.right_or_wrong = select.true_or_false

	@classmethod
	@connection_check
	def get_hw_ss(cls,usr_id,homework_questions, ifNone, let_choice = 1):
		ss_array = []
		try:
			for hwq in homework_questions:
				ss = session.query(cls).filter(and_(
				cls.user_id== usr_id,
				cls.question_id == hwq.id
				)).first()
				ss_array.append(
					(choice_order(ss.selection) if let_choice else ss.selection)
					if ss else ifNone
					)
			return ss_array
		except:
			raise
			return 'error in StudentSelection.get_hw_ss'
		finally:
			session.close()

	@classmethod
	@connection_check
	def get(cls,usr_id,hw_id, q_id):
		ss_array = []
		try:
			userselect = session.query(cls).filter(and_(
				StudentSelection.user_id == usr_id,
				StudentSelection.homework_id == hw_id,
				StudentSelection.question_id == q_id
			)).first()
			return userselect
		except:
			return None
			#raise
			#return 'error in StudentSelection.get_hw_ss'
		finally:
			session.close()

	def get_id(self):
		return '{}_{}_{}'.format(self.user_id, self.homework_id,self.question_id)


	@connection_check
	def get_selection(self):
		if self.selection:
			return self.selection
		else:
			return None

	def get_selection(self):
		return self.selection
	def change_selection(self, value): #if post form does not reveive same key, value = 0 and could erase data
		try:
			if value:
				self.selection = value
				session.merge(self)
				session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()

'''
class Attendance(Base):
	__tablename__   = "attendance"
	date_day        = Column(Date, primary_key=True)
	course_id       = Column(Integer, ForeignKey('courses.id'), primary_key=True)
	user_id         = Column(Integer, ForeignKey('users.id'), primary_key=True)
	#add relatoin to course and user.
'''

class LoginForm(Form):
	school_id = TextField("Student ID",[validators.Required("Please enter your Student ID.")])
	submit = SubmitField("Send")

class AdminLoginForm(Form):
	school_id = TextField("School ID",[validators.Required("Please enter your School ID.")])
	password = PasswordField("Password",[validators.Required("Please enter your Password.")])
	submit = SubmitField("Send")

class QuestionInput(Form):
	#test    = TextField("test text")
	text    = TextAreaField("What is the question?", [validators.Required("Please enter question.")])
	choice1 = TextField("Choice 1", [validators.Required("Please enter choice.")])
	choice2 = TextField("Choice 2", [validators.Required("Please enter choice.")])
	choice3 = TextField("Choice 3", [validators.Required("Please enter choice.")])
	choice4 = TextField("Choice 4", [validators.Required("Please enter choice.")])
	submit  = SubmitField("Send")

class MultipleChoice(Form):
	pass

class BlankForm(Form):
	pass

class OmitForm(Form):
	pass

class GiveBackForm(Form):
	pass

#removed?
class crtassgnmnt(Form):
	submit  = SubmitField("create")
	tsthw   = RadioField('testorhomework', choices = [('Homework', 1), ('Test', 2)])

#removed
class DateTime(Form):
	dttm = DateTimeField(
		"duedate", format="%Y-%m-%dT%H:%M:%S",
		#default=datetime.today, ## Now it will call it everytime.
		#validators=[validators.DataRequired()]
	)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

@login_manager.unauthorized_handler
def unauthorized_handler():
	return 'Unauthorized'

#role: -1 = anyone; 0 = Admin; 2 = student
def login_required(type=-1):
	def wrapper(fn):
		@wraps(fn)
		def decorated_view(*args, **kwargs):
			while True:
				try:
					engine.connect()
					break
				except:
					print('unable to connect')
				finally:
					session.close()
			if not current_user.is_authenticated:
				return redirect(url_for('login'))

			utype = current_user.user_type
			if ( (utype != type) and (type != -1)):
				print('error in login_required')
				return login_manager.unauthorized()
			return fn(*args, **kwargs)
		return decorated_view
	return wrapper

#checks to see if student is in course or the course page
def course_check():
	def wrapper(fn):
		@wraps(fn)
		def decorated_view(idnum):
			#print('course check')

			try:
				user = session.query(Users).filter_by(id=current_user.id).first()
				if(int(idnum) not in [r.course_id for r in user.course_user]):
					print('error in course_check')
					return login_manager.unauthorized()
			except:
				raise
				return 'course_check error'
			finally:
				session.close()

			return fn(idnum)
		return decorated_view
	return wrapper

#checks to see if student is in courseID, -1 = no one can enter
def hw_course_check():
	def wrapper(fn):
		@wraps(fn)
		def decorated_view(idnum):
			try:
				homework = session.query(Homework).filter_by(id=idnum).first()
				user = session.query(Users).filter_by(id=current_user.id).first()
				if not homework:
					return login_manager.unauthorized()
				if (homework.course_id not in [r.course_id for r in user.course_user]):
					return login_manager.unauthorized()
			except:
				return 'error in hw_course_check'
			finally:
				session.close()
			return fn(idnum)
		return decorated_view
	return wrapper




#not sure why i need this anymore
'''
@app.before_request
def before_request():
	flask.session.permanent = False     #I dont think this is working. Still logged in after closing browser
	app.permanent_session_lifetime = datetime.timedelta(minutes=20)
	flask.session.modified = True
	flask.g.user = current_user
'''

#used for user_id stored in session. Close browser = lose login
@connection_check
@login_manager.user_loader
def user_loader(id):
	try:
		user = session.query(Users).filter_by(id=id).first()
		return user
	except:
		return None
	finally:
		session.close()

@connection_check
@app.route('/login', methods=['GET', 'POST'])
def login():
	while True:
		#print('connecting...')
		try:
			engine.connect()
			break
		except:
			print('unable to connect')
		finally:
			session.close()

	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = LoginForm(request.form)
	if request.method == 'GET':
		return render_template('login.html', form = form)
	if form.validate():
		school_id = request.form['school_id']
		try:
			if session.query(Users).filter_by(school_id=school_id).first():
				user = session.query(Users).filter_by(school_id=school_id).first()
				if user.user_type != 2:
					return 'invalid user type'
				else:
					#print(user.first_name+user.last_name)
					login_user(user,
						remember = True,
						force = True
					) #, duration = datetime.timedelta(minutes=20)  doesnt work on pythonanywhere
					next = request.args.get('next')
				'''
				if not is_safe_url(next):
					return flask.abort(400)
				'''
				return redirect(next or url_for('index'))
			else:
				return 'Student ID not found.'
		except:
			raise
			return 'error in login'
		finally:
			session.close()

	return render_template('login.html', form = form)


@connection_check
@app.route("/adminlogin", methods=['GET', 'POST'])
def adminlogin():
	if current_user.is_authenticated:
		return redirect(url_for('admin'))
	form = AdminLoginForm(request.form)
	if request.method == 'GET':
		return render_template('adminlogin.html', form = form)
	if form.validate():
		school_id = request.form['school_id']
		password = request.form['password']
		try:
			if session.query(Users).filter_by(school_id=school_id).first():
				user = session.query(Users).filter_by(school_id=school_id).first()
				if user and bcrypt.check_password_hash(user.password, password):
					login_user(user, remember = False, force = True ) #, duration = datetime.timedelta(minutes=20)  doesnt work on pythonanywhere
					next = request.args.get('next')
					return redirect(next or url_for('admin'))
					'''
					if not is_safe_url(next):
						return flask.abort(400)
					'''
				else:
					return 'invalid password'
			else:
				return 'ID not found.'
		except:
			raise
			return 'error in login'
		finally:
			session.close()
	return render_template('adminlogin.html', form = form)


@connection_check
@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('login'))

@connection_check
@app.route('/')
@login_required(type= -1)
def index():

	try:
		crss = current_user.get_courses()
		if len(crss) == 0:
			return "No active classes found"
		elif len(crss) == 1:
			return redirect(url_for('course', idnum = crss[0].id))
		else:
			return redirect(url_for('courses'))
	except:
		raise
		return 'error in index'
	finally:
		session.close()

	#####old?
	try:
		user = session.query(Users).filter_by(id=current_user.id).first()
		user_courses = [r.course_id for r in user.course_user]
		if len(user_courses) == 1:
			return redirect(url_for('course', idnum = user_courses[0]))
		else:
			return redirect(url_for('courses'))
	except:
		return 'error in index'
	finally:
		session.close()

@connection_check
@app.route('/admin', methods=['GET', 'POST'])
@login_required(type= 0)
def admin():
	return render_template('admin.html', user = current_user)
	'''
	route_list = []
	for rule in app.url_map.iter_rules():
		if (rule.endpoint != 'bootstrap.static'
			and rule.endpoint != 'static'
			and rule.endpoint != 'number'):
			#print(str(rule.endpoint))
			route_list.append(str(rule.endpoint))
	return render_template('index.html', user = current_user, route = route_list)
	'''
@connection_check
@app.route('/admincourse')
@login_required(type= 0)
def admincourse():
	allcrs = session.query(Courses).all()
	return render_template('admincourse.html', allcrs = allcrs)

@connection_check
@app.route('/admin_index_')
@login_required(type= 0)
def admin_index_():
	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)
	df = pd.DataFrame()

	alchemy_dump_url = template.Template.make_url_for('alchdump', label = "Alchemy Dump")
	df2 = pd.DataFrame([[alchemy_dump_url]],columns=list(['Links']))
	courses_url = template.Template.make_url_for('admin_courses_', label = "Courses")
	df3 = pd.DataFrame([[courses_url]],columns=list(['Links']))
	user_list_url = template.Template.make_url_for('admin_user_list_', label = "Student List")
	df4 = pd.DataFrame([[user_list_url]],columns=list(['Links']))

	df = df.append(df2)
	df = df.append(df3)
	df = df.append(df4)
	base.add_body(df.to_html(escape = False, index = False))

	return render_template_string(str(base))


@connection_check
@app.route('/admin_user_list_')
@login_required(type= 0)
def admin_user_list_():
	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)
	df = pd.DataFrame()

	###
	all_users = session.query(Users).all()
	session.close()

	###
	df_1 = [u.id for u in all_users]
	df_2 = [u.school_id for u in all_users]
	df_3 = [u.get_first_name() for u in all_users]
	df_dict = {
		'id':df_1,
		'school id':df_2,
		'first':df_3,
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['id','school id','first']]

	###
	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)

	return render_template_string(str(base))

@connection_check
@app.route('/admin_courses_')
@login_required(type= 0)
def admin_courses_():
	####
	#user = session.query(Users).filter_by(id = 1).first() #current_user

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)
	df = pd.DataFrame()
	try:
		crss = session.query(Courses).all()
	except:
		raise
	finally:
		session.close()

	for crs in crss:
		course_url = template.Template.make_url_for('admin_course_', var = 'crs_num', num = crs.id, label = crs.name)
		df2 = pd.DataFrame([[crs.id, course_url, crs.description]], columns=list(['id','course_name','description']))
		df = df.append(df2)

	base.add_body(df.to_html(escape = False, index = False))

	return render_template_string(str(base))

@connection_check
@app.route('/admin_course_/<crs_num>', methods=['GET','POST'])
@login_required(type= 0)
def admin_course_(crs_num):
	crs_id = crs_num
	crs = Courses.get(crs_id)

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)
	df = pd.DataFrame()

	student_list_url = template.Template.make_url_for('student_list_', var = 'crs_num', num = crs.id, label = "Student List")
	df1 = pd.DataFrame([[student_list_url]],columns=list(['Links']))
	homework_list_url = template.Template.make_url_for('homework_list_', var = 'crs_num', num = crs.id, label = "Homework List")
	df2 = pd.DataFrame([[homework_list_url]],columns=list(['Links']))
	df = df.append(df1)
	df = df.append(df2)

	base.add_body(df.to_html(escape = False, index = False))

	return render_template_string(str(base))

@connection_check
@app.route('/student_list_/<crs_num>', methods=['GET','POST'])
@login_required(type= 0)
def student_list_(crs_num):
	crs_id = crs_num
	crs = Courses.get(crs_id)
	usrs = crs.get_courseuser()

	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	group_name = 'hello'
	dict_old = make_dict(name_ = group_name, list_instance_= usrs,  funct_ = 'get_active')
	dict_funct = make_dict(name_ = group_name, list_instance_= usrs, partial_ ='change_active')

	df_1 = [usr.make_active_checkbox(name = group_name+str(usr.get_id())) for usr in usrs]
	df_2 = [usr.user_id for usr in usrs]
	df_3 = [Users.get_from_id(usr.user_id).get_first_name() for usr in usrs]
	df_dict = {
		'user id':df_2,
		'user name':df_3,
		'active':df_1,
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['user id','user name','active']]

	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)

	###post
	if request.method == 'POST':
		form = request.form
		#print('form:{}'.format(form))
		dict_diff = request_process(form, dict_old)
		#print(dict_diff)
		map_dict(dict_diff, dict_funct)
		return redirect(request.url)

	return render_template_string(str(base))

@connection_check
@app.route('/homework_list_/<crs_num>', methods=['GET','POST'])
@login_required(type= 0)
def homework_list_(crs_num):
	crs_id = crs_num
	crs = Courses.get(crs_id)
	hws = crs.get_homework_list()

	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	group_name = 'hello'
	dict_old = make_dict(name_ = group_name, list_instance_= hws,  funct_ = 'get_due_date')
	dict_old.update(make_dict(name_ = 'world', list_instance_= hws,  funct_ = 'get_description'))
	dict_funct = make_dict(name_ = group_name, list_instance_= hws, partial_ ='change_due_date')
	dict_funct.update(make_dict(name_ = 'world', list_instance_= hws, partial_ = 'change_description'))
	#print('dict_old:{}'.format(dict_old))

	#df_1 = [hw.get_id() for hw in hws]
	df_1 = [template.Template.make_url_for('homework_questions_', var = 'hw_id', num = hw.id, label = hw.id) for hw in hws]
	df_2 = [hw.make_date_form(name = group_name+str(hw.get_id())) for hw in hws]
	df_3 = [hw.make_description_text(name = 'world'+str(hw.get_id())) for hw in hws]
	df_dict = {
		'hw id':df_1,
		'due date':df_2,
		'description':df_3
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['hw id','due date','description']]

	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)

	###post
	if request.method == 'POST':
		form = request.form
		#print('form:{}'.format(form))
		dict_diff = request_process(form, dict_old)
		#print(dict_diff)
		map_dict(dict_diff, dict_funct)
		return redirect(request.url)

	return render_template_string(str(base))

@app.route('/homework_questions_/<hw_id>', methods=['GET','POST'])
@login_required(type= 0)
def homework_questions_(hw_id):
	###init
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	hw_id = hw_id
	hw = Homework.get(hw_id)
	qs = hw.get_questions()


	###body
	#dict_old = dict_old_process(qs, 'get_answer_id', 'get_description')
	dict_old = make_dict(name_ = 'hello', list_instance_= qs,  funct_ = 'get_answer_id')
	dict_old.update(make_dict(name_ = 'world', list_instance_= qs,  funct_ = 'get_description'))
	dict_funct = make_dict(name_ = 'hello', list_instance_= qs, partial_ ='change_answer_')
	dict_funct.update(make_dict(name_ = 'world', list_instance_= qs, partial_ = 'change_description'))
	#dict_funct = dict_funct_process(qs, 'change_answer_', 'change_description')
	#df = data_frame_process(qs, 'get_id', 'make_radio', 'make_description_text')
	df_1 = [q.get_id() for q in qs]
	df_2 = [q.make_radio(name = 'hello'+ str(q.get_id())) for q in qs]   ## need automated way to add id
	df_3 = [q.make_description_text(name = 'world'+str(q.get_id())) for q in qs]
	df_dict = {
		'q id':df_1,
		'answer':df_2,
		'descript':df_3,
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['q id','answer','descript']]
	#print(df)
	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)
	#print(formtest)

	###post
	if request.method == 'POST':
		form = request.form
		#print('form:{}'.format(form))
		dict_diff = request_process(form, dict_old)
		#print(dict_diff)
		map_dict(dict_diff, dict_funct)
		return redirect(request.url)

	return render_template_string(str(base))



@connection_check
@app.route('/instrctr_crss')
@login_required(type= 0)
def instrctr_crs():
	crss = session.query(Courses).all()
	base = template.Base_Template()
	crss_html = '''

	'''
	base.add_body(crss_html)
	return render_template_string('admincourse.html', allcrs = allcrs)


@connection_check
@app.route('/hwrevise/<idnum>', methods=['GET', 'POST'])
@login_required(type= 0)
def hwrevise(idnum):
	zipped = []
	info = []
	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		answer_array = []

		for hwquest in homework.homework_question:
			choice_array = []
			true_choice = None
			choiceid = None

			for choice in hwquest.question.multiplec.choice:
				choice_array.append((str(choice.id),choice.text))
				if choice.true_or_false:
					true_choice = choice.order
					choiceid = choice.id
			if true_choice is None:
				#print(hwquest.question.multiplec.id)
				answer_array.append('NA')
			else:
				answer_array.append(num_to_let(true_choice))


			setattr(MultipleChoice, 'q'+str(hwquest.question.id),
				RadioField(hwquest.question.multiplec.text,
					choices = choice_array,
					default = choiceid))
			setattr(OmitForm, 'omt'+str(hwquest.question.id),
				BooleanField('omt'+str(hwquest.question.id),
					default = hwquest.omit)
			)
			setattr(GiveBackForm, 'gvbck'+str(hwquest.question.id),
				BooleanField('gvbck'+str(hwquest.question.id),
					default = hwquest.give_back)
			)

		omtform = OmitForm(request.form)
		gvbckform = GiveBackForm(request.form)
		ddate = homework.due_date
		#print(ddate.isoformat())
	except:
		raise
		return 'error in creating homework revision'
	finally:
		session.close()


	form = MultipleChoice(request.form)
	zipped = list(zip(form, answer_array, omtform, gvbckform))

	#clears form. need better method
	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		for hwquest in homework.homework_question:
			delattr(MultipleChoice, 'q'+str(hwquest.question.id))
			delattr(OmitForm, 'omt'+str(hwquest.question.id))
			delattr(GiveBackForm, 'gvbck'+str(hwquest.question.id))
	except:
		return 'error in clearing forms'
	finally:
		session.close()

	if request.method == 'POST':
		try:
			homework = session.query(Homework).filter_by(id=idnum).first()
			for hwquest in homework.homework_question:
				try:
					choiceid = None
					question = session.query(Question).filter_by(
						id=hwquest.question_id).first()
					choice_select = session.query(Choice).filter_by(
						id=request.form['q'+str(hwquest.question_id)]).first()
					for choice in hwquest.question.multiplec.choice:
						if choice.true_or_false:
							choiceid = choice.id
					if choiceid != choice_select.id:
						for choice in hwquest.question.multiplec.choice:
							choice.true_or_false = False
						choice_select.true_or_false = True
					info.append('answer key updated')
				except:
					pass

				try:
					request.form['omt'+str(hwquest.question_id)]
					hwquest.omit = True
				except:
					hwquest.omit = False
				try:
					request.form['gvbck'+str(hwquest.question_id)]
					hwquest.give_back = True
				except:
					hwquest.give_back = False

				dttm = request.form['datetime']
				homework.due_date = dttm

				session.commit()

			return render_template('hwrevise.html', zipped = zipped,
				answer = answer_array, info = info, ddate= (ddate.isoformat() if ddate else None))
		except:
			session.rollback()
			raise
			return Response('error in homework input')
		finally:
			session.close()

	else:
		#print(zipped[0][0].default)
		return render_template(
			'hwrevise.html',
			zipped = zipped,
			ddate= (ddate.isoformat() if ddate else None)
		)

@connection_check
@app.route('/alchdump', methods=['GET', 'POST'])
@login_required(type= 0)
def alchdump():
	ad = AlchemyDumps(Base, session, app.root_path)

	if request.method == 'GET':
		return render_template('alchdump.html', text = ad.text)
	if request.method == 'POST':
		filenum = request.form['filenum']
		if request.form['btn'] == 'History':
			ad.history()
			return render_template('alchdump.html', text = ad.text)
		if request.form['btn'] == 'Backup':
			ad.create()
			return render_template('alchdump.html', text = ad.text)
		if request.form['btn'] == 'Restore':
			ad.restore(filenum)
			return render_template('alchdump.html', text = ad.text)
	else:
		return 'error in alchdump'


####need to make
@app.route('/courses')
@login_required(type= 2)
def courses():
	#user_courses = [r.course_id for r in current_user.course_user]
	return 'This page is not avaliable at the moment' #render_template('courses.html', user = current_user)

@connection_check
@app.route('/questioninput', methods=['GET','POST'])
@login_required(type= 0)
def questioninput():
	form = QuestionInput(request.form)
	setattr(form.text, 'render_kw', {'onkeyup': 'Preview.Update()'})
	#setattr(form.text, 'default', "text text 2")
	#form.text.data = "test text"
	#form.choice1.data = "test text"

	if request.method == 'POST':
		if form.validate():
			print('input received')
			newquestion = Question(1, "needs description")
			newmc = MultipleC(request.form['text'])
			newchoice1 = Choice(request.form['choice1'], 0, True)
			newchoice2 = Choice(request.form['choice2'],1, False)
			newchoice3 = Choice(request.form['choice3'],2, False)
			newchoice4 = Choice(request.form['choice4'],3, False)

			newmc.choice.append(newchoice1)
			newmc.choice.append(newchoice2)
			newmc.choice.append(newchoice3)
			newmc.choice.append(newchoice4)
			newquestion.multiplec = newmc

			session.add(newquestion)
			session.commit()
			session.close()
			return render_template('questioninput.html', form = form, stuff="hello")

		else:
			print('input failed')
			return render_template('questioninput.html', form = form)

	else:
		print('first')
		return render_template('questioninput.html', form = form, stuff="hello")

@connection_check
@app.route('/create_assgnmnt', methods=['GET', 'POST'])
@login_required(type= 0)
def create_assgnmnt():
	form = crtassgnmnt(request.form)
	allcrs = session.query(Courses).all()
	text = []
	now = datetime.datetime.now().replace(microsecond=0).isoformat()

	if request.method == 'GET':
		return render_template('crtassgnmnt.html',
			allcrs = allcrs,
			text = text,
			form = form,
			now = now
		)
	elif request.method == 'POST':

		courseid = request.form['courseid']
		assgntyp = request.form['assgntyp']
		numq = request.form['numq']
		dttm = request.form['datetime']
		dscrptn = request.form['dscrptn']
		qdscrpt = request.form['qdscrpt']

		try:
			if (int(assgntyp) == 1) or (int(assgntyp) == 2):
				#print(assgntyp)
				hw = Homework(dscrptn, dttm, assgntyp)
				session.add(hw)

				crse = session.query(Courses).filter_by(id = courseid).first()
				crse.homework.append(hw)

				for x in range(int(numq)):
					q = Question(1, qdscrpt)
					mc = MultipleC('none')
					session.add(q)
					session.add(mc)
					session.commit()
					q.multiplec=mc
					for y in range(4):
						c = Choice('none',y,False)
						q.multiplec.choice.append(c)

					hq = HomeworkQuestion(hw,q)
					session.add(hq)
			elif (int(assgntyp) == 3):
				hw = Homework(dscrptn, dttm, assgntyp)
				session.add(hw)

				crse = session.query(Courses).filter_by(id = courseid).first()
				crse.homework.append(hw)
			else:
				return "error creating assignment"
			session.commit()

		except:
			session.rollback()
			raise
			return 'error on create_assgnmnt'
		finally:
			session.close()
		return 'assignment created'
	else:
		return 'error in create_assgnmnt'

@connection_check
@app.route('/showquestions')
@login_required(type= 0)
def showquestions():
	qs = session.query(Question).all()
	for q in qs:
		choice_array = []
		for choice in q.multiplec.choice:
			choice_array.append((str(choice.id),choice.text))
		setattr(
			MultipleChoice,
			'q'+str(q.id),
			RadioField(
				q.multiplec.text,
				choices = choice_array
			)
		)

	form = MultipleChoice(request.form)

	#clears form. need better method
	try:
		for q in qs:
			delattr(MultipleChoice, 'q'+str(q.id))
	except:
		return 'error in clearing MultipleChoice form'
	finally:
		session.close()

	return wkhtmltopdf.render_template_to_pdf('showquestions.html', download=True, save=False,form= form)
	#return render_template('showquestions.html', form= form)# questions = questions,

@app.route('/reviseq/<qnum>', methods=['GET','POST'])
@login_required(type= 0)
def reviseq(qnum):
	q = session.query(Question).filter_by(id=qnum).first()

	setattr(
		BlankForm,
		'qtext',
		TextAreaField(
			'Question',
			default = q.multiplec.text
		)
	)
	form = BlankForm(request.form)

	for choice in q.multiplec.choice:
		setattr(
			MultipleChoice,
			'c'+str(choice.id),
			TextAreaField(
				'Choice',
				default = choice.text
			)
		)
	mc = MultipleChoice(request.form)

	#clears form. need better method
	try:
		for choice in q.multiplec.choice:
			delattr(MultipleChoice, 'c'+str(choice.id))
	except:
		return 'error in clearing MultipleChoice form'

	if request.method == 'POST':
		#if request.form['btn'] == 'Preview':

		choice_array = []
		for choice in q.multiplec.choice:
			choice_array.append((str(choice.id),request.form['c'+str(choice.id)]))

		setattr(
			MultipleChoice,
			'q'+str(q.id),
			RadioField(
				request.form['qtext'],
				choices = choice_array
			)
		)
		prevmc = MultipleChoice(request.form)

		#clears form. need better method
		try:
			delattr(MultipleChoice, 'q'+str(q.id))
		except:
			return 'error in clearing MultipleChoice form'


		if request.form['btn'] == 'Save':
			q.multiplec.text = request.form['qtext']
			for choice in q.multiplec.choice:
				choice.text = request.form['c'+str(choice.id)]
			try:
				session.commit()
			except:
				session.rollback()
			finally:
				session.close()
			return render_template('reviseq.html', form= form, mc = mc, prevmc = prevmc)
		return render_template('reviseq.html', form= form, mc = mc, prevmc = prevmc)
	return render_template('reviseq.html', form= form, mc = mc)


@app.route('/homework/<idnum>', methods=['GET','POST'])
@login_required(type= 2)
@hw_course_check()
def homework(idnum):
	disable = False
	now = datetime.datetime.now().astimezone(pytz.utc)
	grace = datetime.timedelta(hours = 8)
	zipped = []
	message = []

	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		due = homework.due_date.astimezone(pytz.utc)
		duegrace = due+grace
		info = {'point':0, 'total':0}
		if now > duegrace:
			disable = True
		answer_array = []
		omit_array = []
		crrct_array = []
		gvbck_array = []

		for hwquest in homework.homework_question:
			choice_array = []

			true_choice = None

			if not hwquest.omit:
				info['total'] += 1

			userselect = session.query(StudentSelection).filter(and_(
				StudentSelection.user_id == current_user.id,
				StudentSelection.homework_id == homework.id,
				StudentSelection.question_id == hwquest.question_id
				)).first()

			for choice in hwquest.question.multiplec.choice:
				choice_array.append((str(choice.id),choice.text))
				if choice.true_or_false:
					true_choice = choice.order
			if true_choice is None:
				answer_array.append('NA')
			else:
				answer_array.append(num_to_let(true_choice))

			if userselect:
				slctn = userselect.selection
				chc = session.query(Choice).filter_by(id = userselect.selection).first()
				crrct_array.append(chc.true_or_false)
				if chc.true_or_false and not hwquest.omit:
					info['point'] += 1
				#process give back
				if hwquest.give_back == 1:
					info['point'] += 1
			else:
				crrct_array.append(False)
				slctn = None

			omit_array.append(hwquest.omit)
			gvbck_array.append(hwquest.give_back)

			setattr(MultipleChoice, 'q'+str(hwquest.question.id),
				RadioField(hwquest.question.multiplec.text,
					choices = choice_array,
					default = slctn))
	except:

		return 'error in creating homework'
	finally:
		session.close()

	#print(gvbck_array)
	form = MultipleChoice(request.form)
	zipped = list(zip(form, crrct_array, answer_array, omit_array, gvbck_array))


	#clears form. need better method
	try:
		homework = session.query(Homework).filter_by(id=idnum).first()
		for hwquest in homework.homework_question:
			delattr(MultipleChoice, 'q'+str(hwquest.question.id))
	except:
		return 'error in clearing MultipleChoice form'
	finally:
		session.close()


	if request.method == 'POST':
		try:
			homework = session.query(Homework).filter_by(id=idnum).first()
			for hwquest in homework.homework_question:
				try:
					questionselectone = session.query(Question).filter_by(
						id=hwquest.question_id).first()
					choice_select = session.query(Choice).filter_by(
						id=request.form['q'+str(hwquest.question_id)]).first()
					studentselectone = StudentSelection(current_user, homework,
						hwquest.question, choice_select) #request.form[str(question.id)] returns the selected choice_id of each question_id
					session.merge(studentselectone)
					session.commit()

				except:
					pass
			message.append('Last saved {}'.format(
				datetime.datetime.now().astimezone(pytz.timezone("America/Chicago")).strftime('%b-%d %I:%M %p')
				))
			return render_template('homework.html', zipped = zipped,
				disable = disable, answer = answer_array, info = info,
				message = message
			)
		except:
			session.rollback()
			raise
			return Response('error in homework input')
		finally:
			session.close()
	else:
		#print(zipped[0][0].default)
		return render_template('homework.html', zipped = zipped,
			disable = disable, info = info)

#testing if i can use plotly. own app run needed. delete
@app.route('/stuff')
def beta():
	print('stuff')
	df = pd.read_csv('http://www.stat.ubc.ca/~jenny/notOcto/STAT545A/examples/gapminder/data/gapminderDataFiveYear.txt', sep='\t')
	df2007 = df[df.year==2007]
	df1952 = df[df.year==1952]
	df.head(2)

	fig = {
		'data': [
			{
				'x': df[df['year']==year]['gdpPercap'],
				'y': df[df['year']==year]['lifeExp'],
				'name': year, 'mode': 'markers',
			} for year in [1952, 1982, 2007]
		],
		'layout': {
			'xaxis': {'title': 'GDP per Capita', 'type': 'log'},
			'yaxis': {'title': "Life Expectancy"}
		}
	}

	graphJSON = json.dumps(fig, cls=py.utils.PlotlyJSONEncoder)

	return render_template('beta.html',
		url = py.plotly.plot(fig, filename='pandas/grouped-scatter')
	)


@app.route("/tables/<hwnum>")
@login_required(type= 0)
def show_tables(hwnum):   #shows student homework selection and answers

	hw = Homework.get(hwnum)
	crs_id = hw.get_course_id() #get course from hw
	crs = Courses.get(crs_id)
	usrs = crs.get_users()
	qs = hw.get_questions()

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	#make index
	index_array = [(
		usr.get_school_id(),
		usr.get_first_name(),
		usr.get_last_name()
		)
		for usr in usrs
	]
	index = pd.MultiIndex.from_tuples(index_array, names=['ID', 'First','Last'])
	table_df = pd.DataFrame(index = index)

	for i,q in enumerate(qs):
		sss_list = []
		for usr in usrs:
			ss = StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id())
			sss_list.append(ss.get_selection() if ss else None)
		slct_df = pd.DataFrame(sss_list, columns=['{}({})'.format(i+1,q.get_id())], index = index)
		table_df = pd.concat([table_df,slct_df], axis=1)

	### df of letter choice
	#to_let = lambda x: np.NaN if pd.isnull(x) else choice_order(x)
	#let_df = table_df.applymap(to_let)

	###make true/false df
	to_right = lambda x: np.NaN if pd.isnull(x) else Choice.get(x).get_true_or_false()
	slct_right_df = table_df.applymap(to_right)

	### sum of row trues
	sum_df = slct_right_df.apply(np.sum, axis = 1)
	#sum_df = pd.DataFrame(sum_df, columns = ['TotalPt'])###########
	## percent of trues
	percentage = lambda x: x/len(qs)
	percentage_df = sum_df.apply(percentage)

	##only show wrong answers
	to_only_wrong = lambda x: '-' if pd.isnull(x) else choice_order(x) if not Choice.get(x).true_or_false else ''
	only_wrong_df = table_df.applymap(to_only_wrong)

	boxone_df = pd.concat([sum_df,only_wrong_df], axis = 1)

	###answer key
	answer_key = ['Key']
	answer_key.extend([choice_order(q.get_answer_id()) for q in qs])
	answer_df = pd.DataFrame(data=answer_key)
	###point biserial
	rows_that_have_data = slct_right_df[percentage_df.notnull()]
	pointbiserial = lambda x: stats.pointbiserialr(x, percentage_df.dropna())[0]
	pointbiserial_df = rows_that_have_data.fillna(False).apply(pointbiserial, axis = 0)
	pointbiserial_df =pd.DataFrame(data=pointbiserial_df)
	###percent correct
	percent_correct = lambda x: x.fillna(False).sum()/len(x)
	percent_correct_df = rows_that_have_data.apply(percent_correct, axis = 0)
	percent_correct_df =pd.DataFrame(data=percent_correct_df)

	base.add_body(boxone_df.to_html(escape = False, index = True))
	base.add_body(answer_df.T.to_html(escape = False, index = False))
	base.add_body(pointbiserial_df.T.to_html(escape = False, index = False))
	base.add_body(percent_correct_df.T.to_html(escape = False, index = False))
	return render_template_string(str(base))


	################ old
	hw = Homework.get(hwnum)
	crs_id = hw.get_course_id() #get course from hw
	crs = Courses.get(crs_id)
	usrs = crs.get_users()
	qs = hw.get_questions()

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	df_1 = [
		usr.get_first_name()
		for usr in usrs
	]
	df_dict = {
		'Name': df_1
	}

	#need to put choice order function in Choice class
	#is there a way to simplify this?
	for q in qs:
		answer_id = q.get_answer_id()
		#' 'is right, - if no answer, their selection if wrong
		sss_list = []
		for usr in usrs:
			ss = StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id())
			if ss:
				ss_selection = ss.get_selection()
				if ss_selection == answer_id:
					sss_list.append('')
				else:
					sss_list.append(choice_order(ss_selection))
			else:
				sss_list.append('-')

		df_dict.update({str(q.get_id()) : sss_list})

	df = pd.DataFrame.from_dict(df_dict)
	header = ['Name']
	header.extend([str(q.get_id()) for q in qs])
	df = df[header]

	answer_key = ['Key']
	answer_key.extend([choice_order(q.get_answer_id()) for q in qs])
	df.loc[-1] = answer_key

	#formtest = template.forms()
	#formtest.add_content(df.to_html(escape = False, index = False))
	#base.add_body(formtest)
	base.add_body(df.to_html(escape = False, index = False))
	session.close()
	return render_template_string(str(base))

#####################old
	try:
		hwid = hwnum
		hw = Homework.get(hwid)
		hwqs = hw.get_questions()
		sshw = StudentSelection.get_hw_ss(
			usr_id = usr.id,
			hwqs = hwqs,
			ifNone = '-'
			)
		qhw = pd.read_sql(sshw.statement,session.bind)
		if qhw.empty:
			return 'no data for homework'
		crs = Courses.get(hw.course_id)
		usrs = crs.get_users()

	except:
		raise
	finally:
		session.close()

	usrdata = pd.read_sql(usrs.statement, session.bind, index_col = 'id')
	usrdata.index.names = ['user_id']
	usrname = usrdata.loc[:, ['last_name', 'first_name']]

	qhwtbl = qhw.loc[:, ['user_id', 'question_id', 'selection']]
	qhwtbl['choiceorder'] = qhwtbl['selection'].apply(choice_order)
	qhwtblslctn = qhwtbl.pivot(index='user_id', columns='question_id', values='choiceorder')

	fnltbl = usrname.join(qhwtblslctn)
	fnltbl = fnltbl.set_index(['last_name', 'first_name'])


	qhwtbl['rghtwrng'] = qhwtbl['selection'].apply(choicerw)
	qhwtblrw = qhwtbl.pivot(index='user_id', columns='question_id', values='rghtwrng')

	fnltblrw = usrname.join(qhwtblrw)
	fnltblrw = fnltblrw.set_index(['last_name', 'first_name'])

	redmap = fnltblrw.applymap(
		lambda y: 'background-color: red' if not y else ''
	)

	#prcntcrrct = pd.DataFrame(fnltblrw.apply(np.sum,axis = 0).div(fnltblrw.apply(np.size,axis = 0)))
	prcntcrrct2 = fnltblrw.apply(np.sum,axis = 0)/fnltblrw.apply(np.size,axis = 0)

	#print()
	#print(test2)

	#redmap['prcntcrrct'] = barmap
	#print(barmap)
	#print(redmap)

	#qhwtblslctn = qhwtblslctn.style.apply(lambda x: redmap,axis=None)

	fnltbl.loc['prcntcrrct'] = prcntcrrct2

	test = pd.DataFrame(fnltbl.loc['prcntcrrct'])
	#print(test)

	barmap = test.applymap(
		lambda y:
		'width:10em; height:80%; background:linear-gradient(90deg,#d65f5f {}%, transparent 0%);'.format(y*100)
		if y else ''
	)

	redmap.loc['prcntcrrct'] = barmap['prcntcrrct']

	#print(redmap)
	#print(fnltbl)

	end = fnltbl.style.apply(lambda x: redmap,axis=None)

	return render_template('view.html',
		tables = [end.set_table_attributes(
			"class='dataframe table table-hover table-bordered'").render()
		]

	)

	'''
	return render_template('view.html',
		tables=[qhwtblslctn.style.render(
		classes='table table-striped table-sm').replace('<th>', '<th scope="col">')],
		titles = ['na', 'test table']
	)
	'''


@app.route("/alphatable/<hwnum>")
def alphatable(hwnum):

	hwid = hwnum
	html_row = []

	try:

		hw = Homework.get(hwid)
		hwqs = hw.get_questions()
		crs = Courses.get(hw.course_id)
		usrs = crs.get_users()

	except:
		raise
	finally:
		session.close()
	for usr in usrs:
		newrow = RowMake()
		fname = CellMake()
		fname.add_text(usr.first_name)
		newrow.add_cell(fname)
		lname = CellMake()
		lname.add_text(usr.last_name)
		newrow.add_cell(lname)

		hwss = StudentSelection.get_hw_ss(
			usr_id = usr.id,
			hwqs = hwqs,
			ifNone = '-'
			)
		for ss in hwss:
			sscell = CellMake()
			sscell.add_text(ss)
			newrow.add_cell(sscell)
		html_row.append(newrow)
	return render_template(
		'alpha.html',
		html_row = html_row
		)

@app.route("/betatable/<crsnum>")
def betatable(crsnum):
	user_list = []
	try:
		crs_id = crsnum
		if int(crs_id) == 2:
			hwdrop = 1
			lbdrop = 1
		elif int(crs_id) == 3:
			hwdrop = 0
			lbdrop = 1
		else:
			hwdrop = 0
			lbdrop = 0

		(a,b,c,d) = (0.845,0.745,0.645,0.545)
		letgrd = letterGrade(a,b,c,d)

		crs = session.query(Courses).filter_by(id=crs_id).first()
		crsu = crs.get_users()
		crshw = Courses.get_homework(course_id=crs_id,assgnmnt_type=1)
		crstst = Courses.get_homework(course_id=crs_id,assgnmnt_type=2)
		crslb =  Courses.get_homework(course_id=crs_id,assgnmnt_type=3)
		crsfrmllb =  Courses.get_homework(course_id=crs_id,assgnmnt_type=4)
		crsfinal =  Courses.get_homework(course_id=crs_id,assgnmnt_type=5)
		for usr in crsu:
			usrid = usr.id

			hw_grds =  AssgnmntGrade.get_assgnmnt_grds(user_id=usrid, homeworks=crshw)
			hwprcnt = AssgnmntGrade.get_percent(usrid, hw_grds)
			hwmxpnts = AssgnmntGrade.get_max_point(usrid, hw_grds)
			tst_grds =  AssgnmntGrade.get_assgnmnt_grds(user_id=usrid, homeworks=crstst)
			tstprcnt = AssgnmntGrade.get_percent(usrid, tst_grds)
			tstmxpnts = AssgnmntGrade.get_max_point(usrid, tst_grds)
			lb_grds =  AssgnmntGrade.get_assgnmnt_grds(user_id=usrid, homeworks=crslb)
			lbprcnt = AssgnmntGrade.get_percent(usrid, lb_grds)
			lbmxpnts = AssgnmntGrade.get_max_point(usrid, lb_grds)
			frmllb_grds =  AssgnmntGrade.get_assgnmnt_grds(user_id=usrid, homeworks=crsfrmllb)
			frmllbprcnt = AssgnmntGrade.get_percent(usrid, frmllb_grds)
			frmllbmxpnts = AssgnmntGrade.get_max_point(usrid, frmllb_grds)
			final_grds =  AssgnmntGrade.get_assgnmnt_grds(user_id=usrid, homeworks=crsfinal)
			finalprcnt = AssgnmntGrade.get_percent(usrid, final_grds)
			finalmxpnts = AssgnmntGrade.get_max_point(usrid, final_grds)

			hw = Grades(hwprcnt,hwmxpnts)
			tst = Grades(tstprcnt,tstmxpnts)
			lb = Grades(lbprcnt,lbmxpnts)
			frmllb = Grades(frmllbprcnt,frmllbmxpnts)
			final = Grades(finalprcnt,finalmxpnts)

			hwzip = zip(
				hwprcnt,
				hw.points(),
				hwmxpnts,
				hw.droplist(hwdrop)
				)
			tstzip = zip(
				tstprcnt,
				tst.points(),
				tstmxpnts,
				)
			lbzip = zip(
				lbprcnt,
				lb.points(),
				lbmxpnts,
				lb.droplist(lbdrop)
				)
			frmllbzip = zip(
				frmllbprcnt,
				frmllb.points(),
				frmllbmxpnts,
				)

			#####
			fnlpntmx = 17.5
			fnlpnt = tst.avg() * fnlpntmx
			fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]

			finalzip = zip(
				[a*100 for a in finalprcnt],
				final.points(),
				finalmxpnts,
				)

			sumpt = hw.sum(dropnum=hwdrop)+tst.sum()+lb.sum(dropnum=lbdrop)+frmllb.sum()+fnlpnt
			summax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt()+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+fnlpntmx
			wfinalsumpt = hw.sum(dropnum=hwdrop)+tst.sum(dropnum=1)+lb.sum(dropnum=lbdrop)+frmllb.sum()+final.sum()
			wfinalsummax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt(dropnum=1)+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+final.maxpnt()

			fnlprcnt = sumpt/summax
			wfinalfnlprcnt = wfinalsumpt/wfinalsummax
			#print(wfinalfnlprcnt)
			grd = letgrd.toLet(max(fnlprcnt,wfinalfnlprcnt))

			ttlzip = [
				sumpt,
				summax,
				fnlprcnt*100,
				wfinalsumpt,
				wfinalsummax,
				wfinalfnlprcnt*100,
				grd
			]

			usr_info = [
				usr.school_id,
				usr.first_name,
				usr.last_name
			]
			usr_all = [
				usr_info,
				hwzip,
				tstzip,
				lbzip,
				frmllbzip,
				ttlzip,
				fnlpntzip,
				finalzip
			]
			user_list.append(usr_all)
			header = []
			title_start = ['ID', 'First', 'Last']
			title_end = ['FnlPnt','total', 'MaxPt', 'percent','wfinaltotal', 'wfinalMaxPt', 'wfinalpercent', 'Letter']
			header += title_start

			for num, x in enumerate(filter(None,crstst), start=1):
				header.append("EXM{}".format(num))
			for num, x in enumerate(filter(None,crsfinal), start=1):
				header.append("FINAL{}".format(num))
			for num, x in enumerate(filter(None,crshw), start=1):
				header.append("HW{}".format(num))
			for num, x in enumerate(filter(None,crslb), start=1):
				header.append("LAB{}".format(num))
			for num, x in enumerate(filter(None,crsfrmllb), start=1):
				header.append("FORMAL{}".format(num))
			header += title_end
			#print(ttlzip[0],ttlzip[1],ttlzip[2])
	except:
		#return 'error'
		raise
	finally:
		session.close()

	return render_template(
		'beta.html',
		user_list = user_list,
		header = header
	)

from werkzeug.utils import secure_filename


ALLOWED_EXTENSIONS = set([ 'svg','png', 'jpg', 'jpeg'])

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

base_html= '''
{% extends "base.html" %}
{% block content %}
	{{ html|safe }}
{% endblock %}
'''
@app.route("/gamma", methods=['GET', 'POST'])
def gamma():
	context = {
	'html' :
	'''
	<h1>Upload new File</h1>
	<form method=post enctype=multipart/form-data>
	<input type=file name=file>
	<input type=submit value=Upload>
	</form>
	'''
	}
	#print(UPLOAD_FOLDER)
	if request.method == 'POST':
		# check if the post request has the file part
		if 'file' not in request.files:
			#print('No file part')
			return redirect(request.url)
		file = request.files['file']
		print(type(file))
		# if user does not select file, browser also
		# submit an empty part without filename
		if file.filename == '':
			#print('No selected file')
			return redirect(request.url)
		if file and allowed_file(file.filename):

			#print('file uploaded')
			npimg = np.fromstring(file.read(), np.uint8)
			file_img = cv2.imdecode(npimg, cv2.IMREAD_UNCHANGED)
			cropMarker(file_img)
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			return redirect(url_for('gamma',
									filename=filename))

	return render_template_string(base_html, **context)

	#############
	return '''
	<!doctype html>
	<title>Upload new File</title>
	<h1>Upload new File</h1>
	<form method=post enctype=multipart/form-data>
		<input type=file name=file>
		<input type=submit value=Upload>
	</form>
	'''


	image = []
	filename = 'test.svg'
	svg = open(filename, 'r')
	width = 800
	height = 600
	image.append({
		'width': int(width),
		'height': int(height),
		'src': filename
	})
	full = os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)
	print(full)
	return render_template(
		'gamma.html',
		image = full
	)

@app.route("/delta")
def delta():
	hwnum = 1

	#file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
	try:
		hw = Homework.get(hwnum)
		hwqs = hw.get_questions()
	except:
		raise
	finally:
		session.close()

	q_html_array = []
	for q in hwqs:
		#print('adding question:{}'.format(q.id))
		(q_path, c_path) = get_svg(hwnum,q.id)
		if not q_path and not c_path:
			continue
		q_html = imgHTML(q_path)
		#q_cell = CellMake(q_html)
		#q_row = RowMake(q_cell)
		c_html = [imgHTML(x) for x in c_path]
		#c_cell = [CellMake(x) for x in c_html]
		#c_row = [RowMake(x) for x in c_cell]
		chs = MultipleC.get_choices(q.id)
		radio_html = radioHTML(q.id, [ch.id for ch in chs])
		context = {
		'question' : q_html,
		'choices' : zip(c_html,radio_html)
		}
		q_html_array.append(context)

	#print(q_html_array)
	base_html= '''
	{% extends "base.html" %}
	{% block content %}

	{% for context in q_html_array %}
	<div class="container-fluid top-buffer">

		<div class="col-8 rounded" style="width: 50vw; background-color: rgba(0,0,255,.1)">
			{{ context['question']|safe }}
		</div>


		{% for c,r in context['choices']  %}
		<div class="d-flex justify-content-start top-buffer left-buffer" style="width: 30vw">
			<div class="col-1 rounded align-items-center justify-content-center" style="background-color: rgba(0,255,0,.2)" >
				{{ r|safe }}
			</div>

			<div class="col-4 rounded left-buffer" style="background-color: rgba(0,255,0,.1)">
				{{ c|safe }}
			</div>

		</div>
		{% endfor %}
	</div>
	{% endfor %}
	{% endblock %}
	'''
	return render_template_string(base_html, q_html_array=q_html_array)

@app.route("/epsilon")
def epsilon():
	crss = session.query(Courses).all()
	test = template.Base_Template()
	table = "<table>"

	head_id = 'ID'
	head_name = "Class"
	head_row = RowMake()
	head_id_cell = CellMake()
	head_id_cell.add_text(head_id)
	head_name_cell = CellMake()
	head_name_cell.add_text(head_name)
	head_row.add_cell(head_id_cell)
	head_row.add_cell(head_name_cell)

	table = table + str(head_row)
	print(head_row)
	print(table)

	for crs in crss:
		class_row = RowMake()
		link = template.Template.make_url_for('course', var = 'idnum', num = crs.id, label = crs.name)
		id_cell = CellMake()
		id_cell.add_text(crs.id)
		name_cell = CellMake()
		name_cell.add_text(link)
		class_row.add_cell(id_cell)
		class_row.add_cell(name_cell)

		table = table + str(class_row)


	table.join('</table>')
	test.add_body(table)
	return render_template_string(str(test))


def color_negative_red(val):
	"""
	Takes a scalar and returns a string with
	the css property `'color: red'` for negative
	strings, black otherwise.
	"""
	color = 'red' if val < 0 else 'black'
	return 'color: %s' % color

@app.route("/zeta")
def zeta():
	np.random.seed(24)
	df = pd.DataFrame({'A': np.linspace(1, 10, 10)})
	df = pd.concat([df, pd.DataFrame(np.random.randn(10, 4), columns=list('BCDE'))],
				   axis=1)
	df.iloc[0, 2] = '''<a href="https://www.w3schools.com/html/">Visit our HTML tutorial</a>'''
	#style = df.style.applymap(color_negative_red).render()

	test = template.Base_Template()
	test.add_body(df.to_html(escape = False))


	return render_template_string(str(test))

@app.route("/theta")
def theta():
	test = template.MathJax_Template()
	formtest = template.forms()
	test_value = '''When $a \ne 0$, there are two solutions to \(ax^2 + bx + c = 0\) and they are
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$'''
	things = formtest.textarea(div_id = 'text', other = '''onkeyup="Preview.Update()"''' , value = test_value)
	formtest.add_content(things)
	test.add_body(formtest, front = 1)
	return render_template_string(str(test))


def allow_csv(filename):
	ALLOWED_EXT = set(['csv'])
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in ALLOWED_EXT


#need something to detect correct ID#
@app.route("/iota", methods=['GET', 'POST'])
def iota():
	html = '''
	<h1>Upload new File</h1>
	<form method=post enctype=multipart/form-data>
	<input type=file name=file>
	<input type=submit value=Upload>
	</form>
	{{ message }}
	'''
	stuff = 'stuff'
	message = 'message'
	if request.method == 'POST':
		# check if the post request has the file part
		if 'file' not in request.files:
			print('No file part')
			return redirect(request.url)
		file = request.files['file']
		# if user does not select file, browser also
		# submit an empty part without filename
		if file.filename == '':
			print('No selected file')
			print(request.url)
			return redirect(request.url)
		if not allow_csv(file.filename):
			print('file not allowed')
		if file and allow_csv(file.filename):
			stream = io.StringIO(request.files["file"].stream.read().decode("UTF8"))
			csv_input = csv.DictReader(stream)
			print('file uploaded')
			for row in csv_input:
				school_id = row['school_id']
				name_last = row['name_last']
				name_first = row['name_first']

				check_id = Users.get(school_id)
				if check_id:
					stuff = stuff + 'Error: {} already in database'.format(school_id)
				else:

					try:

						new_user = Users()
						new_user.password = school_id
						new_user.school_id = school_id
						new_user.first_name = name_first
						new_user.Last_name = name_last
						session.add(new_user)
						session.commit()
						stuff = stuff + 'Success: {} added database'.format(school_id)

					except:
						stuff = stuff + 'Error: {} failed to add to database'.format(school_id)
						#session.rollback()
						#raise
					finally:
						session.close()
				try:
					crs = Courses.get(5)
					new_usr_for_course = Users.get(school_id)
					new_crs_u = CourseUser(crs,new_usr_for_course)
					session.add(new_crs_u)
					session.commit()
					stuff = stuff + 'Success: {} added to course'.format(school_id)
				except:
					stuff = stuff + 'Error: {} fail to add to course'.format(school_id)
					#session.rollback()
					#raise
				finally:
					session.close()
			context = {'message' : stuff}
			return render_template_string(html, **context)
			##check if school id is already in database


			#return redirect(url_for('gamma',filename=filename))
	context = {'message' : stuff}
	return render_template_string(html, **context)

class group_method():
	def __init__(self, instance, name = 'default'):
		self.instance = instance
		if name:
			self.name = name + str(instance.get_id())
		else:
			self.name = None
	def get_instance(self):
		return self.instance
	def get_name(self):
		return self.name
@debug
def map_dict(dict_a,dict_b):
	for key in dict_a:
		dict_b[key](value = dict_a[key])

def request_process(request_form, dict_old):

	form = request_form
	#print("in request proc:{}".format(dict_old))
	dict_new = dict()
	for key in dict_old:
		key_ = str(key)
		try:
			if form[key_] == 'on':   #checkbox returns 'on'
				dict_new[key_] = True
			elif form[key_] == '':
				dict_new[key_] = None
			else:
				dict_new[key_] = form[key_]
		except:
			dict_new[key_] = False   #returns false if checkbox is not marked, or radio is not marked
	set_1 = set(dict_old.items())
	set_2 = set(dict_new.items())
	dict_diff = dict(set_2 - set_1)
	return dict_diff


@app.route("/kappa" , methods=['GET','POST'])
def kappa():
	crs_id = 2
	crs = Courses.get(crs_id)
	#usrs = crs.get_users(includeInactive = 1)
	usrs = crs.get_courseuser()

	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	usr_group = [group_method(u, name = 'hello') for u in usrs]
	usr_group_blank = [group_method(u, name = None ) for u in usrs]
	usr_funct = [getattr(u.get_instance(), 'get_active') for u in usr_group]

	usr_group_name = [usr.get_name() for usr in usr_group]
	usr_group_funct = [getattr(usr.get_instance(), 'get_active') for usr in usr_group]

	dict_old = {a:b for a,b in zip([usr.get_name() for usr in usr_group],
		[getattr(usr.get_instance(), 'get_active') for usr in usr_group])}
	#print(dict_test)

	#dict_old = dict_old_process_((usr_group, 'get_active'))
	dict_funct = dict_funct_process_((usr_group, 'change_active'))
	df = data_frame_process_((usr_group_blank, 'get_id'),(usr_group, 'make_active_checkbox'))

	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)

	return render_template_string(str(base))

	for usr in usrs:
		name_active = 'usr_active_{}'.format(usr.id)
		active_ = CourseUser.get_active(crs.id, usr.id)
		dict_active[name_active] = active_
		dict_funct[name_active] = functools.partial(CourseUser.change_active, crs_id= crs.id, usr_id= usr.id)
		active_checkbox = template.forms.checkbox(name_active, checked = active_)

		df2 = pd.DataFrame([[usr.id, usr.first_name, usr.last_name, active_checkbox]], columns=list(['id','first','last', 'active']))
		df = df.append(df2)

	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)
	if request.method == 'POST':
		form = request.form
		dict_new = dict()

		for key in dict_active:
			#check box returns nothing if unchecked. check = 'on'. this is a workaround
			try:
				if request.form[key] == 'on':
					dict_new[key] = True
				else:
					dict_new[key] = request.form[key]
			except:
				dict_new[key] = False

		set_1 = set(dict_active.items())
		set_2 = set(dict_new.items())
		dict_diff = dict(set_2 - set_1)
		map_dict(dict_diff, dict_funct)


		return redirect(request.url)
	return render_template_string(str(base))

@app.route("/lambda_" , methods=['GET','POST'])
def lambda_():		#question radio for admin
	###init
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	hw_id = 2
	hw = Homework.get(hw_id)
	qs = hw.get_questions()


	###body
	#dict_old = dict_old_process(qs, 'get_answer_id', 'get_description')
	dict_old = make_dict(
		name_ = 'hello',
		list_instance_= qs,
		funct_ = 'get_answer_id'
	)
	dict_old.update(make_dict(
		name_ = 'world',
		list_instance_= qs,
		funct_ = 'get_description'
	))
	dict_funct = make_dict(
		name_ = 'hello',
		list_instance_= qs,
		partial_ ='change_answer_'
	)
	dict_funct.update(make_dict(
		name_ = 'world',
		list_instance_= qs,
		partial_ = 'change_description'
	))
	#dict_funct = dict_funct_process(qs, 'change_answer_', 'change_description')
	#df = data_frame_process(qs, 'get_id', 'make_radio', 'make_description_text')
	df_1 = [q.get_id() for q in qs]
	df_2 = [
		q.make_radio(
			name = 'hello'+ str(q.get_id()),
			checked_id = q.get_answer_id()
		)
		for q in qs
	]   ## need automated way to add id
	df_3 = [q.make_description_text(name = 'world'+str(q.get_id())) for q in qs]
	df_dict = {
		'q id':df_1,
		'answer':df_2,
		'descript':df_3,
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['q id','answer','descript']]
	#print(df)
	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)
	#print(formtest)

	###post
	if request.method == 'POST':
		form = request.form
		#print('form:{}'.format(form))
		dict_diff = request_process(form, dict_old)
		#print(dict_diff)
		map_dict(dict_diff, dict_funct)
		return redirect(request.url)

	return render_template_string(str(base))


@app.route("/omikron" , methods=['GET','POST'])
def omikron():

	crs_id = 1
	crs = Courses.get(crs_id)
	#usrs = crs.get_users(includeInactive = 1)
	usrs = crs.get_courseuser()

	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	group_name = 'hello'
	dict_old = make_dict(name_ = group_name, list_instance_= usrs,  funct_ = 'get_active')
	dict_funct = make_dict(name_ = group_name, list_instance_= usrs, partial_ ='change_active')

	df_1 = [usr.make_active_checkbox(name = group_name+str(usr.get_id())) for usr in usrs]
	df_2 = [usr.user_id for usr in usrs]
	df_3 = [Users.get_from_id(usr.user_id).get_first_name() for usr in usrs]
	df_dict = {
		'user id':df_2,
		'user name':df_3,
		'checkbox':df_1,
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['user id','user name','checkbox']]

	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)

	###post
	if request.method == 'POST':
		form = request.form
		#print('form:{}'.format(form))
		dict_diff = request_process(form, dict_old)
		#print(dict_diff)
		map_dict(dict_diff, dict_funct)
		return redirect(request.url)

	return render_template_string(str(base))

@app.route("/rho")  ### student index if they have multiple courses
def rho():
	usr = Users.get_from_id(2)
	crss = usr.get_courses()
	if len(crss) == 0:
		return "No active classes found"
	elif len(crss) == 1:
		return redirect(url_for('sigma'))   #add crs_id
	else:
		return "Are you enrolled in multiple courses? Something went wrong"

@app.route("/sigma")  ### student index if they have multiple courses
def sigma():
	usr = Users.get_from_id(2)
	crs = Courses.get(1)
	hws = crs.get_homework_list()

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)
	df = pd.DataFrame()

	df_1 = [
		template.Template.add_strike(
			template.Template.make_url_for('homework_questions_', var = 'hw_id', num = hw.id, label = hw.get_description()),
			bool_ = hw.is_due()
		)
		for hw in hws
	]
	df2 = df_2 = [
		hw.get_due_date_().strftime('%b %d') if hw.get_due_date_() else ''
		for hw in hws
	]
	df_3 = list(range(1, len(hws)+1))
	df_dict = {
		'descript':df_1,
		'due date':df_2,
		'HW#':df_3
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['HW#','descript','due date']]

	base.add_body(df.to_html(escape = False, index = False))

	return render_template_string(str(base))


@app.route("/tau", methods=['GET','POST']) #question radio for student
def tau():
	usr = Users.get_from_id(2)
	#crs = Courses.get(1)
	hw = Homework.get(1)
	qs = hw.get_questions()
	sss = [
		StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id())
		for q in qs
	]

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)
	df = pd.DataFrame()

	###body
	dict_old = make_dict(name_ = 'hello', list_instance_= sss,  funct_ = 'get_selection')  #better to get answer object, then get id later
	dict_funct = make_dict(name_ = 'hello', list_instance_= sss, partial_ ='change_selection')
	#dict_funct = dict_funct_process(qs, 'change_answer_', 'change_description')
	#df = data_frame_process(qs, 'get_id', 'make_radio', 'make_description_text')

	#df_1 = [q.get_id() for q in qs]
	df_1 = [
		q.make_radio(
			name = 'hello'+ str(ss.get_id()),
			checked_id = ss.get_selection()
		)
		for q,ss in zip(qs,sss)
	]   ## need automated way to add id
	df_3 = list(range(1, len(qs)+1))
	df_dict = {
		'A B C D':df_1,
		'Q#':df_3
	}
	df = pd.DataFrame.from_dict(df_dict)
	df = df[['Q#','A B C D']]

	formtest = template.forms()
	formtest.add_content(df.to_html(escape = False, index = False))
	base.add_body(formtest)


	###post
	if request.method == 'POST':
		form = request.form
		print('form:{}'.format(form))
		dict_diff = request_process(form, dict_old)
		#print(dict_diff)
		map_dict(dict_diff, dict_funct)
		return redirect(request.url)

	return render_template_string(str(base))

@app.route("/upsilon") #get all student selection for a homework
def upsilon():
	hw = Homework.get(2)
	crs_id = hw.get_course_id() #get course from hw
	crs = Courses.get(crs_id)
	usrs = crs.get_users()
	qs = hw.get_questions()

	###
	base = template.Base_Template()
	pd.set_option('display.max_colwidth', -1)

	df_1 = [
		usr.get_first_name()
		for usr in usrs
	]
	df_dict = {
		'Name': df_1
	}

	#need to put choice order function in Choice class
	#is there a way to simplify this?
	for q,i in zip(qs,list(range(1, len(qs)+1))):
		answer_id = q.get_answer_id()
		#' 'is right, - if no answer, their selection if wrong
		sss_list = []
		for usr in usrs:
			ss = StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id())
			if ss:
				ss_selection = ss.get_selection()
				if ss_selection == answer_id:
					sss_list.append('')
				else:
					sss_list.append(choice_order(ss_selection))
			else:
				sss_list.append('-')

		df_dict.update({str(i) : sss_list})
		'''
		sss = [
			'-'
			if not StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id())
			else ''
			if StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id()).get_selection() is answer_id
			else choice_order(StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id()).get_selection())
			for usr in usrs
		]
		'''

	#print(df_dict)
	df = pd.DataFrame.from_dict(df_dict)
	header = ['Name']
	header.extend([str(i) for q,i in zip(qs,list(range(1, len(qs)+1)))])
	df = df[header]

	answer_key = ['Key']
	answer_key.extend([choice_order(q.get_answer_id()) for q in qs])
	df.loc[-1] = answer_key

	#formtest = template.forms()
	#formtest.add_content(df.to_html(escape = False, index = False))
	#base.add_body(formtest)
	base.add_body(df.to_html(escape = False, index = False))

	return render_template_string(str(base))

@app.route("/phi")
def phi():

	users = session.query(Users).all()
	header = [{ 'Header': 'First',  'accessor': 'first' }]
	user_list = [{'first':user.first_name, 'last':user.last_name} for user in users]
	session.close()
	package = {
		'header': header,
		'list': user_list
	}
	return jsonify(package)
@app.route("/chi", methods=['POST'])
def chi():
	if request.method == 'POST':
		data = request.get_json(silent=True)
		item = {'userid': data.get('user_id')}
		print(item)
		return ' {} received'.format(item)

@app.route("/psi", methods=['POST'])
def psi():
	try:
		crs = session.query(Courses).filter_by(id=1).first()
	except:
		package = {
			'status': 'failed',
			'message': 'Unable to commicate with database'
		}
	try:
		crs.active = 0 if crs.active else 1
		session.commit()
		package = {
			'status': 'success',
		}
	except:
		package = {
			'status': 'failed',
			'message': 'Error in database'
		}
	finally:
		return jsonify(package)



@app.route("/coursegrades/<crsnum>")
@login_required(type= 0)
def coursegrades(crsnum):
	user_list = []
	try:
		crs_id = crsnum
		if int(crs_id) == 2:
			hwdrop = 1
			lbdrop = 1
		elif int(crs_id) == 3:
			hwdrop = 0
			lbdrop = 1
		else:
			hwdrop = 0
			lbdrop = 0

		(a,b,c,d) = (0.845,0.745,0.645,0.545)
		letgrd = letterGrade(a,b,c,d)

		crsu = session.query(CourseUser).filter_by(course_id = crs_id)
		for usr in crsu:
			usrid = usr.user.id
			crs = session.query(Courses).filter_by(id=crs_id).first()
			crshw = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==1,
				#now > Homework.due_date
				)
			)
			crstst = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==2))
			crslb = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==3))
			crsfrmllb = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==4))
			crsfinal = session.query(Homework).filter(and_(
				Homework.course==crs,
				Homework.assgnmt_typ==5))

			hwgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crshw),
					AssgnmntGrade.user_id == usrid
				))
			)
			tstgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crstst),
					AssgnmntGrade.user_id == usrid
				))
			)
			lbgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crslb),
					AssgnmntGrade.user_id == usrid
				))
			)
			frmllbgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crsfrmllb),
					AssgnmntGrade.user_id == usrid
				))
			)
			finalgrd = (session.query(AssgnmntGrade).
				filter(and_(
					AssgnmntGrade.homework_id.in_(hw.id for hw in crsfinal),
					AssgnmntGrade.user_id == usrid
				))
			)

			hwprcnt = [grd.percent_score for grd in hwgrd]
			hwmxpnts = [grd.max_point for grd in hwgrd]
			tstprcnt = [grd.percent_score for grd in tstgrd]
			tstmxpnts = [grd.max_point for grd in tstgrd]
			lbprcnt = [grd.percent_score for grd in lbgrd]
			lbmxpnts = [grd.max_point for grd in lbgrd]
			frmllbprcnt = [grd.percent_score for grd in frmllbgrd]
			frmllbmxpnts = [grd.max_point for grd in frmllbgrd]
			finalprcnt = [grd.percent_score for grd in finalgrd]
			finalmxpnts = [grd.max_point for grd in finalgrd]

			hw = Grades(hwprcnt,hwmxpnts)
			tst = Grades(tstprcnt,tstmxpnts)
			lb = Grades(lbprcnt,lbmxpnts)
			frmllb = Grades(frmllbprcnt,frmllbmxpnts)
			final = Grades(finalprcnt,finalmxpnts)

			hwzip = zip(
				hwprcnt,
				hw.points(),
				hwmxpnts,
				hw.droplist(hwdrop)
				)
			tstzip = zip(
				tstprcnt,
				tst.points(),
				tstmxpnts,
				)
			lbzip = zip(
				lbprcnt,
				lb.points(),
				lbmxpnts,
				lb.droplist(lbdrop)
				)
			frmllbzip = zip(
				frmllbprcnt,
				frmllb.points(),
				frmllbmxpnts,
				)

			#####
			fnlpntmx = 17.5
			fnlpnt = tst.avg() * fnlpntmx
			fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]

			finalzip = zip(
				[a*100 for a in finalprcnt],
				final.points(),
				finalmxpnts,
				)

			sumpt = hw.sum(dropnum=hwdrop)+tst.sum()+lb.sum(dropnum=lbdrop)+frmllb.sum()+fnlpnt
			summax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt()+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+fnlpntmx
			wfinalsumpt = hw.sum(dropnum=hwdrop)+tst.sum(dropnum=1)+lb.sum(dropnum=lbdrop)+frmllb.sum()+final.sum()
			wfinalsummax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt(dropnum=1)+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+final.maxpnt()

			fnlprcnt = sumpt/summax
			wfinalfnlprcnt = wfinalsumpt/wfinalsummax
			#print(wfinalfnlprcnt)
			grd = letgrd.toLet(max(fnlprcnt,wfinalfnlprcnt))

			ttlzip = [
				sumpt,
				summax,
				fnlprcnt*100,
				wfinalsumpt,
				wfinalsummax,
				wfinalfnlprcnt*100,
				grd
			]

			usr_info = [
				usr.user.school_id,
				usr.user.first_name,
				usr.user.last_name
			]
			usr_all = [
				usr_info,
				hwzip,
				tstzip,
				lbzip,
				frmllbzip,
				ttlzip,
				fnlpntzip,
				finalzip
			]
			user_list.append(usr_all)
			header = []
			title_start = ['ID', 'First', 'Last']
			title_end = ['FnlPnt','total', 'MaxPt', 'percent','wfinaltotal', 'wfinalMaxPt', 'wfinalpercent', 'Letter']
			header += title_start

			for num, x in enumerate(filter(None,crstst), start=1):
				header.append("EXM{}".format(num))
			for num, x in enumerate(filter(None,crsfinal), start=1):
				header.append("FINAL{}".format(num))
			for num, x in enumerate(filter(None,crshw), start=1):
				header.append("HW{}".format(num))
			for num, x in enumerate(filter(None,crslb), start=1):
				header.append("LAB{}".format(num))
			for num, x in enumerate(filter(None,crsfrmllb), start=1):
				header.append("FORMAL{}".format(num))
			header += title_end
			#print(ttlzip[0],ttlzip[1],ttlzip[2])
	except:
		#return 'error'
		raise
	finally:
		'''
		for info,hw,tst,lb,frmllb,ttl in user_list:
			print(info[0],info[1], info[2])
			for hwprcnt,hwpnts,hwmxpnts in hw:
				print(hwprcnt,hwpnts,hwmxpnts)
			for tstprcnt,tstpnts,tstmxpnts in tst:
				print(tstprcnt,tstpnts,tstmxpnts)
			for lbprcnt,lbpnts,lbmxpnts in lb:
				print(lbprcnt,lbpnts,lbmxpnts)
			for frmllbprcnt,frmllbpnts,frmllbmxpnts in frmllb:
				print(frmllbprcnt,frmllbpnts,frmllbmxpnts)
			print(ttl[0],ttl[1], ttl[2])
		'''
		session.close()


	return render_template(
		'coursegrades.html',
		user_list = user_list,
		header = header
	)

@app.route("/gradehw/<hwnum>")
@login_required(type= 0)
def gradehw(hwnum):
	try:
		hw = session.query(Homework).filter_by(id = hwnum).first()
		crsu = session.query(CourseUser).filter_by(course_id = hw.course_id)
		for usr in crsu:
			usrhw = session.query(StudentSelection).filter(and_(
				StudentSelection.user_id == usr.user_id,
				StudentSelection.homework_id == hw.id,
			))

			hwqs = session.query(HomeworkQuestion).filter(and_(
				HomeworkQuestion.homework_id == hw.id,
				HomeworkQuestion.omit == 0,
			))

			point = 0
			total = 0

			for hwq in hwqs:
				total += 1
				hws = session.query(StudentSelection).filter(and_(
					StudentSelection.user_id == usr.user_id,
					StudentSelection.homework_id == hw.id,
					StudentSelection.question_id == hwq.question_id
				)).first()
				try:
					chc = session.query(Choice).filter_by(id = hws.selection).first()
					if chc.true_or_false:
						point +=1
					#process giveback
					if hwq.give_back == 1:
						point +=1

				except:
					#raise
					pass
			try:
				newgrd = AssgnmntGrade(homework_id = hw.id, user_id = usr.user_id)
				newgrd.percent_score = point/total
			except:
				newgrd.percent_score = None

			session.merge(newgrd)
			session.commit()
	except:
		session.rollback()
		raise
	finally:
		session.close()
	return redirect(url_for('admincourse'))

@app.route('/api', methods = ['GET'])
@login_required(type= 0)
def this_func():
	"""This is a function. It does nothing."""
	return jsonify({ 'result': '' })

@app.route('/api/help', methods = ['GET'])
@login_required(type= 0)
def help():
	"""Print available functions."""
	route_list = []
	for rule in app.url_map.iter_rules():
		print(str(rule.endpoint))
		route_list.append(str(rule))
		#if rule.endpoint != 'static':
			#print(app.view_functions[rule.endpoint].__doc__)
			#func_list[rule.rule] = app.view_functions[rule.endpoint].__doc__
	return Response(response= route_list)


@app.route('/test/<number>', methods=['GET','POST'])
@login_required(type= 2)
@hw_course_check()
def number(number):
	return number


@app.route('/course/<idnum>', methods=['GET','POST'])
@login_required(type= 2)
@course_check()
def course(idnum):

	#############old
	now = datetime.datetime.now()
	grace = datetime.timedelta(hours = 8)
	nowgrace = now-grace
	#last modified
	if os.path.isfile(os.path.join('static','schedule', '{}.pdf'.format(idnum))):
		lstmdf = os.path.getmtime(os.path.join('static','schedule', '{}.pdf'.format(idnum)))
		lstmdf = datetime.datetime.fromtimestamp(lstmdf)
		lstmdf = lstmdf.strftime('%m/%d')
	else:
		lstmdf = 'NA'

	try:
		this_course = session.query(Courses).filter_by(id=idnum).first()
		hw = session.query(Homework).filter(and_(
			Homework.course_id == this_course.id,
			Homework.assgnmt_typ == 1
		))
		exms = session.query(Homework).filter(and_(
			Homework.course_id == this_course.id,
			Homework.assgnmt_typ == 2
		))
		final = session.query(Homework).filter(and_(
			Homework.course_id == this_course.id,
			Homework.assgnmt_typ == 5
		))
		return render_template('course.html',
			user=current_user,
			this_course=this_course,
			hw=hw,
			now =nowgrace,
			exms = exms,
			lstmdf = lstmdf,
			final = final
		)
	except:
		raise
		return 'error on course page'
	finally:
		session.close()

@app.route("/hwextract/<hwnum>", methods=["GET", "POST"])
@login_required(type= 0)
def hwextract(hwnum):
	if request.method == "POST":
		#hwcsv = genfromtxt(request.files["hwcsv"], delimiter=',', skip_header=1, dtype=None)
		#data = hwcsv.tolist()
		#hwcsv = csv.DictReader(request.files["hwcsv"], delimiter=',')
		stream = io.StringIO(request.files["hwcsv"].stream.read().decode("UTF8"))
		csv_input = csv.DictReader(stream)

		try:
			hwqs = session.query(HomeworkQuestion).filter_by(homework_id=hwnum)
			for row in csv_input:
				user = session.query(Users).filter_by(school_id=row['id']).first()
				for hwq in hwqs:
					try:
						slctn = row[str(hwq.question_id)]
					except:
						return ('question id ' + str(hwq.question_id) +' not found in csv file')

					choice = session.query(Choice).filter(and_(
						Choice.multiplec_id == hwq.question.multiplec.id,
						Choice.order == let_to_num(slctn)
					)).first()

					if choice:
						newslctn = StudentSelection(user, hwq.homework, hwq.question, choice)
						session.merge(newslctn)
						#print('choice found')
					else:
						pass
						#return (str(hwq.question.multiplec.id)+ 'choice not found' + 'student id' + str(user.school_id))

			session.commit()
			#print(" extract_hw success")
		except:
			session.rollback() #Rollback the changes on error
			return "extract_hw error"
			#raise
		finally:
			session.close() #Close the connection

		return render_template("hwextract.html", hwnum =hwnum)
	else:
		return render_template("hwextract.html", hwnum =hwnum)

@app.route("/lbgrade/<crsnum>", methods=["GET", "POST"])
@login_required(type= 0)
def lbgrade(crsnum):
	if request.method == "POST":
		stream = io.StringIO(request.files["grdcsv"].stream.read().decode("UTF8"))
		csv_input = csv.DictReader(stream)
		try:
			crs = session.query(Courses).filter_by(id=crsnum).first()
			crsusr = session.query(CourseUser).filter_by(course_id = crsnum)
			crslbs = session.query(Homework).filter(and_(
				Homework.course==crs,
				or_(Homework.assgnmt_typ==3,Homework.assgnmt_typ==4)
				))
			usrs = (session.query(Users).
				filter(Users.id.
					in_(usr.user_id for usr in crsusr)
				)
			)
			#print ([usr.user_id for usr in crsusr])
			for row in csv_input:
				usr = session.query(Users).filter_by(school_id=row['id']).first()

				if usr in usrs:
					for lb in crslbs:
						try:
							grd = row[str(lb.id)]
							newgrd = AssgnmntGrade(homework_id = lb.id, user_id = usr.id)
							newgrd.percent_score = grd
							session.merge(newgrd)
							session.commit()
							#print('grade added')
						except:

							session.rollback()
				else:
					print('user not found')
		except:
			session.rollback() #Rollback the changes on error
			raise
			return "extract_hw error"
		finally:
			session.close()
		return render_template("lbgrade.html", crsnum = crsnum)
	else:
		print('else')
		return render_template("lbgrade.html", crsnum = crsnum)

@app.route('/grades/<idnum>', methods=['GET','POST'])
@login_required(type= 2)
@course_check()
def stdnt_grd(idnum):
	now = datetime.datetime.now()
	try:
		if int(idnum) == 2:
			hwdrop = 1
			lbdrop = 1
		elif int(idnum) == 3:
			hwdrop = 0
			lbdrop = 1
		else:
			hwdrop = 0
			lbdrop = 0
		(a,b,c,d) = (0.845,0.745,0.645,0.545)
		letgrd = letterGrade(a,b,c,d)

		#crsnum = crsnum
		usrid = current_user.id
		crs = session.query(Courses).filter_by(id=idnum).first()
		crshw = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==1,
			now > Homework.due_date)
		)
		crstst = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==2))
		crslb = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==3))
		crsfrmllb = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==4))
		crsfinal = session.query(Homework).filter(and_(
			Homework.course==crs,
			Homework.assgnmt_typ==5))

		hwgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crshw),
				AssgnmntGrade.user_id == usrid
			))
		)
		tstgrd = (session.query(AssgnmntGrade).
			filter   (and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crstst),
				AssgnmntGrade.user_id == usrid
			))
		)
		lbgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crslb),
				AssgnmntGrade.user_id == usrid
			))
		)
		frmllbgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crsfrmllb),
				AssgnmntGrade.user_id == usrid
			))
		)
		finalgrd = (session.query(AssgnmntGrade).
			filter(and_(
				AssgnmntGrade.homework_id.in_(hw.id for hw in crsfinal),
				AssgnmntGrade.user_id == usrid
			))
		)

		hwprcnt = [grd.percent_score for grd in hwgrd]
		hwmxpnts = [grd.max_point for grd in hwgrd]
		tstprcnt = [grd.percent_score for grd in tstgrd]
		tstmxpnts = [grd.max_point for grd in tstgrd]
		lbprcnt = [grd.percent_score for grd in lbgrd]
		lbmxpnts = [grd.max_point for grd in lbgrd]
		frmllbprcnt = [grd.percent_score for grd in frmllbgrd]
		frmllbmxpnts = [grd.max_point for grd in frmllbgrd]
		finalprcnt = [grd.percent_score for grd in finalgrd]
		finalmxpnts = [grd.max_point for grd in finalgrd]


		hw = Grades(hwprcnt,hwmxpnts)
		tst = Grades(tstprcnt,tstmxpnts)
		lb = Grades(lbprcnt,lbmxpnts)
		frmllb = Grades(frmllbprcnt,frmllbmxpnts)
		final = Grades(finalprcnt,finalmxpnts)



		#####
		fnlpntmx = 17.5
		fnlpnt = tst.avg() * fnlpntmx

		sumpt = hw.sum(dropnum=hwdrop)+tst.sum()+lb.sum(dropnum=lbdrop)+frmllb.sum()+fnlpnt
		summax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt()+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+fnlpntmx
		#####
		wfinalsumpt = hw.sum(dropnum=hwdrop)+tst.sum(dropnum=1)+lb.sum(dropnum=lbdrop)+frmllb.sum()+final.sum()
		wfinalsummax = hw.maxpnt(dropnum=hwdrop)+tst.maxpnt(dropnum=1)+lb.maxpnt(dropnum=lbdrop)+frmllb.maxpnt()+final.maxpnt()
		#print(final.maxpnt())


		hwzip = zip(
			[a*100 for a in hwprcnt],
			hw.points(),
			hwmxpnts,
			hw.droplist(hwdrop)
			)
		tstzip = zip(
			[a*100 for a in tstprcnt],
			tst.points(),
			tstmxpnts,
			)
		lbzip = zip(
			[a*100 for a in lbprcnt],
			lb.points(),
			lbmxpnts,
			lb.droplist(lbdrop)
			)
		frmllbzip = zip(
			[a*100 for a in frmllbprcnt],
			frmllb.points(),
			frmllbmxpnts,
			)
		fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]
		finalzip = zip(
			[a*100 for a in finalprcnt],
			final.points(),
			finalmxpnts,
			)

		fnlprcnt = sumpt/summax
		grd = letgrd.toLet(fnlprcnt)

		ttlzip = [
				sumpt,
				summax,
				fnlprcnt*100,
				grd
			]
		option1zip = [hwzip,tstzip,lbzip,frmllbzip,ttlzip,fnlpntzip,finalzip]


		hwzip = zip(
			[a*100 for a in hwprcnt],
			hw.points(),
			hwmxpnts,
			hw.droplist(hwdrop)
			)
		tstzip = zip(
			[a*100 for a in tstprcnt],
			tst.points(),
			tstmxpnts,
			tst.droplist(1)
			)

		lbzip = zip(
			[a*100 for a in lbprcnt],
			lb.points(),
			lbmxpnts,
			lb.droplist(lbdrop)
			)
		frmllbzip = zip(
			[a*100 for a in frmllbprcnt],
			frmllb.points(),
			frmllbmxpnts,
			)
		fnlpntzip = [tst.avg()*100, fnlpnt, fnlpntmx]
		finalzip = zip(
			[a*100 for a in finalprcnt],
			final.points(),
			finalmxpnts,
			)
		fnlprcnt = wfinalsumpt/wfinalsummax
		grd = letgrd.toLet(fnlprcnt)
		ttlzip = [
				wfinalsumpt,
				wfinalsummax,
				fnlprcnt*100,
				grd
			]

		option2zip = [hwzip,tstzip,lbzip,frmllbzip,ttlzip,fnlpntzip,finalzip]


	except:
		raise
		return 'error in stdnt_grd'
	finally:
		session.close()

	return render_template('stdnt_grd_wfinal.html',
		option1zip=option1zip,
		option2zip=option2zip

	)



def load_data(file_name):
	data = genfromtxt(file_name, delimiter=',', skip_header=1, dtype=None) #, converters={0: lambda s: int(s)}
	return data.tolist()


#old?
def extract_hw(file_path, hw_id):
	file_path = file_path
	try:
		data = load_data(file_path)
		query = session.query(HomeworkQuestion).filter_by(homework_id=hw_id)
		for i in data:

			user = session.query(Users).filter_by(school_id=i[0]).first()
			#print(user.school_id)
			select = list(i[1:])
			zipped = list(zip(query,select))
			for hwq,slctn in zipped:
				choice = session.query(Choice).filter(and_(
					Choice.multiplec_id == hwq.question.multiplec.id,
					Choice.order == let_to_num(slctn)
				)).first()
				if choice:
					newslctn = StudentSelection(user, hwq.homework, hwq.question, choice)
					session.merge(newslctn)

		session.commit()
		print(file_path + " extract_hw success")
	except:
		print(file_path + " extract_hw error")
		session.rollback() #Rollback the changes on error
		raise
	finally:
		session.close() #Close the connection

def let_to_num(let):
	if let == 'A':
		return 0
	elif let == 'B':
		return 1
	elif let == 'C':
		return 2
	elif let == 'D':
		return 3
	else:
		pass

def num_to_let(num):
	if num == 0:
		return 'A'
	elif num == 1:
		return 'B'
	elif num == 2:
		return 'C'
	elif num == 3:
		return 'D'
	else:
		return 'NA'

def choice_order(cid):
	try:
		choice = session.query(Choice).filter_by(id=cid).first()
		if choice:
			return num_to_let(choice.order)
		else:
			return 'NA'
	except:
		raise
		return 'error in choice_order'
	finally:
		session.close()

#not needed?
def calc_points(prcnt, mxpnt):
	return(prcnt*mxpnt)

def choicerw(cid):
	try:
		chc = session.query(Choice).filter_by(id=cid).first()
		if chc:
			return chc.true_or_false
		else:
			return False
	except:
		return False
		raise
		return 'error in choicerw'
	finally:
		session.close()

def clr_neg_red(cid):

	if cid != cid:
		color = 'red'
		return 'color: %s' % color
	try:
		chc = session.query(Choice).filter_by(id=cid).first()
		if chc:
			if not chc.true_or_false:
				color = 'red'
				return 'color: %s' % color
			else:
				color = 'black'
				return 'color: %s' % color
		else:
			return 'error in clr_neg_red. choice not found.'
	except:
		raise
		return 'error in clr_neg_red'
	finally:
		session.close()

def rmvnone(val):
	if val is None:
		return 1
	else:
		return val

def rowIndex(row):
	print(row)
	return row

#not need anymore
def highlight(value):
	hw = session.query(StudentSelection).filter_by(homework_id = 8)
	qhw = pd.read_sql(hw.statement,session.bind)

	qhwtbl = qhw.loc[:, ['user_id', 'question_id', 'selection']]
	qhwtbl['rghtwrng'] = qhwtbl['selection'].apply(choicerw)
	qhwtblrw = qhwtbl.pivot(index='user_id', columns='question_id', values='rghtwrng')
	print (qhwtblrw)
	return qhwtblrw.applymap(
		lambda x: 'background-color: red' if not x else ''
	)

@app.route('/pdf')
def create_pdf():
	form = LoginForm(request.form)
	return wkhtmltopdf.render_template_to_pdf('showquestions.html', download=True, save=False)

#import ast
from urllib.parse import unquote
@app.route('/exmscn/<hwnum>')
@login_required(type= 2)
def exmscn(hwnum):
	#static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
	stdnt_id = current_user.school_id
	#folder = os.path.join('exam_drop', str(hwnum))
	file = os.path.join('exam_drop', str(hwnum), '{}.png'.format(stdnt_id))
	#print(static_file_dir)
	#if os.path.isfile(os.path.join('static',file)):
		#print('file yes')
	if os.path.isfile(os.path.join(static_file_dir,'static',file)):
	#if os.path.isfile(unquote(url_for('static', filename= file))):

		return ("<img src=" +
			unquote(
				url_for('static', filename= file)
				) + ' '
			"width='100%'' height='auto'" +
			">"
		)
		'''

		return ("<img src=" +
			os.path.join(static_file_dir,'static',file) + ' '
			"width='100%'' height='auto'" +
			">"
		)
		'''
	else:
		return ('file not found')

@app.route('/schdl/<crsnum>')
@login_required(type= 2)
def schdl(crsnum):
	#static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
	if os.path.isfile(os.path.join(static_file_dir,'static','schedule', '{}.pdf'.format(crsnum))):
		statbuf = os.path.getmtime(os.path.join(static_file_dir,'static','schedule', '{}.pdf'.format(crsnum)))
		statbuf = datetime.datetime.fromtimestamp(statbuf)
		statbuf = statbuf.strftime('%m / %d')
		#print("Modification time: {}".format(statbuf))
		file = os.path.join(static_file_dir,'schedule', '{}.pdf'.format(crsnum))
		#print(file)
		'''
		return ("<embed src=" +
			unquote(
				url_for('static', filename= file)
				) + ' '
			"width='100%'' height='100%'" +
			">"
		)
		'''
		return send_file(os.path.join(static_file_dir,'static','schedule', '{}.pdf'.format(crsnum)
				)
		)
	else:
		return ('file not found')

@app.route('/course/<crsnum>/homework/<hwnum>')
#@login_required(type= 2)
def hw_file(crsnum, hwnum):
	if os.path.isfile(os.path.join(static_file_dir,'static',
		'course', crsnum, 'homework', '{}.pdf'.format(hwnum))
	):
		file =  os.path.join('course', crsnum,
			'homework', '{}.pdf'.format(hwnum)
		)

		return send_file(os.path.join(static_file_dir,'static',file))
	else:
		return ('file not found')

@app.route('/attndnc/<date>')
@login_required(type= 2)
def attndncs(hwnum):

	now = datetime.datetime.now().replace(microsecond=0).isoformat()

def get_svg(homework_id, question_id):
	#crsnum = 1
	hwnum = homework_id
	qnum = question_id
	root_folder = 'test_folder'

	q_svg_path = None
	ch_svg_path = []

	hw_path = os.path.join(root_folder,'homework')
	#print(hw_path)
	#print(unquote(url_for('static', filename = hw_path)))
	hw = Homework.get(hwnum)
	hwqs = hw.get_questions()
	hwq = HomeworkQuestion.get(homework_id=hwnum, question_id=qnum)
	if hw is None:
		return 'Homework id not found'
	if qnum not in [q.id for q in hwqs]:
		return 'q_id not in homework'
	#check to see in home is in course?
	#crshw = Courses.get_homework(course_id= crsnum,assgnmnt_type = 1)

	#if not os.path.exists(hw_path):
		#os.makedirs(hw_path)
	#print(os.listdir(hw_path))

	#### this method work for both test server and python anywhere.
	#print(next(os.walk(os.path.join(static_file_dir,'static',hw_path)))[1])
	for f in next(os.walk(os.path.join(static_file_dir,'static',hw_path)))[1]:
		hw_order = int(re.search(r'\[([^]]*)\]', f)[1])
		hw_id = int(re.search(r'\{([^]]*)\}', f)[1])
		if hw_id == hwnum:
			qs_path = os.path.join(hw_path, f)
			for f in next(os.walk(os.path.join(static_file_dir,'static',qs_path)))[1]:
				q_order = int(re.search(r'\[([^]]*)\]', f)[1])
				q_id = re.search(r'\{([^]]*)\}', f)
				if q_id[1]:
					q_id = int(q_id[1])
				else:
					continue
				if q_id == qnum:
					q_path = os.path.join(qs_path, f)
					q_svg = [x for x in os.listdir(os.path.join(static_file_dir,'static',q_path)) if x.endswith('.svg')]
					if q_svg:
						#print('q_svg found')
						q_svg_path = os.path.join(q_path, q_svg[0])
						#print('q_svg_path:{}'.format(q_svg_path))
					else:
						print('q_svg not found. creating blank')
						#create_svg(q_path,q.id)
					c_path = os.path.join(q_path, 'choice')
					c_svg = [x for x in os.listdir(os.path.join(static_file_dir,'static',c_path)) if x.endswith('.svg')]
					if c_svg:
						#print('c_svg found')
						for x in c_svg:
							ch_svg_path.append(os.path.join(c_path,x))
						#print('q_svg_path:{}'.format(ch_svg_path))
					else:
						print('c_svg not found')
						#chs = MultipleC.get_choices(q.id)
						#for ch in chs:
							#create_svg(c_path, ch.id)
	return (q_svg_path, ch_svg_path)

def imgHTML(path):
	image_html = '<img src="{}"style=width: "100px"; height: "100px"/>'.format(unquote(url_for('static', filename = path)))
	return image_html

def radioHTML(name, values):
	radio_html = []
	for value in values:
		template ='''
		<div class="form-check" >
			<input class="form-check-input " type="radio" name="{}" id="{}" value="{}">
		</div>
		'''.format(name, '{}_{}'.format(name, value), value)
		radio_html.append(template)

	return radio_html

def make_dict(name_, list_instance_, funct_ = None, partial_ = None):

	dict_ = {a:b for a,b in zip(
		[name_ + str(instance_.get_id()) for instance_ in list_instance_],  #need get id function
		[getattr(instance_, funct_)() if funct_
		else functools.partial(getattr(instance_,partial_))
		 for instance_ in list_instance_]
		)}
	return dict_

if __name__ == '__main__':

	def scan_exm():
		scnlst = [x for x in os.listdir("exam_scans/")]
		#print(scnlst)
		for scn in scnlst:
			#print(scn)
			image = cv2.imread('exam_scans/{}'.format(scn))
			hwnum = 82
			qrs = decode(image)
			for qr in qrs:
				#hw_id = int(qr.data.decode('UTF-8'))
				hwnum = int(qr.data.decode('UTF-8'))
				#hwnum = 82
			#print('hwnum: {}'.format(hwnum))
			#markers = detect_markers(image)
			#print('{} marker length'.format(len(markers)))

			'''
			for marker in markers:
				#print('{} marker index'.format(markers.index(marker)))
				#print('{} marker id'.format(marker.id))
				#if markers.index(marker) % 2 == 0:  #goes through every marker x2 but not marker 1
					#print(marker.id)
				if marker.id == 1:
					mone = marker
				elif marker.id == 2:
					mtwo = marker
				elif marker.id == 3:
					mthree = marker
				elif marker.id == 4:
					mfour = marker
				else:
					break
			'''
			try:
				(MD_center,MC_center,MA_center,MB_center) = detectCircle(image)
				warpedarray = np.array([
							MD_center,
							MC_center,
							MA_center,
							MB_center
						])
			except:
				print('{} scan error'.format(scn))
				raise
			finally:
				session.close()

			image = cv2.resize(image, (0,0), fx=0.5, fy=0.5  )
			warped = four_point_transform(image, warpedarray)


			############return warped

			#image = cv2.imread("testrgbfill.png")
			gray = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)

			graycrop = Image.fromarray((gray).astype(np.uint8))
			graycrop.save("graycrop.png")

			thresh = cv2.threshold(gray, 0, 255,
				cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]


			thresh2 = cv2.threshold(gray, 0, 255,
				cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
			threshrgb = Image.fromarray((thresh2).astype(np.uint8))
			threshrgb.save("exam_crop/{}".format(scn))


			mlength = math.sqrt(
				thresh.shape[0]**2
				+ thresh.shape[1]**2
			)*1.15
			#print(mlength)
			left = 0
			top = 0
			#plus 1 to adjust for pixel rounding down
			xgap = (mlength*0.013)
			ygap = (mlength*0.013)
			circlesize = (mlength * 0.005)
			let = ['A', 'B', 'C','D']
			y_lvl = (mlength*0.05)
			x_lvl = (mlength*0.10)
			#let_x = left+x_lvl-(xgap*0.22)
			#let_y = top+y_lvl-(.8*ygap)
			#num_x = left+x_lvl-(xgap*1.2)-circlesize
			num_y = top+y_lvl+(circlesize/2*1.2)
			cir_x = left+x_lvl
			cir_y = top+y_lvl


			#id_num = [0,1,2,3,4,5,6,7,8,9]
			id_y_lvl = (mlength*0.30)
			id_x_lvl = (mlength*0.35)
			id_num_x = left+id_x_lvl-(xgap*1.2)-circlesize
			id_num_y = top+id_y_lvl+(circlesize/2*1.2)
			id_cir_x = left+id_x_lvl
			id_cir_y = top+id_y_lvl


			akey_x = left+x_lvl+(xgap+circlesize)*3
			akey_y = top+y_lvl+(circlesize/2*1.2)
			atxt_x = akey_x
			atxt_y = top+y_lvl-(.8*ygap)
			you_x = akey_x+xgap*2
			you_y = atxt_y
			name_x = (mlength*0.30)
			name_y = (mlength*0.50)
			score_x = (mlength*0.30)
			score_y = (mlength*0.52)

			font = cv2.FONT_HERSHEY_SIMPLEX

			stdnt_id = ""
			for j in range(0,7):

				bubbled = None
				for i in range(0,10):
					mask = np.zeros(thresh.shape, dtype="uint8")
					cv2.circle(mask, (int(id_cir_x+xgap*j),int(id_cir_y+ygap*i)),
						int(circlesize), 255, -1)

					mask = cv2.bitwise_and(thresh, thresh, mask=mask)
					total = cv2.countNonZero(mask)

					if bubbled is None or total > bubbled[0]:
						bubbled = (total, i)
				#print(bubbled)
				stdnt_id += str(bubbled[1])
			print('student id {}'.format(stdnt_id))

			#change hwnum for testing
			try:
				#hwqs = session.query(HomeworkQuestion).filter_by(homework_id=66)
				hwqs = session.query(HomeworkQuestion).filter_by(homework_id=hwnum)
				user = session.query(Users).filter_by(school_id=int(stdnt_id)).first()
				#print("{} {}".format(user.first_name,user.last_name))

				#adding text
				cv2.putText(gray,'Key',(int(atxt_x),
					int(atxt_y)), font, 0.5,(0,0,0),2,cv2.LINE_AA
				)
				cv2.putText(gray,'You',(int(you_x),
					int(you_y)), font, 0.5,(0,0,0),2,cv2.LINE_AA
				)

				cv2.putText(gray,"{} {}".format(user.first_name,user.last_name)
					,(int(name_x),int(name_y)),
					font, 1,(0,0,0),1,cv2.LINE_AA
				)
			except:
				print('{} {} scan error'.format(scn,stdnt_id))
				#raise
			finally:
				session.close()
			score = hwqs.count()   #score
			try:
				for i, hwq in enumerate(hwqs):
					bubbled = None
					for j in range(0,4):
						mask = np.zeros(thresh.shape, dtype="uint8")
						cv2.circle(mask, (int(cir_x+xgap*j),int(cir_y+ygap*i)), int(circlesize), 255, -1)

						mask = cv2.bitwise_and(thresh, thresh, mask=mask)
						total = cv2.countNonZero(mask)

						if bubbled is None or total > bubbled[0]:
							bubbled = (total, j)
					#print('{} bubbled'.format(bubbled))

					choice = session.query(Choice).filter(and_(
						Choice.multiplec_id == hwq.question.multiplec.id,
						Choice.order == bubbled[1]
					)).first()

					if choice:
						#print(choice.id)
						newslctn = StudentSelection(user, hwq.homework, hwq.question, choice)
						session.merge(newslctn)




					#answer key text
					answr = session.query(Choice).filter(and_(
						Choice.multiplec_id == hwq.question.multiplec.id,
						Choice.true_or_false == 1
					)).first()

					cv2.putText(gray,num_to_let(answr.order) if answr else 'NA',(int(akey_x),
						int(akey_y+ygap*i)), font, 0.5,(0,0,0),2,cv2.LINE_AA
					)

					if choice != answr:
						cv2.putText(gray,num_to_let(choice.order) if answr else 'NA',(int(akey_x+xgap*2),
							int(akey_y+ygap*i)), font, 0.5,(0,0,0),2,cv2.LINE_AA
						)

						score-=1 #subtracts from total score is wrong

				#score text
				cv2.putText(gray,"score: {}/{}".format(score,hwqs.count())
					,(int(score_x),int(score_y)),
					font, 1,(0,0,0),1,cv2.LINE_AA
				)

				#saving crop + answer key
				thresh3 = cv2.threshold(gray, 0, 255,
					cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
				threshrgb3 = Image.fromarray((thresh3).astype(np.uint8))
				folder = os.path.join('static','exam_drop', str(hwnum))
				if not os.path.exists(os.path.join('static','exam_drop', str(hwnum))):
					os.makedirs(os.path.join('static','exam_drop', str(hwnum)))
					#print(os.path.join('static','exam_drop', str(hwnum)) + " has been created")
				file = os.path.join(folder, "{}.png".format(user.school_id))
				threshrgb3.save(file)

				session.commit()
				print('{} scan successful'.format(scn))
				os.remove('exam_scans/{}'.format(scn))
			except:
				session.rollback() #Rollback the changes on error
				#return 'scan error'
				print('{} {} scan error'.format(scn,stdnt_id))
				#raise

			finally:
				session.close() #Close the connection
	scan_exm()

	import sys
	#import comtypes.client
	def save_pdf():
		wdFormatPDF = 17
		in_file =  os.path.abspath('test.docx')
		print(in_file)
		out_file =os.path.abspath('test.pdf')
		try:
			word = comtypes.client.CreateObject('Word.Application')
			doc = word.Documents.Open(in_file)
			doc.SaveAs(out_file, FileFormat=wdFormatPDF)
		except:
			raise
		finally:
			doc.Close()
			word.Quit()
	#save_pdf()

	def chngtyp():
		hw = session.query(Homework).filter_by(id= 57).first()
		hw.assgnmt_typ = 5
		hw = session.query(Homework).filter_by(id= 58).first()
		hw.assgnmt_typ = 5
		session.commit()
		session.close()
	#chngtyp()
	def chngmaxscr():
		hws = session.query(Homework).filter_by(assgnmt_typ = 1)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 1
		hws = session.query(Homework).filter_by(assgnmt_typ = 2)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 17.5
		hws = session.query(Homework).filter_by(assgnmt_typ = 3)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 1.2
		hws = session.query(Homework).filter_by(assgnmt_typ = 4)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 4
		hws = session.query(Homework).filter_by(assgnmt_typ = 5)
		assgnmnts = session.query(AssgnmntGrade).filter(
			AssgnmntGrade.homework_id.in_(hw.id for hw in hws))
		for assgnmnt in assgnmnts:
			assgnmnt.max_point = 35
		session.commit()
		session.close()
	#chngmaxscr()

	def test():
		print(os.path.join(url_for('static')))
	#test()

	def writefile():
		file = open('testfile.txt','a+')
		file.write('Hello World\n')
		file.write('This is our new text file\n')
		file.write('and this is another line.\n')
		file.write('Why? Because we can.\n')
		file.close()
	#writefile()


	def make_all_active():
		try:
			crss = session.query(Courses).all()
			for crs in crss:
				crs.active = 0
				session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()
	#make_all_active()

	def test():
		try:
			crs = session.query(Courses).filter_by(id = 1).first()
			for u in crs.get_users():
				print(u)
		except:
			raise
		finally:
			session.close()
	#test()

	def get_alembic():
		try:
			almbc = session.query(Alembic).all()
			for ver in almbc:
				print(ver.version_num)
		except:
			raise
		finally:
			pass
	#get_alembic()

	def alphatable():
		try:
			hwid = 2
			hw = Homework.get(hwid)
			hwqs = hw.get_questions()
			crs = Courses.get(hw.course_id)
			usrs = crs.get_users()

		except:
			raise
		finally:
			session.close()
		for usr in usrs:
			newrow = RowMake()
			fname = CellMake()
			fname.add_text(usr.first_name)
			newrow.add_cell(fname)
			lname = CellMake()
			lname.add_text(usr.last_name)
			newrow.add_cell(lname)

			hwss = StudentSelection.get_hw_ss(usr_id = usr.id, hwqs = hwqs, ifNone = None)
			for ss in hwss:
				sscell = CellMake()
				sscell.add_text(ss)
				newrow.add_cell(sscell)
			print(newrow)
	#alphatable()
	def create_svg(path,name):
		height = 100
		width = 100
		f = open(os.path.join(path,'{{{}}}.svg'.format(name)), 'w+')
		f.write(
			'<svg id="new_id" xmlns="http://www.w3.org/2000/svg" \
			xmlns:xlink="http://www.w3.org/1999/xlink" \
			width="{}" height="{}" viewBox="0 0 {} {}">\n'.format(
				width,height,width,height
				)
			)
		#f.write('\t<image width="{}" height="{}" xlink:href="data:image/png;base64,'.format(width, height))

		#f.write('"/>\n')
		f.write('</svg>')
		f.close()
	def folder_test():
		crsnum = 1
		hwnum = 1
		qnum = 1
		root_folder = 'test_folder'

		q_svg_path = None
		ch_svg_path = []

		hw_path = os.path.join(root_folder,'homework')

		crshw = Courses.get_homework(course_id= crsnum,assgnmnt_type = 1)
		#if not os.path.exists(hw_path):
			#os.makedirs(hw_path)
		#print(os.listdir(hw_path))
		for f in next(os.walk(hw_path))[1]:
			hw_order = int(re.search(r'\[([^]]*)\]', f)[1])
			hw_id = int(re.search(r'\{([^]]*)\}', f)[1])

			if hw_id:
				print(hw_id)
			else:
				print('no id')

			hw = Homework.get(hw_id)
			if hw:
				print(hw)
			hwqs = hw.get_questions()
			qs_path = os.path.join(hw_path, f)
			#print(next(os.walk(qs_path))[1])
			for f in next(os.walk(qs_path))[1]:
				q_order = int(re.search(r'\[([^]]*)\]', f)[1])
				q_id = re.search(r'\{([^]]*)\}', f)
				if q_id[1]:
					q_id = int(q_id[1])
				else:
					print('no id in bracket')
					#break
				q = Question.get(q_id)
				if q:
					hwq = HomeworkQuestion.get(homework_id=hw_id, question_id=q_id)
					#print(q_order)
					print('q order:{}'.format(hwq.order))
					if hwq.order == None:
						try:
							hwq.order = q_order
							session.merge(hwq)
							session.commit()
							print(hwq.order)
						except:
							session.rollback()
							raise
						finally:
							session.close()
					else:
						print('cont to question folder')
						q_path = os.path.join(qs_path, f)
						q_svg = [x for x in os.listdir(q_path) if x.endswith('.svg')]
						if q_svg:
							print('q_svg found')
							q_svg_path = os.path.join(q_path, q_svg[0])
							print(q_svg_path)
						else:
							print('q_svg not found. creating blank')
							create_svg(q_path,q.id)
					c_path = os.path.join(q_path, 'choice')
					c_svg = [x for x in os.listdir(c_path) if x.endswith('.svg')]
					if c_svg:
						print('c_svg found')
						for x in c_svg:
							ch_svg_path.append(os.path.join(c_path,x))
						print(ch_svg_path)
					else:
						print('c_svg not found. creating blanks')
						chs = MultipleC.get_choices(q.id)
						for ch in chs:
							create_svg(c_path, ch.id)
				else:
					print('question id not found in database')
					break
	#folder_test()

	def pandas_table():
		try:
			hwid = 3
			usr = Users.get_from_id(5)
			hw = Homework.get(hwid)
			hwqs = hw.get_questions()
			ss = session.query(StudentSelection).filter(and_(
				#StudentSelection.homework_id == usr.id,
				StudentSelection.question_id == hw.id
				))
			qhw = pd.read_sql(ss.statement,session.bind)
			if qhw.empty:
				return 'no data for homework'
			crs = Courses.get(hw.course_id)
			usrs = crs.get_users()
		except:
			raise
		finally:
			session.close()

		print(qhw)
	#pandas_table()

	def users_get_courses():
		usr = Users.get_from_id(2)
		crss = usr.get_courses()
		crss_list = [crs for crs in crss]
		print(len(crss_list))
		if not crss_list:
			print('yes')
		for crs in crss_list:
			print(crs.name)
	#users_get_courses()

	def make_item(instance_, funct):
		item_name = name_con + object_id
		item_funct = funct
		return item_name,item_funct

	def admin_courses():
		####
		#user = session.query(Users).filter_by(id = 1).first() #current_user

		###
		base = template.Base_Template()
		pd.set_option('display.max_colwidth', -1)
		df = pd.DataFrame()
		try:
			crss = session.query(Courses).all()
		except:
			raise
		finally:
			session.close()

		for crs in crss:
			course_url = template.Template.make_url_for('admin_course', var = 'crs_num', num = crs.id, label = 'link')
			df2 = pd.DataFrame([[crs.id, crs.name, crs.description, course_url]], columns=list(['id','course_name','description', 'link']))
			df = df.append(df2)

		base.add_body(df.to_html(escape = False))
		print(base)

	#admin_courses()


	def instr_crs_roster():
		crs_id = 1
		crs = Courses.get(crs_id)
		usrs = crs.get_users(includeInactive = 1)
		#crsusrs = crs.get_courseuser()
		dict_active = dict()
		dict_funct = dict()
		df = pd.DataFrame()

		for usr in usrs:
			name_active = 'usr_active_{}'.format(usr.id)
			active_ = CourseUser.get_active(crs.id, usr.id)
			dict_active[name_active] = active_
			dict_funct[name_active] = functools.partial(CourseUser.change_active, crs_id= crs.id, usr_id= usr.id)
			active_checkbox = template.forms.checkbox(name_active)

			df2 = pd.DataFrame([[usr.id, usr.first_name, usr.last_name, active_checkbox]], columns=list(['id','first','last', 'active']))
			df = df.append(df2)

			#CourseUser.toggle_active(crs_id,usr.id, toggle = 0)
		#print(dict_active)
		#print(dict_funct)
		#dict_funct['usr_active_2'](value = True)
		print(df)
		print(df.to_html(escape = False))
		formtest = template.forms()
		formtest.add_content(df.to_html(escape = False))
		print(formtest)
	#instr_crs_roster()

	def test(list_inst, meth):
		for inst in list_inst:
			funct = getattr(inst,meth)
			name = funct()
			print(name)
	def test_u():
		crs = Courses.get(1)
		usrs = crs.get_users()
		test(usrs, 'get_first_name')

	def process_one(list_inst, get_answ, ch_answ):
		dict_old = dict()
		dict_funct = dict()
		for inst in list_inst:
			name_ = 'q_{}'.format(inst.id) #need get id funct and naming funct
			funct = getattr(inst, get_answ)
			inst_answ = funct()
			funct_two = getattr(inst, ch_answ)
			dict_old[name_]	= str(inst_answ.id) if inst_answ else None
			dict_funct[name_] = functools.partial(funct_two)
		return dict_old, dict_funct


	def dict_old_process(list_inst, *args):
		dict_ = dict()
		for inst in list_inst:
			#name_ = 'q_{}'.format(inst.id) #need get id funct
			for arg in args:
				name_ = str(arg) + '_'+ str(inst.id)
				funct = getattr(inst, arg)
				inst_ = funct()
				dict_[name_] = str(inst_) if inst_ else None
		return dict_

	def dict_old_process_(*args):
		dict_ = dict()
		for arg in args:
			list_inst, meth = arg
			for inst in list_inst:
				nested_inst = inst.get_instance()
				inst_name = inst.get_name()
				funct = getattr(nested_inst, meth)

				dict_[inst_name] = str(funct())
				#print(dict_[inst_name])
		return dict_


		#accdentally made changes that doesnt matter. works like dict_old_process_
	def dict_funct_process_(*args):
		dict_ = dict()
		list_ = []
		for arg in args:
			list_inst, meth = arg
			list_2 = []
			for inst in list_inst:
				nested_inst = inst.get_instance()
				inst_name = inst.get_name()
				funct = getattr(nested_inst, meth)
				list_2.append([inst_name,funct])
			list_.append(list_2)

		zip_list = zip(*list_)
		for row in zip_list:
			for col in row:
				name_ , funct_ = col
				dict_[name_] = functools.partial(funct_)
		#print(dict_)
		return dict_

	def data_frame_process_(*args):
		df = pd.DataFrame()
		list_ = []
		for arg in args:
			list_inst, meth = arg
			list_2 = []
			for inst in list_inst:
				nested_inst = inst.get_instance()
				inst_name = inst.get_name()
				funct = getattr(nested_inst, meth)
				list_2.append([inst_name,funct])
			list_.append(list_2)

		zip_list = zip(*list_)
		for row in zip_list:
			list_3 = []
			for col in row:
				name_ , funct_ = col
				if name_:
					list_3.append(funct_(name = name_))
				else:
					list_3.append(funct_())
			df_ = pd.DataFrame([list_3])
			#print(df_)
			df = df.append(df_)
			#df =  pd.concat([df, df_], axis=0)
		df = df
		return df

	class group_method_():
		def __init__(self, instance, name = 'default'):
			self.instance = instance
			if name:
				self.name = name + str(instance.get_id())
		def get_instance(self):
			return self.instance
		def get_name(self):
			return self.name

	def dict_funct_process(list_inst, *args):
		dict_ = dict()
		for inst in list_inst:
			#name_ = 'q_{}'.format(inst.id) #need get id funct
			for arg in args:
				name_ = str(arg) + '_'+ str(inst.id)
				funct = getattr(inst, arg)
				dict_[name_] = functools.partial(funct)
		return dict_


	def data_frame_process(list_inst, *args):
		df = pd.DataFrame()
		for inst in list_inst:
			#name_ = 'q_{}'.format(inst.id) #need get id funct
			list_ = []
			for arg in args:
				funct = getattr(inst, arg)
				list_.append(funct())
			df_ = pd.DataFrame([list_])
			df = df.append(df_)
		return df

	def request_process_(request_form, dict_old):
		form = request_form
		dict_new = dict()
		for key in dict_old:
			dict_new[key] = request_form.get(key, False)
		set_1 = set(dict_old.items())
		set_2 = set(dict_new.items())
		dict_diff = dict(set_2 - set_1)
		return dict_diff

	def instr_hw_revise_(hw_id):

		###
		request_method = 'POST'

		###init
		base = template.Base_Template()
		pd.set_option('display.max_colwidth', -1)

		hw_id = hw_id
		hw = Homework.get(hw_id)
		qs = hw.get_questions()

		qs_group = [group_method(q) for q in qs]
		qs_group_2 = [group_method(q) for q in qs]

		dict_old = dict_old_process_((qs_group, 'get_answer_id'),(qs_group_2, 'get_description'))
		dict_funct = dict_funct_process_((qs_group, 'change_answer_'),(qs_group_2, 'change_description'))
		df = data_frame_process_((qs_group, 'make_radio'),(qs_group_2,'make_description_text'))

		formtest = template.forms()
		formtest.add_content(df.to_html(escape = False))
		base.add_body(formtest)

		###post
		if request_method == 'POST':
			form = request_form
			dict_diff = request_process(form, dict_old)

			map_dict(dict_diff, dict_funct)
	#instr_hw_revise_(1)

	def instr_hw_revise(hw_id):
		###init
		base = template.Base_Template()
		pd.set_option('display.max_colwidth', -1)

		hw_id = hw_id
		hw = Homework.get(hw_id)
		qs = hw.get_questions()


		###body
		#dict_old = dict_old_process(qs, 'get_answer_id', 'get_description')
		dict_old = make_dict(name_ = 'hello', list_instance_= qs,  funct_ = 'get_answer_id')
		dict_old.update(make_dict(name_ = 'world', list_instance_= qs,  funct_ = 'get_description'))
		dict_funct = make_dict(name_ = 'hello', list_instance_= qs, partial_ ='change_answer_')
		dict_funct.update(make_dict(name_ = 'world', list_instance_= qs, partial_ = 'change_description'))
		#dict_funct = dict_funct_process(qs, 'change_answer_', 'change_description')
		df = data_frame_process(qs, 'get_id', 'make_radio', 'make_description_text')
		df_1 = [q.get_id() for q in qs]
		df_2 = [q.make_radio(name = 'hello' + str(q.get_id())) for q in qs]
		df_3 = [q.make_description_text(name = 'world'+str(q.get_id())) for q in qs]
		df_dict = {
			'q id':df_1,
			'answer':df_2,
			'descript':df_3,
		}
		df = pd.DataFrame.from_dict(df_dict)
		df = df[['q id','answer','descript']]
		print(df)
		formtest = template.forms()
		formtest.add_content(df.to_html(escape = False, index = False))
		base.add_body(formtest)
		#print(formtest)


	#instr_hw_revise(1)

	def admin_index():
		###
		base = template.Base_Template()
		pd.set_option('display.max_colwidth', -1)
		df = pd.DataFrame(columns=list(['Links']))

		alchemy_dump_url = template.Template.make_url_for('alchdump', label = "Alchemy Dump")
		df2 = pd.DataFrame([alchemy_dump_url])
		courses_url = template.Template.make_url_for('admin_courses_', label = "Courses")
		df3 = pd.DataFrame([courses_url])

		df = df.append(df2)
		df = df.append(df3)
		print(df)
		base.add_body(df.to_html(escape = False, index = False))

		#print(base)
	#admin_index()

	def new_course():
		new_c = Courses('CHEM 1412 (8:00am)', 'blank')
		session.add(new_c)
		session.commit()
		new_d = Courses('CHEM 1412 (11:15am)', 'blank')
		session.add(new_d)
		session.commit()
		session.close()
	#new_course()

	#1=test, 2 = hw, 3 = lab
	def copy_homework():
		try:
			old_hw_list = Courses.get_homework(3,2)
			for hw in old_hw_list:
				print('hw_uid:{}'.format(hw.id))

			old_hw = Homework.get(3)
			new_hw = Homework.get(59)
			qs = old_hw.get_questions()
			for q in qs:
				print('q_id:{}'.format(q.id))

				hq = HomeworkQuestion(new_hw,q)
				session.add(hq)
				session.commit()
		except:
			session.rollback()
			raise
		finally:
			session.close()

	#copy_homework()
	def make_course_active():
		crs = Courses.get(1)
		crs.active = 1
		session.merge(crs)
		session.commit()
		session.close()
	#make_course_active()

	def add_user_to_course():
		list_ = ['0057463',
]
		crs = Courses.get(4)
		for u in list_:
			usr = Users.get(u)
			try:
				new_cu = CourseUser(crs,usr)
				session.add(new_cu)
				session.commit()
				session.close()
				print('success{}'.format(usr.school_id))
			except:
				print("error at user_id:{}".format(usr.school_id))
				raise
			finally:
				session.close()
	#add_user_to_course()

	def get_std_list():
		crs = Courses.get(5)
		usrs = crs.get_users(includeInactive = 1)
		for u in usrs:
			print(u.school_id)
	#get_std_list()
	def del_course():
		for i in range(6,66):
			crs = Courses.get(i)
			session.delete(crs)
			session.commit()
	#del_course()
	def get_crsd_list():
		crss = session.query(Courses).all()
		for c in crss:
			print(c.id)
	#get_crsd_list()

	def omikron_():
		crs_id = 1
		crs = Courses.get(crs_id)
		#usrs = crs.get_users(includeInactive = 1)
		usrs = crs.get_courseuser()

		base = template.Base_Template()
		pd.set_option('display.max_colwidth', -1)

		group_name = 'hello'
		'''
		dict_old = {a:b for a,b in zip(
			[group_name + str(usr.user_id) for usr in usrs],  #need get id function
			[usr.get_active() for usr in usrs]
			)}
		print(dict_old)
		'''
		dict_old = make_dict(name_ = group_name, list_instance_= usrs,  funct_ = 'get_active')
		print(dict_old)
		'''
		dict_funct = {a:b for a,b in zip(
			[group_name + str(usr.user_id) for usr in usrs],
			[functools.partial(usr.change_active) for usr in usrs]
			)}
		'''
		dict_funct = make_dict(name_ = group_name, list_instance_= usrs, partial_ ='get_active')
		print(dict_funct)
		df_1 = [usr.make_active_checkbox(name = group_name + str(usr.user_id)) for usr in usrs]
		df_2 = [usr.user_id for usr in usrs]
		df_3 = [Users.get_from_id(usr.user_id).get_first_name() for usr in usrs]
		df_dict = {
			'user id':df_2,
			'user name':df_3,
			'checkbox':df_1,
		}
		df = pd.DataFrame.from_dict(df_dict)
		df = df[['user id','user name','checkbox']]
		print(df)
	#omikron_()

	def add_q_to_hw():
		hw = Homework.get(64)
		q_id_list = [142,143,144,145,146,147,148,149,150,151,172,173,174,175,176,177,178,179,180,181,182,183,184]

		for q_id in q_id_list:
			q = Question.get(q_id)
			hwq = HomeworkQuestion(hw,q)
			try:
				session.add(hwq)
				session.commit()
				print(q.id)
			except:
				session.rolback()
			finally:
				session.close()
	#add_q_to_hw()
	def add_new_q_to_hw():
		hw = Homework.get(82)
		q_range = 38
		try:
			with session.no_autoflush:
				for x in range(int(q_range)):
					mc = MultipleC('none')
					session.add(mc)
					session.commit()

					for y in range(4):
						c = Choice('none',y,False)
						session.add(c)
						session.commit()
						mc.choice.append(c)
						session.commit()

					q = Question(1, 'blank')

					session.add(q)
					session.commit()
					q.multiplec=mc
					session.commit()
					q = Question.get(q.id)
					hq = HomeworkQuestion(hw,q)
					session.add(hq)
					session.commit()
					print(x)
		except:
			session.rollback()
			raise
		finally:
			session.close()
	#add_new_q_to_hw()

	def phi_():
		users = session.query(Users).all()
		user_list = [{'first':user.first_name, 'last':user.last_name} for user in users]
		print(user_list)
		session.close()
	#phi_()

	def student_selection_table():
		hw = Homework.get(8)
		crs_id = hw.get_course_id() #get course from hw
		crs = Courses.get(crs_id)
		usrs = crs.get_users()
		qs = hw.get_questions()

		###
		#base = template.Base_Template()
		#pd.set_option('display.max_colwidth', -1)

		#make index
		index_array = [(
			usr.get_school_id(),
			usr.get_first_name(),
			usr.get_last_name()
			)
			for usr in usrs
		]
		index = pd.MultiIndex.from_tuples(index_array, names=['ID', 'First','Last'])
		table_df = pd.DataFrame(index = index)

		for i,q in enumerate(qs):
			sss_list = []
			for usr in usrs:
				ss = StudentSelection.get(usr.get_id(),hw.get_id(),q.get_id())
				sss_list.append(ss.get_selection() if ss else None)
			#slct_dict.update({'{}({})'.format(i+1,q.get_id()) : sss_list})
			slct_df = pd.DataFrame(sss_list, columns=['{}({})'.format(i+1,q.get_id())], index = index)
			table_df = pd.concat([table_df,slct_df], axis=1)

		### df of letter choice
		to_let = lambda x: np.NaN if pd.isnull(x) else choice_order(x)
		let_df = table_df.applymap(to_let)

		###make true/false df
		to_right = lambda x: np.NaN if pd.isnull(x) else Choice.get(x).get_true_or_false()
		slct_right_df = table_df.applymap(to_right)

		## sum of row trues
		sum_df = slct_right_df.apply(np.sum, axis = 1)
		#sum_df = pd.Series(sum_df, columns = ['TotalPt'])########### this breaks it? makes it into a frame instaad of series?
		## percent of trues
		percentage = lambda x: x/len(qs)
		percentage_df = sum_df.apply(percentage)

		##only show wrong answers
		to_only_wrong = lambda x: '-' if pd.isnull(x) else choice_order(x) if not Choice.get(x).true_or_false else ''
		only_wrong_df = table_df.applymap(to_only_wrong)

		###answer key
		answer_key = ['Key']
		answer_key.extend([choice_order(q.get_answer_id()) for q in qs])
		answer_df = pd.DataFrame(data=answer_key)
		###point biserial
		rows_that_have_data = slct_right_df[percentage_df.notnull()]
		pointbiserial = lambda x: stats.pointbiserialr(x.fillna(False), percentage_df.dropna())[0]
		pointbiserial_df = rows_that_have_data.apply(pointbiserial, axis = 0)
		###percent correct
		percent_correct = lambda x: x.fillna(False).sum()/len(x)
		percent_correct_df = rows_that_have_data.apply(percent_correct, axis = 0)

		print(percent_correct_df)
		#print(sum_df)
		#print(let_df)
		#print(only_wrong_df)
		session.close()

	#student_selection_table()

	def bcrypt_password():
		user = Users.get_from_id(1)
		user.password = bcrypt.generate_password_hash('skadoodle')
		session.commit()
		session.close()
	#bcrypt_password()

	def del_question():
		for i in range(1237,1238):
			q = Question.get(i)
			session.delete(q)
			session.commit()
	del_question()

	app.run(debug = False)
