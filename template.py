class Template():
	def __init__(self):
		#self.html = None
		self.style = []
		self.body = []
		self.head = []
	def add_style(self, *args):
		for x in args:
			self.cell_class.append(x)
	def add_body(self,*args, front = 0):
		if front:
			for x in args:
				self.body.insert(0,x)			
		else:
			for x in args:
				self.body.append(x)
	def add_head(self, *args):
		for x in args:
			self.head.append(x)
	@staticmethod
	def make_url_for(route, var = '', num = '', label = ''):
		if var and num:
			var_num = ', {} = {}'.format(var,num)
		else:
			var_num = ''

		html = '''<a href="{{{{ url_for('{}'{}) }}}}">{}</a>

		'''.format(route,var_num,label)
		return html
	def __str__(self):
		body_html = (
			"<body>{}</body>".format(' '.join(
				str(x) for x in self.body)) 
			if self.body else ''		
			)
		head_html = (
			"<head>{}</head>".format(' '.join(
				str(x) for x in self.head)) 
			if self.head else ''		
			)		
		base = '''
			<!DOCTYPE html>
			<html>
				{}
				{}
			</html>
			'''.format(head_html, body_html)
		return base

class Base_Template(Template):
	def __init__(self):
		Template.__init__(self)
		bootstrap_css = '''
			<link rel="stylesheet" 
			href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
			integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
			crossorigin="anonymous">
		'''	
		jquery_script = '''
			<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
			integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
			crossorigin="anonymous">
			</script>
		'''	
		bootstrap_script = '''
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
			integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" 
			crossorigin="anonymous">
			</script>	
		'''
		self.add_body(jquery_script, bootstrap_script)
		self.add_head(bootstrap_css)

class MathJax_Template(Base_Template):
	def __init__(self):
		Base_Template.__init__(self)
		MathJax_js_1 = '''
			<script type="text/javascript" async 
			src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
			</script>

		'''
		MathJax_config_1 ='''
			<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				showProcessingMessages: false,
				tex2jax: { inlineMath: [['$','$'],['\\\\(','\\\\)']] }
				});
			</script>
		'''
		MathJax_js_2 = '''
			<script type="text/javascript" 
         		src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
      		</script>

		'''
		MathJax_config_2 ='''
			<script>
			var Preview = {
			  delay: 150,        // delay after keystroke before updating
			  preview: null,     // filled in by Init below
			  buffer: null,      // filled in by Init below
			  timeout: null,     // store setTimout id
			  mjRunning: false,  // true when MathJax is processing
			  mjPending: false,  // true when a typeset has been queued
			  oldText: null,     // used to check if an update is needed
			  //
			  //  Get the preview and buffer DIV's
			  //
			  Init: function () {
			    this.preview = document.getElementById("MathPreview");
			    this.buffer = document.getElementById("MathBuffer");
			  },
			  //
			  //  Switch the buffer and preview, and display the right one.
			  //  (We use visibility:hidden rather than display:none since
			  //  the results of running MathJax are more accurate that way.)
			  //
			  SwapBuffers: function () {
			    var buffer = this.preview, preview = this.buffer;
			    this.buffer = buffer; this.preview = preview;
			    buffer.style.visibility = "hidden"; buffer.style.position = "absolute";
			    preview.style.position = ""; preview.style.visibility = "";
			  },
			  //
			  //  This gets called when a key is pressed in the textarea.
			  //  We check if there is already a pending update and clear it if so.
			  //  Then set up an update to occur after a small delay (so if more keys
			  //    are pressed, the update won't occur until after there has been 
			  //    a pause in the typing).
			  //  The callback function is set up below, after the Preview object is set up.
			  //
			  Update: function () {
			    if (this.timeout) {clearTimeout(this.timeout)}
			    this.timeout = setTimeout(this.callback,this.delay);
			  },
			  //
			  //  Creates the preview and runs MathJax on it.
			  //  If MathJax is already trying to render the code, return
			  //  If the text hasn't changed, return
			  //  Otherwise, indicate that MathJax is running, and start the
			  //    typesetting.  After it is done, call PreviewDone.
			  //  
			  CreatePreview: function () {
			    Preview.timeout = null;
			    if (this.mjPending) return;
			    var text = document.getElementById("text").value;
			    if (text === this.oldtext) return;
			    if (this.mjRunning) {
			      this.mjPending = true;
			      MathJax.Hub.Queue(["CreatePreview",this]);
			    } else {
			      this.buffer.innerHTML = this.oldtext = text;
			      this.mjRunning = true;
			      MathJax.Hub.Queue(
			  ["Typeset",MathJax.Hub,this.buffer],
			  ["PreviewDone",this]
			      );
			    }
			  },
			  //
			  //  Indicate that MathJax is no longer running,
			  //  and swap the buffers to show the results.
			  //
			  PreviewDone: function () {
			    this.mjRunning = this.mjPending = false;
			    this.SwapBuffers();
			  }
			};
			//
			//  Cache a callback to the CreatePreview action
			//
			Preview.callback = MathJax.Callback(["CreatePreview",Preview]);
			Preview.callback.autoReset = true;  // make sure it can run more than once
			</script>

		'''
		prev_box = '''
		<p>Preview is shown here:</p>
		<div id="MathPreview" style="border:1px solid; padding: 3px; width:50%; margin-top:5px"></div>
		<div id="MathBuffer" style="border:1px solid; padding: 3px; width:50%; margin-top:5px; 
			visibility:hidden; position:absolute; top:0; left: 0"></div>
		'''
		init = '''
		<script>Preview.Init();</script>
		'''
		self.add_body(prev_box,init)
		self.add_head(MathJax_js_1, MathJax_js_2, MathJax_config_1, MathJax_config_2)

class forms():
	def __init__(self):
		self.content = []
	def add_content(self, item):
		self.content.append(item)
	@staticmethod
	def textarea(name = '', other = '', value = ''):
		name_text = (
			'name = \"{}\"'.format(name)
			if name else ''
			)
		other_text = (
			other
			) if other else ''
		html = '<textarea {} {}>{}</textarea>'.format(name_text, other, value)
		return html
	@staticmethod
	def checkbox(name = '', checked = 0):
		name_text = (
			'name = \"{}\"'.format(name)
			if name else ''
			)
		#print('checkbox: {}'.format(checked))
		html = '<input type="checkbox" {} {}>'.format(name_text, 'checked' if checked == True else '')
		return html
	@staticmethod
	def radio(choices, name = ''):
		choice_id_list = [choice.id for choice in choices]
		name_text = (
			'name = \"{}\"'.format(name)
			if name else ''
			)

		html = ''
		for choice in choices:
			checked = ''
			if choice.true_or_false:
				checked = 'checked'
			value_text = 'value = \"{}\"'.format(choice.id)
			html = html + ' <input type="radio" {} {} {}>'.format(name_text,value_text,checked)
		return html
	@staticmethod
	def text(name = '', value = ''):
		name_text = (
			'name = \"{}\"'.format(name)
			if name else ''
			)
		value_text = (
			'value = \"{}\"'.format(value)
			if value else ''
			)
		html = '<input type="text" {} {}>'.format(name_text, value_text)
		return html
	def __str__(self):
		submit_html = '''<input type="submit" value="Submit">'''
		self.add_content(submit_html)
		html = "<form method = post enctype=multipart/form-data>{}</form>".format('<br>'.join(
				self.content)) 
		return html

#<textarea id="text" name="text" onkeyup="Preview.Update()"></textarea>

if __name__ == '__main__':
	test = Base_Template()
	form = '''
	<form method = post>
		<div class="form-group">
		</div>
	</form>
	'''
	link = Template.make_url_for('course', var = 'crsnum', num = 1, label = "label")

	test.add_body(form)
	test.add_body(link)
	#print(str(test))

	formtest = forms()
	things2 = formtest.text(name = 'idid', value = 'default value')
	#print(things2)
	formtest.add_content(things2)
	things = formtest.textarea(name = 'idid', other = '''onkeyup="Preview.Update()"''' )
	formtest.add_content(things)
	print(str(formtest))
