from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

SQLALCHEMY_DATABASE_URI = "mysql://{username}:{password}@{hostname}/{databasename}".format(
    username="tnguyen",
    password="tuesday0418",
    hostname="tnguyen.mysql.pythonanywhere-services.com",
    databasename="tnguyen$default",
)

#SQLALCHEMY_DATABASE_URI2 = "sqlite:///test.db"
SQLALCHEMY_DATABASE_URI2 = "mysql://{username}:{password}@{hostname}/{databasename}".format(
    username="root",
    password="",
    hostname="localhost",
    databasename="test_area",
)

engine = create_engine(SQLALCHEMY_DATABASE_URI)
engine2 = create_engine(SQLALCHEMY_DATABASE_URI2)


Base = declarative_base()

#Base = declarative_base(cls=Model, name='Model', metaclass=_BoundDeclarativeMeta)
#Base.query = _QueryProperty(self)

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

Session2 = sessionmaker()
Session2.configure(bind=engine2)
session2 = Session2()