# coding: utf-8

from flask import current_app
from sqlalchemy.ext.serializer import dumps, loads
from sqlalchemy.inspection import inspect

'''
######################added
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

SQLALCHEMY_DATABASE_URI = "mysql://{username}:{password}@{hostname}/{databasename}".format(
    username="root",
    password="",
    hostname="localhost",
    databasename="test_database",
)

engine = create_engine(SQLALCHEMY_DATABASE_URI)
Base = declarative_base()

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()
######################added
'''

class AlchemyDumpsDatabase(object):

    def __init__(self, base, session):     #base added
        self.do_not_backup = list()
        self.models = list()
        self.db = base     #####added
        self.session = session #####added

    @staticmethod
    def db():
        return current_app.extensions['alchemydumps'].db

    def get_mapped_classes(self):
        """Gets a list of SQLALchemy mapped classes"""
        #db = self.db()              #############removed

        self.add_subclasses(self.db)     #############added
        #self.add_subclasses(db.Model)
        return self.models

    def add_subclasses(self, model):
        """Feed self.models filtering `do_not_backup` and abstract models"""
        if model.__subclasses__():
        #if self.db.__subclasses__():    # added
            for submodel in model.__subclasses__():
                self.add_subclasses(submodel)
        else:
            self.models.append(model)

        '''
        else:
            columns = inspect(model).columns
            primfor = False
            for r in columns:
                if r.primary_key and r.foreign_keys:
                    primfor = True
                    
            if primfor:
                self.models.append(model)
            else:
                self.models.insert(0, model)

        Added to check if primarykey is a foreign key.
        If true, the model is placed at the end of the list. False is placed at beginning.
        Needed for sqlalchemy merge to work correctly
        !!!!!!!order is still bad. parents must alwys be placed in from of child. needs rework
        '''



    def get_data(self):
        """Go through every mapped class and dumps the data"""
        db = self.db()
        data = dict()
        for model in self.get_mapped_classes():
            #query = session.query(model)
            query = self.session.query(model)     #############added
            data[model.__name__] = dumps(query.all())
        return data

    def parse_data(self, contents):
        """Loads a dump and convert it into rows """
        db = self.db()
        return loads(contents, self.db, self.session)
